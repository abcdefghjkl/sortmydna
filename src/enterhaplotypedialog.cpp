/**
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include"sortmydnaconfig.h"
#include"enterhaplotypedialog.hpp"
#include"haplotype.hpp"
#include"commonuielements.hpp"

#include<iostream>
#include<QDialogButtonBox>
#include<QVBoxLayout>
#include<QComboBox>
#include<QGroupBox>
#include<QPlainTextEdit>
#include<QTextStream>

namespace sortmydna
{
    EnterHaplotypeDialog::EnterHaplotypeDialog(
        QWidget* parent,
        const AppConfig& config) 
    : QDialog(parent),
      custom_model_(config.custom_model),
      model_name_(config.model_name),
      haplo_type_(config.haplotype), 
      haplo_group_(config.haplo_group),
      haplo_subgroup_(config.haplo_subgroup)
    {
        QString dialogname = PROJECT_NAME;
        dialogname += " - enter your haplotype";
        setWindowTitle(dialogname);
        auto panels = dna_manipulation::HaploSet::get_panel_names();
        QComboBox* panel_combo = new QComboBox();
        for(const auto& name : panels)
            panel_combo->addItem(name.c_str());
        panel_combo->addItem(tr("Custom"));
        panel_combo->setCurrentText(model_name_);

        QPlainTextEdit* error_edit = new QPlainTextEdit();
        QPlainTextEdit* type_edit = new QPlainTextEdit();
        connect(
            type_edit,
            &QPlainTextEdit::textChanged,
            this,
            [this, type_edit, error_edit, panel_combo]() {
                model_text_changed(
                    type_edit,
                    error_edit,
                    panel_combo->currentText() == tr("Custom"));
            });
        connect(
            panel_combo,
            &QComboBox::currentTextChanged,
            this,
            [&, type_edit](const QString& model) { model_changed(model, type_edit);});

        model_changed(panel_combo->currentText(), type_edit);


        QVBoxLayout *type_layout = new QVBoxLayout;
        type_layout->addWidget(panel_combo);
        type_layout->addWidget(type_edit);

        QGroupBox* type_box = new QGroupBox();
        type_box->setTitle("DNA panel");
        type_box->setLayout(type_layout);

        QPlainTextEdit* haplo_edit = new QPlainTextEdit();
        haplo_edit->setPlainText(haplo_type_);
        verify_text_(haplo_type_, haplo_errors_, 0, -1, true);
        connect(
            haplo_edit,
            &QPlainTextEdit::textChanged,
            this,
            [&, haplo_edit, error_edit]() {
                haplo_text_changed(
                    haplo_edit,
                    error_edit);
            });
        QVBoxLayout* haplo_layout = new QVBoxLayout;
        haplo_layout->addWidget(haplo_edit);
        QGroupBox* haplo_box = new QGroupBox();
        haplo_box->setTitle("Haplotype");
        haplo_box->setLayout(haplo_layout);
        
        error_edit->setReadOnly(true);
        error_edit->setBackgroundVisible(false);
        error_edit->setVisible(false);

        QDialogButtonBox* bbox = new QDialogButtonBox(
            QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
        connect(bbox, &QDialogButtonBox::accepted, this, &EnterHaplotypeDialog::accept);
        connect(bbox, &QDialogButtonBox::rejected, this, &EnterHaplotypeDialog::reject);

        QHBoxLayout* hgroup_box = CommonUiElements::setup_haplo_box(
            this, haplo_group_, haplo_subgroup_);

        //fill main vbox
        QVBoxLayout *main_layout = new QVBoxLayout;
        main_layout->addLayout(hgroup_box);
        main_layout->addWidget(type_box);
        main_layout->addWidget(haplo_box);
        main_layout->addWidget(error_edit);
        main_layout->addWidget(bbox);
        setLayout(main_layout);
    }

    EnterHaplotypeDialog::~EnterHaplotypeDialog()
    {
    }

    void EnterHaplotypeDialog::model_changed(
            const QString& new_model,
            QPlainTextEdit* edit) noexcept
    {
        try
        {
            model_name_ = new_model;
            if(new_model == tr("Custom")) {
                edit->setPlainText(custom_model_);
                edit->setReadOnly(false);
                verify_text_(custom_model_, custom_model_errors_, 0, -1, false);
            }
            else {
                edit->setReadOnly(true);
                const dna_manipulation::HaploSet::MarkersLine& markers = 
                    dna_manipulation::HaploSet::get_markers(
                        model_name_.toStdString());
                const std::vector<std::string>& names =
                    dna_manipulation::HaploSet::get_supported_marker_names();
                QString marker_text;
                for(const auto& marker : markers)
                {
                    if(!marker_text.isEmpty())
                        marker_text += ' ';
                    marker_text +=
                        QString::fromStdString(names[static_cast<std::size_t>(marker)]);
                }
                edit->setPlainText(marker_text);
            }
        }
        catch(const dna_manipulation::LibException& e)
        {
            std::cerr << e.what() << std::endl;
        }
    }

    bool EnterHaplotypeDialog::verify_text_(
        const QString& text,
        Errors& errors,
        int pos1,
        int pos2,
        bool haplo_check) noexcept
    {
        if(pos2 == -1)
            pos2 = text.length();
        else if(pos1 > pos2) {
            std::swap(pos1, pos2);
            pos2++;
        }
        pos2 = std::min(pos2, text.length());
        //remove old_errors
        for(auto it = errors.lower_bound(pos1);
                it != errors.end() && it->first < pos2;)
            errors.erase(it++);
        for(auto it = errors.lower_bound(text.length()); it != errors.end();)
            errors.erase(it++);

        for(int i = pos1; i < pos2; ++i) {
            QString error;
            if(!text[i].isPrint()) {
                QTextStream err(&error);
                err << i << tr(": not print character '") << text[i]
                    << '\''  << endl;
                errors[i] = error;
            }
            else if(text[i].isLetterOrNumber()) {
                if(text[i].unicode() > 127) {
                    QTextStream err(&error);
                    err << i << tr(": not valid character '") << text[i]
                        << '\''  << endl;
                    errors[i] = error;
                }
                else if(haplo_check && !text[i].isNumber()) {
                    QTextStream err(&error);
                    err << i << tr(": not valid number '") << text[i]
                        << '\''  << endl;
                    errors[i] = error;
                }
            }
            else if(!text[i].isSpace() && !(haplo_check && text[i] == '-')) {
                QTextStream err(&error);
                err << i << tr(": not valid character '") << text[i]
                    << '\''  << endl;
                errors[i] = error;
            }
        }
        return !errors.empty();
    }

    void EnterHaplotypeDialog::model_text_changed(
        QPlainTextEdit* edit,
        QPlainTextEdit* error_edit,
        bool custom) noexcept
    {
        if(custom) {
            int new_pos = edit->textCursor().position();
            if(new_pos != custom_type_position_) {
                verify_text_(
                    edit->toPlainText(),
                    custom_model_errors_,
                    custom_type_position_,
                    new_pos,
                    false);
                if(!error_edit->toPlainText().isEmpty() ||
                   !custom_model_errors_.empty()) { 
                    error_edit->clear();
                    for(auto it = custom_model_errors_.begin();
                        it != custom_model_errors_.end(); ++it)
                        error_edit->insertPlainText(it->second);
                }
                error_edit->setVisible(!custom_model_errors_.empty());
            }
            custom_type_position_ = new_pos;
            if(custom_model_errors_.empty())
                custom_model_ = edit->toPlainText();
        }
        else {
            error_edit->setVisible(false);
        }
    }

    void EnterHaplotypeDialog::haplo_text_changed(
        QPlainTextEdit* edit,
        QPlainTextEdit* error_edit) noexcept
    {
        int new_pos = edit->textCursor().position();
        if(new_pos != haplo_type_position_)
            verify_text_(
                edit->toPlainText(),
                haplo_errors_,
                haplo_type_position_,
                new_pos,
                true);
        if(!error_edit->toPlainText().isEmpty() || !haplo_errors_.empty()) { 
            error_edit->clear();
            for(auto it = haplo_errors_.begin();
                it != haplo_errors_.end(); ++it)
                error_edit->insertPlainText(it->second);
        }
        error_edit->setVisible(!haplo_errors_.empty());
        haplo_type_position_ = new_pos;
        if(haplo_errors_.empty())
            haplo_type_ = edit->toPlainText();
    }

    bool EnterHaplotypeDialog::get_custom_model(QString& custom_model) const noexcept
    {
        custom_model = custom_model_;
        return model_name_ == tr("Custom");
    }

}
