/**
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include<iostream>
#include<string>
#include<vector>
#include"haploutils.hpp"
#include"haplotype.hpp"

int test(const std::string& haplotype)
{
    std::vector<dna_manipulation::SubString> tokens;
    dna_manipulation::Delimiters delim(" \t-;");
    dna_manipulation::Splitter<> splitter(haplotype, delim);
    dna_manipulation::SubString token;
    while(splitter.get_token(token))
        tokens.push_back(std::move(token));
    try
    {
        dna_manipulation::HaploSet::MarkersLineInfo info =
            dna_manipulation::HaploSet::choose_markers_line(tokens);
        const std::vector<std::string>& marker_names =  dna_manipulation::HaploSet::get_supported_marker_names();
        std::cout << info.name << std::endl;
        for(const auto& marker : info.line)
            std::cout << marker_names[static_cast<size_t>(marker)] << std::endl;
        return 0;
    }
    catch(const dna_manipulation::LibException& e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
}

int main(int argc, char* argv[])
{
    std::string haplotype;
    for(int i = 1; i < argc; ++i) {
        haplotype += argv[i];
        haplotype.push_back(' ');
    }
    return test(haplotype);
}
