/**
  This file is part of sortdna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortdna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include<getopt.h>
#include<cstdlib>
#include<iostream>
#include"haploutils.hpp"

int parse_file(
        const std::string& file,
        unsigned long portion,
        long column,
        bool only_header)
{
    try
    {
        std::vector<std::string> lines;
        dna_manipulation::FileReader in(file.c_str());
        if(only_header)
            portion = 1;
        size_t count, line_num = 0;
        do {
            count = in.read_lines(portion, lines); 
            for(size_t i = 0; i < std::min(portion, lines.size()); ++i)
            {
                std::cout << ++line_num <<  ": ";
                dna_manipulation::Splitter<> splitter(lines[i]);
                dna_manipulation::SubString substr;
                bool add_space = true;
                long c_num = 0;
                while(splitter.get_token(substr)) {
                    if(add_space)
                        std::cout << ' ';
                    if(column == -1 || c_num++ == column) {
                        std::cout <<  substr.str();
                        add_space = true;
                    }
                }
                std::cout << std::endl;
            }
        } while(count > 0 && !only_header);
        return 0;
    }
    catch(const dna_manipulation::LibException& e)
    {
        std::cerr << __func__ << ": " << e.what() << std::endl;
        return 1;
    }
}

int main(int argc, char* argv[])
{
    int only_header = 0;
    static struct option long_options[] =
    {
        { "header", no_argument, &only_header, 1 },
        { "file", required_argument, 0, 'f' },
        { "portion", required_argument, 0, 'p' },
        { "column", required_argument, 0, 'c' },
        { 0, 0, 0, 0 }
    };
    int option_index, option = 0;
    std::string file;
    unsigned long portion = 128;
    long column = -1;
    while(option != -1)
    {
        option = getopt_long(
            argc, argv, "f:p:c:", long_options, &option_index);
        switch(option)
        {
            case 'f':
                file = optarg;
                break;
            case 'p':
                portion = atol(optarg);
                break;
            case 'c':
                column = atol(optarg);
                break;
            case 0:
            case -1:
            default:
                break;
        }
    }
    if(file.empty())
        std::cout << "Usage: " << argv[0] <<
            " --file=file_name -p porton -c column --only-header" << std::endl;
    else
        return parse_file(file, portion, column, only_header == 1);
    return 0;
}
