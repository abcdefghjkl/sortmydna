/**
  file: dbviewmodel.cpp 2015-2019 Andrey Gusev
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include<unordered_map>
#include"haplotype.hpp"
#include"dbviewmodel.hpp"


namespace sortmydna
{
    const std::vector<QString> DbViewModel::marker_names_ = DbViewModel::generate_all_names();

    class StringFactory
    {
    public:
        static QString create(const std::string& val);
        template<class T>
        static QString create(T val)
        {
            return create(std::to_string(val));
        }
    private:
        static std::unordered_map<std::string, QString> shorts_;
    };

    std::unordered_map<std::string, QString> StringFactory::shorts_;

    QString StringFactory::create(const std::string& val)
    {
        if(val.size() <= 2)
        {
            auto it = shorts_.find(val);
            if(it == shorts_.end())
                it = shorts_.insert({val, QString(val.c_str())}).first;
            return it->second;
        }
        return QString(val.c_str());
    }

    //QString StringFactory::create(unsigned char val)


    DbViewModel::DbViewModel(const dna_manipulation::HaploSet::MarkersLine& markers)
        : markers_(markers)
    {
    }

    int DbViewModel::rowCount(const QModelIndex&) const
    {
        return db_ptr_? static_cast<int>(db_ptr_->size()) : 0;
    }

    int DbViewModel::columnCount(const QModelIndex&) const
    {
        return markers_.size() + c_additional_headers;
    }

    QVariant DbViewModel::data(const QModelIndex &index, int role) const
    {
        auto column_index = static_cast<size_t>(index.column());
        if(!index.isValid() || role != Qt::DisplayRole || column_index >= markers_.size() + c_additional_headers)
            return QVariant();
        const auto& record = (*db_ptr_)[index.row()];
        if(column_index == get_distance_index())
            return StringFactory::create(db_ptr_->get_distance(index.row()));
        if(column_index == get_info_index())
        {
            std::string info_line = record.info + record.snp;
            if(info_line.size() > 256) {
                //qt bug, for long strings longer than 4120
                //and very long strings aren't usable in UI
                info_line.resize(256);
            }
            return QString(info_line.c_str());
        }
        return StringFactory::create(record[markers_[column_index]]);
    }

    QVariant DbViewModel::headerData(int section, Qt::Orientation orientation, int role) const
    {
        if(role == Qt::DisplayRole || role == Qt::ToolTipRole)
        {
            if(orientation == Qt::Horizontal && static_cast<size_t>(section) < markers_.size() + c_additional_headers)
            {
                auto index = static_cast<size_t>(section) >= markers_.size() ?
                    marker_names_.size() - c_additional_headers + section - markers_.size() : markers_[section];
                return marker_names_[index];
            }
            if(orientation == Qt::Vertical && db_ptr_)
            {
                return QString::fromStdString((*db_ptr_)[section].id);
            }
        }
        return QVariant();
    }

    void DbViewModel::set_db(SharedDbPtr ptr) noexcept
    {
        beginResetModel();
        db_ptr_ = ptr;
        endResetModel();
    }

    void DbViewModel::change_sort_order(dna_manipulation::SortedDB::SortIndex index) noexcept
    {
        if(db_ptr_ && db_ptr_->get_sort_index() != index)
        {
            beginResetModel();
            db_ptr_->switch_index(index);
            endResetModel();
        }
    }

    void DbViewModel::change_markers(dna_manipulation::HaploSet::MarkersLine&& markers) noexcept
    {
        if(markers != markers_)
        {
            beginResetModel();
            markers_ = std::move(markers);
            endResetModel();
        }
    }

    const QString DbViewModel::column_name_(const std::string& name)
    {
        QString item;
        for(size_t j = 0; j < name.size(); ++j) {
            if(j)
                item += '\n';
            item += name[j];
        }
        return item;
    }

    QStringList DbViewModel::get_marker_names(
        const dna_manipulation::HaploSet::MarkersLine& markers)
    {
        QStringList headers;
        for(size_t i = 0; i < markers.size(); ++i)
            headers << marker_names_[markers[i]];
        return headers;
    }

    std::vector<QString> DbViewModel::generate_all_names()
    {
        std::vector<QString> names;
        const auto& market_names = dna_manipulation::HaploSet::get_supported_marker_names();
        names.reserve(market_names.size() + DbViewModel::c_additional_headers);
        for(const auto& name : market_names)
            names.emplace_back(column_name_(name.c_str()));
        names.emplace_back(column_name_(tr("distance").toStdString()));
        names.emplace_back(column_name_(tr("info").toStdString()));
        return names;
    }

}
