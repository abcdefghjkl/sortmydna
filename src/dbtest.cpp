#include<unistd.h>
#include<sstream>
#include<iostream>
#include<string>
#include"sqlite3_backend.hpp"
#include"exception.hpp"

int test_db(const std::string& db_path)
{
    int ret_code = 0;
    try
    {
        dna_manipulation::MarkersRecord rec;
        dna_manipulation::MarkersRecords recs;
        rec.id = "my_id";
        rec.origin  = "India";
        rec.category = "X-100";
        rec.haplogroup = "I";
        rec.subclad = "1-Z63";
        rec.snp =  "Z63+";
        rec.info = "test record";
        for(size_t i = 0; i < dna_manipulation::SupportedMarkers::MAX; ++i)
            rec.markers[i] = 5 + i % 30;
        sortmydna::SQLiteBackend backend(db_path);
        backend.fill_default();
        auto models = backend.read_model();
        for(const auto& model : *models)
        {
            std::cout << "read model: " << model.id << ' ' << model.name << ' ' << model.count_markers << std::endl;
            auto one_model = backend.read_model(&model.name);
            if(one_model->size() != 1)
            {
                std::ostringstream err;
                err << "expected one model name:'" << model.name << "', got " << one_model->size();
                throw sortmydna::AppException(err.str());
            }
            const auto& cmp_model = one_model->front();
            if(model.id != cmp_model.id){
                std::ostringstream err;
                err << "Models id are different expected:" << model.id << ", got: " << cmp_model.id;
                throw sortmydna::AppException(err.str());
            }
            if(model.name != cmp_model.name){
                std::ostringstream err;
                err << "Models name are different expected:'" << model.name << "', got:'" << cmp_model.name << "'";
                throw sortmydna::AppException(err.str());
            }
            if(model.count_markers != cmp_model.count_markers){
                std::ostringstream err;
                err << "Models count_markers are different expected:"
                    << model.count_markers << ", got: " << cmp_model.count_markers;
                throw sortmydna::AppException(err.str());
            }
            if(model.line != cmp_model.line){
                std::ostringstream err;
                err << "Models line are different expected:"
                    << model.line.size() << ", got: " << cmp_model.line.size();
                throw sortmydna::AppException(err.str());
            }
            //std::cout << model.line << std::endl;
        }
        const auto& model = models->front();
        auto new_line = model.line;
        const std::string name = "test_model";
        new_line.resize(6);
        std::cout << "write: '" << name << "' model" << std::endl;
        backend.write_model(name, new_line);
        models = backend.read_model(&name);
        if(models->size() != 1)
        {
            std::ostringstream err;
            err << "expected one model name:'" << name << "', got " << models->size();
            throw sortmydna::AppException(err.str());
        }
        const auto& new_model = models->front();
        if(new_model.name != name || new_model.count_markers != 6 || new_model.line != new_line)
        {
            std::ostringstream err;
            err << "Models name are different expected:'" << name << "', got:'" << new_model.name << "'";
            throw sortmydna::AppException(err.str());
        }
        backend.write_record(std::string(), std::string(), rec);
        backend.read_records(std::string(), std::string(), recs);
        if(recs.size() != 1)
        {
            std::cerr << "exprected 1 record, got " << recs.size() << std::endl;
            ret_code = 1;
        }
        for(auto it = recs.begin(); it != recs.end() && ret_code == 0; ++it) {
            if(it->origin != rec.origin) {
                std::cerr << "exprected " << rec.origin
                    << " got " << it->origin;
                ret_code = 1;
            }
            if(it->category != rec.category) {
                std::cerr << "exprected " << rec.category
                    << " got " << it->category;
                ret_code = 1;
            }
            if(it->haplogroup != rec.haplogroup) {
                std::cerr << "exprected " << rec.haplogroup
                    << " got " << it->haplogroup;
                ret_code = 1;
            }
            if(it->subclad != rec.subclad) {
                std::cerr << "exprected " << rec.subclad
                    << " got " << it->subclad;
                ret_code = 1;
            }
            if(it->snp != rec.snp) {
                std::cerr << "exprected " << rec.snp
                    << " got " << it->snp;
                ret_code = 1;
            }
            if(it->info != rec.info) {
                std::cerr << "exprected " << rec.info
                    << " got " << it->info;
                ret_code = 1;
            }
            for(size_t i = 0; i < dna_manipulation::SupportedMarkers::MAX; ++i)
                if(rec.markers[i] != it->markers[i]) {
                    std::cerr << "markers at position " << i
                        << "are different :"
                        << rec.markers[i] << " != " 
                        << it->markers[i] << std::endl;
                    ret_code = 1;
                }
        }
        //backend.read_records(group, subgroup, recs);
    }
    catch(const sortmydna::AppException& e)
    {
        std::cerr << e.what() << std::endl;
        ret_code = 1;
    }
    if(unlink(db_path.c_str())){
        std::cerr << "Can't unlink file '" << db_path << std::endl;
        ret_code = 1;
     }
 return ret_code;
}

int main(int argc, char* argv[])
{
    std::string db_path = "db.test";
    if(argc > 1)
        db_path = argv[1];
    return test_db(db_path);
}
