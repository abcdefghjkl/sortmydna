#ifndef SORT_MY_DNA_COMMON_UI_ELEMENTS_HPP
#define SORT_MY_DNA_COMMON_UI_ELEMENTS_HPP

class QString;
class QWidget;
class QHBoxLayout;
namespace sortmydna
{
    class CommonUiElements
    {
    public:
        static QHBoxLayout* setup_haplo_box(
            const QWidget* widget, QString& group, QString& subgroup);

    };
}

#endif
