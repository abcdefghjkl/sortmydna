/**
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include<QLineEdit>
#include<QComboBox>
#include<QWidget>
#include<QHBoxLayout>
#include"commonuielements.hpp"
#include"haplotype.hpp"

namespace sortmydna
{
    QHBoxLayout* CommonUiElements::setup_haplo_box(
        const QWidget* widget, QString& group, QString& subgroup)
    {
        QComboBox* type_combo = new QComboBox();
        const auto& groups = dna_manipulation::HaploSet::get_haplogroups();
        for(const auto& name : groups)
            type_combo->addItem(name.c_str());
        if(!group.isEmpty())
            type_combo->setCurrentText(group);
        else
            group = type_combo->currentText();
        QObject::connect(
            type_combo,
            &QComboBox::currentTextChanged,
            widget,
            [&group](const QString& val) { group = val;});

        QLineEdit* type_edit = new QLineEdit();

        if(!subgroup.isEmpty())
            type_edit->setText(subgroup);

        QObject::connect(
            type_edit,
            &QLineEdit::textChanged,
            widget,
            [&subgroup, type_edit] {subgroup = type_edit->displayText();});
        QHBoxLayout *type_layout = new QHBoxLayout;
        type_layout->addWidget(type_combo);
        type_layout->addWidget(type_edit);
        return type_layout;
    }
}
