/**
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include"haploutils.hpp"
#include<fstream>
#include<sstream>
#include<iostream>

namespace dna_manipulation
{
    const char* space_delimiters = " \t";
    const Delimiters s_delim(space_delimiters);

    FileReader::FileReader(const char* name)
    {
        ifs_.open(name);
        if(ifs_.fail())
        {
            std::ostringstream ostr;
            ostr << __func__ << ": fail to open file '" << name << "'";
            throw LibException(ostr.str());
        }
        ifs_.exceptions(std::ifstream::badbit);
    }

    size_t FileReader::read_lines(size_t count, std::vector<std::string>& lines)
    {
        lines.clear();
        lines.resize(count);
        size_t rd = 0;
        try
        {
            while(ifs_.good() && !ifs_.eof() && rd < count)
            {
                std::getline(ifs_, lines[rd]);
                if(!lines[rd].empty())
                    rd++;
            }
            lines.resize(rd);
            return rd;
        }
        catch(const std::ios_base::failure& e)
        {
            lines.resize(rd);
            std::ostringstream ostr;
            ostr << __func__ << ": fail to read " << rd << " line " << e.what();
            throw LibException(ostr.str());
        }
    }

    FileReader::~FileReader() noexcept
    {
        try
        {
            ifs_.close();
        }
        catch(const std::ifstream::failure e)
        {
            std::cerr << __func__ << " exception on closing stream: "
                << e.what() << std::endl;
        }
    }
    

}
