/**
  This file is part of sortdna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortdna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include"helpdialog.hpp"
#include<sortmydnaconfig.h>
#include<QPushButton>
#include<QVBoxLayout>
#include<QTextEdit>
#include<sstream>

namespace sortmydna
{
    AboutDialog::AboutDialog()
    : QDialog(Q_NULLPTR)
    {
        QVBoxLayout *main_layout = new QVBoxLayout;
        std::ostringstream message;
        message << "<b>" << PROJECT_NAME << ' '
            << SORTMYDNA_VERSION_MAJOR << '.'
            << SORTMYDNA_VERSION_MINOR << '.'
            << SORTMYDNA_VERSION_REVISION << "</b>" << std::endl
            << PROJECT_NAME << " is a finder of similar haplotypes and builder of trees" << std::endl
            << "Copyright @ 2016 Andrey Gusev " << std::endl
            << PROJECT_NAME << " is free software, "
            "you can redistribute it " << std::endl
            << "and/or modify it under the terms of the GNU " << std::endl
            << "General Public Licensee as published by " << std::endl
            << "the Free Software Foundation, either " << std::endl
            << "version 3 of License, or (at your option) "
            << "any later version." << std::endl
            << "This program is distributed in the hope that " << std::endl
            << "it will be useful, but WITHOUT ANY WARRANTY; " << std::endl
            << "without even the implied warranty of MERCHANTABILITY " << std::endl
            << "or FITNESS FOR A PARTICULAR PURPOSE.  See the " << std::endl
            << "GNU General Public License for more details.";
        QTextEdit* edit = new QTextEdit(QString::fromStdString(message.str()));
        edit->setReadOnly(true);
        main_layout->addWidget(edit);
        QPushButton *button= new QPushButton("Close");
        main_layout->addWidget(button);
        connect(button, &QAbstractButton::clicked, this, &AboutDialog::close);
        setLayout(main_layout);
    }
}
