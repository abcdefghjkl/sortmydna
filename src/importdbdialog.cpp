/**
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include<cmath>
#include"sortmydnaconfig.h"
#include"importdbdialog.hpp"
#include"haplotype.hpp"
#include"exception.hpp"
#include"commonuielements.hpp"
#include<iostream>
#include<sstream>
#include<cctype>
#include<QHBoxLayout>
#include<QVBoxLayout>
#include<QHeaderView>
#include<QLineEdit>
#include<QComboBox>
#include<QStringList>
#include<QPushButton>
#include<QGroupBox>
#include<QLabel>
#include<QCheckBox>
#include<QFileDialog>
#include<QStandardPaths>
#include<QTableWidget>
#include<QGridLayout>
#include<QCheckBox>
#include<QRadioButton>
#include<QButtonGroup>
#include<QDialogButtonBox>
#include<QScrollArea>
#include<QPlainTextEdit>
#include<QStatusBar>
#include<QTextStream>

namespace sortmydna
{
    ImportDbDialog::ImportDbDialog(QWidget* parent, AppConfig& config)
    : QDialog(parent),
      config_(config),
      db_(new dna_manipulation::MarkersDB)
    {
        QString dialogname = PROJECT_NAME;
        dialogname += tr(" - import haplotypes from file");
        setWindowTitle(dialogname);

        QHBoxLayout* type_layout = CommonUiElements::setup_haplo_box(
            this, config_.import_group, config_.import_subgroup);

        /*
        QRadioButton* radio1 =
            new QRadioButton(tr("Plain text"));
        radio1->setChecked(true);
        connect(radio1, &QRadioButton::toggled, this, [this] () {csv_file_ = false;});
        QRadioButton* radio2 =
            new QRadioButton(tr("CSV"));
        connect(radio2, &QRadioButton::toggled, this, [this] () {csv_file_ = true;});
        */
        QHBoxLayout* delim_layout = setup_delim_layout_();

        QLabel *f_label = new QLabel("...");
        QPushButton *f_button = new QPushButton(tr("Select file"));
        connect(f_button, &QAbstractButton::clicked, this,
                [&, f_label] () { select_file_name(f_label);});

        QPushButton *i_button = new QPushButton(tr("Import"));
        connect(
            i_button,
            &QAbstractButton::clicked,
            this,
            &ImportDbDialog::import_file);
                //[&, f_label] () { select_file_name(f_label);});
        QHBoxLayout *buttons_layout = new QHBoxLayout;
        buttons_layout->addWidget(f_button);
        buttons_layout->addWidget(f_label);
        buttons_layout->addWidget(i_button);

        QCheckBox* check_box = new QCheckBox(tr("Get group from file"));
        connect(
            check_box,
            &QCheckBox::stateChanged,
            this,
            [this] (int state) { this->get_group_from_file_ = (state != Qt::Unchecked);});

        QVBoxLayout *group_layout = new QVBoxLayout;
        group_layout->addLayout(type_layout);
        group_layout->addWidget(check_box);

        QGroupBox* haplo_box = new QGroupBox();
        haplo_box->setTitle(tr("Haplotype"));
        haplo_box->setLayout(group_layout);

        QGroupBox* delim_box = new QGroupBox();
        delim_box->setTitle(tr("Delimiters"));
        delim_box->setLayout(delim_layout);

        QHBoxLayout *haplo_layout = new QHBoxLayout;
        haplo_layout->addWidget(haplo_box);
        haplo_layout->addWidget(delim_box);

        QDialogButtonBox* bbox = new QDialogButtonBox(
            QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
        connect(bbox, &QDialogButtonBox::accepted, this, &ImportDbDialog::accept);
        connect(bbox, &QDialogButtonBox::rejected, this, &ImportDbDialog::reject);

        error_edit_ = new QPlainTextEdit();
        error_edit_->setReadOnly(true);
        error_edit_->setBackgroundVisible(false);
        error_edit_->setVisible(false);

        status_bar_ = new QStatusBar(this);
        status_bar_->showMessage(tr("Set parameters"));

        QVBoxLayout *main_layout = new QVBoxLayout;
        main_layout->addLayout(haplo_layout);
        main_layout->addWidget(error_edit_);
        main_layout->addLayout(buttons_layout);
        main_layout->addWidget(status_bar_);
        main_layout->addWidget(bbox);
        setLayout(main_layout);
    }

    QHBoxLayout* ImportDbDialog::setup_delim_layout_()
    {
        QStringList items = {tr("Space&Tab"), tr("Space"), tr("Tab"), tr("Semicolon")};
        QComboBox* delim_combo =  new QComboBox;
        delim_combo->addItems(items);
        QLineEdit* edit = new QLineEdit;
        edit->setText(delim_);
        connect(edit, &QLineEdit::textChanged, this,
            [this, edit] () { delim_ = edit->text(); });
        connect(
            delim_combo,
            &QComboBox::currentTextChanged,
            this,
            [this, edit] (const QString& text) {
                if(text == tr("Space&Tab"))
                    delim_ = " \t";
                else if(text == tr("Space"))
                    delim_ = " ";
                else if(text == tr("Tab"))
                    delim_ = "\t";
                else if(text == tr("Semicolon"))
                    delim_ = ";";
                edit->setText(delim_);
            });
        QHBoxLayout* res = new QHBoxLayout;
        res->setAlignment(Qt::AlignTop);
        res->addWidget(delim_combo);
        res->addWidget(edit);
        return res;
    }

    void ImportDbDialog::create_text_postfix_(const Model& model)
    {
        config_.import_text_postfix.clear();
        for(size_t i = 0; i < model.size(); ++i) {
            config_.import_text_postfix += ' ';
            config_.import_text_postfix += QString::fromStdString(model[i].name);
        }
    }

    ImportDbDialog::ParseResult ImportDbDialog::parse_line(const std::string& line)
    {
        const dna_manipulation::Delimiters delim(delim_.toStdString().c_str());
        dna_manipulation::Splitter<> splitter(line, delim);
        dna_manipulation::SubString sub;
        std::list<dna_manipulation::SubString> tokens;
        size_t count_fields  = 0;
        while(splitter.get_token(sub))
        {
            tokens.push_back(std::move(sub));
            ++count_fields;
        }
        ModelPtr model(Model::create_model(tokens));
        if(!model->count_fields)
            return PR_FAIL;
        ModelDialog::BaseModelContainer models;
        auto it = models_.begin();
        while(it != models_.end())
        {
            switch(model->check_model(**it))
            {
                case Model::PR_SUCCESS:
                    if((*it)->accept_data == Model::PR_CANCEL)
                        return PR_FAIL;
                    if(model->data){
                        models.push_back(it->get());
                    }
                    break;
                case Model::PR_FAIL:
                    models.push_back(it->get());
                    break;
                case Model::PR_CANCEL:
                    break;
            }
            ++it;
        }
        ModelPtr new_model;//assume model
        if(model->data) {
            if(models.empty()) {
                try
                {
                    new_model = model->create_base_model();
                    models.push_back(new_model.get());
                }
                catch(const dna_manipulation::LibException& e)
                {
                    std::ostringstream ostr;
                    error_edit_->setVisible(true);
                    ostr << "fail to parse string '" << line << "': "
                        <<  e.what() << std::endl;
                    error_edit_->insertPlainText(QString::fromStdString(ostr.str()));
                    std::cerr << __func__ << ": " << ostr.str();
                    ++stat_.count_errors;
                    return PR_FAIL;
                }
            }
            else  {
                for(size_t j = 0; j < models.size(); ++j)
                    if(models[j]->accept_data == Model::PR_SUCCESS) {
                        add_data_(*models[j], *model);
                        ++stat_.count_imported;
                        return PR_SUCCESS;
                    }
            }
        }
        bool add_as_postfix = !config_.import_text_postfix.isEmpty();
        std::unique_ptr<ModelDialog> header_dialog(
            new ModelDialog(*model, models, add_as_postfix));
        try
        {
            ImportDbDialog::ParseResult res = PR_FAIL;
            header_dialog->exec();
            ModelDialog::DialogResult dlg_res = header_dialog->result();
            int model_index = header_dialog->get_model_index();
            if(model_index == -1)
                new_model = std::move(header_dialog->get_current_model());
            switch(dlg_res)
            {
                case ModelDialog::DR_ACCEPT:
                case ModelDialog::DR_ACCEPT_ALL:
                    if(model->data) {
                        if(new_model)
                            add_data_(*new_model, *model);
                        else { 
                            add_data_(*models[model_index], *model);
                            if(dlg_res == ModelDialog::DR_ACCEPT_ALL)
                                models[model_index]->accept_data = Model::PR_SUCCESS;
                        }
                        ++stat_.count_imported;
                    }
                    else {
                        if(model->count_markers == 0) {
                            if(add_as_postfix)
                                create_text_postfix_(*model);
                            else
                                config_.import_text_postfix.clear();
                        }
                        else if(!new_model){
                            if(dlg_res == ModelDialog::DR_ACCEPT_ALL)
                                model->accept_data = Model::PR_SUCCESS;
                            else
                               model->accept_data = Model::PR_FAIL;
                            models_.push_back(std::move(model));
                        }
                    }
                    res = PR_SUCCESS;
                    break;
                case ModelDialog::DR_CANCEL:
                    db_.reset(new dna_manipulation::MarkersDB);
                    res = PR_CANCEL;
                    break;
                case ModelDialog::DR_SKIP:
                    res = PR_FAIL;
                    break;
                case ModelDialog::DR_SKIP_ALL:
                    if(model->data) {
                        if(!new_model && model_index >= 0)
                            models[model_index]->accept_data = Model::PR_CANCEL;
                    }
                    else {
                        model->accept_data = Model::PR_CANCEL;
                        if(model_index == -1)
                            models_.push_back(std::move(model));
                    }
                    res = PR_FAIL;
                    break;
            }
            if(new_model) {
                if(dlg_res == ModelDialog::DR_ACCEPT_ALL)
                    new_model->accept_data = Model::PR_SUCCESS;
                else if(dlg_res == ModelDialog::DR_ACCEPT)
                    new_model->accept_data = Model::PR_FAIL;
                else if(dlg_res == ModelDialog::DR_SKIP_ALL)
                    new_model->accept_data = Model::PR_CANCEL;
                if(model->count_markers > 0)
                   models_.push_back(std::move(new_model));
            }
            return res;
        }
        catch(const AppException& e)
        {
            ++stat_.count_errors;
            std::cerr << __func__ << ": " << e.what() << std::endl;
            return PR_CANCEL;
        }
    }

    void ImportDbDialog::add_data_(const Model& base_model, Model& model)
    {
        dna_manipulation::MarkersRecord rec;
        size_t j = 0;//index in base model
        for(size_t i = 0; i < model.size(); ++i) {
            switch(model[i].type)
            {
                case FieldInfo::FT_ID:
                    rec.id.swap(model[i].name);
                    break;
                case FieldInfo::FT_ORIGIN:
                    rec.origin.swap(model[i].name);
                    break;
                case FieldInfo::FT_CATEGORY:
                    rec.category.swap(model[i].name);
                    break;
                case FieldInfo::FT_HAPLOGROUP:
                    if(!model[i].name.empty()) {
                        const auto& groups =
                            dna_manipulation::HaploSet::get_haplogroups();
                        auto it = groups.end();
                        for(auto it2 = groups.begin(); it2 != groups.end(); ++it2)
                            if(*it2 <= model[i].name) 
                                it = it2;

                        if(it != groups.end()) {
                            rec.haplogroup = *it;
                            if(model[i].name.size() > rec.haplogroup.size()) {
                                size_t off = rec.haplogroup.size();
                                if(model[i].name[off] == '-' ||
                                   model[i].name[off] == '_')
                                    ++off;
                                rec.subclad.assign(
                                    model[i].name, off, model[i].name.size() - off);
                            }
                        }
                        else
                            rec.haplogroup.swap(model[i].name);
                    }
                    break;
                case FieldInfo::FT_SNP:
                    rec.snp.swap(model[i].name);
                    break;
                case FieldInfo::FT_INFO:
                    if(rec.info.empty())
                        rec.info.swap(model[i].name);
                    else {
                        rec.info.push_back(' ');
                        rec.info += model[i].name;
                    }
                    break;
                case FieldInfo::FT_IGNORY:
                    break;
                case FieldInfo::FT_MARKER:
                    while(base_model[j].type != FieldInfo::FT_MARKER)
                        if(++j == base_model.size())
                            throw AppException("can't find marker in base model");
                    unsigned char ind = base_model[j].name[0];
                    rec.markers[ind] =
                        static_cast<unsigned char>(std::stoi(model[i].name));
                    ++j;
                    break;
            }
        }
        if(!config_.import_text_postfix.isEmpty())
            rec.info += config_.import_text_postfix.toStdString();
        db_->add_record(std::move(rec));
    }

    void ImportDbDialog::update_status_bar_() noexcept
    {
        QString status_str;
        QTextStream status(&status_str);
        status << stat_.count_imported << " ";
        status << tr("imported");
        status << ", ";
        status << stat_.count_errors;
        status << " " << tr("errors");
        status_bar_->showMessage(status_str);
    }

    void ImportDbDialog::import_file() noexcept
    {
        try
        {
            if(file_name_.isEmpty())
                return;
            status_bar_->showMessage(tr("Statrt importing"));
            std::vector<std::string> lines;
            dna_manipulation::FileReader reader(file_name_.toStdString().c_str());
            ParseResult result;
            while(reader.read_lines(1024, lines)) {
                for(auto& elem : lines) {
                        result = parse_line(elem);
                        update_status_bar_();
                        if(result == PR_CANCEL) {
                            status_bar_->showMessage(tr("Cancel importing"));
                            break;
                        }
                        //if(result == PR_SUCCESS)
                }
                    if(result == PR_CANCEL)
                        return;//exception here?
            }
            //db_->debug_print();
        }
        catch(const dna_manipulation::LibException& e)
        {
            status_bar_->showMessage(tr("Error importing"));
            std::cerr << __func__ << ": " << e.what() << std::endl;
        }
    }

    void ImportDbDialog::select_file_name(QLabel* label) noexcept
    {
        file_name_ = QFileDialog::getOpenFileName(
            this,
            tr("Open file"),
            QStandardPaths::standardLocations(QStandardPaths::HomeLocation).at(0));
        label->setText(file_name_);
    }

    ModelDialog::ModelDialog(
        Model& info, //current model
        const BaseModelContainer& models,
        bool& postix) noexcept
        : QDialog(Q_NULLPTR),
          info_(info),
          add_as_posfix_(postix),
          models_(models)
    {
        if(!models.empty()) {
            current_model_ = 0;
            info_.adapt_model(*models_[current_model_]);
        }
        QVBoxLayout* main_layout = new QVBoxLayout;

        const std::unordered_set<std::string>& simple_markers =
            dna_manipulation::HaploSet::get_simple_markers();

        size_t count_columns = simple_markers.size() + dna_manipulation::HaploSet::count_multi_markers;

        QTableWidget* other_data = setup_info_table_();


        QHBoxLayout *other_layout = new QHBoxLayout;
        other_layout->addWidget(other_data);
        main_layout->addLayout(other_layout);

        std::unique_ptr<QTableWidget> markers_holder(new QTableWidget(count_columns, 3));
        QTableWidget* markers = markers_holder.get();

        if(info_.count_markers > 0) {

            static const QStringList c_names2 = { tr("Column"), tr("Data"), tr("Marker")};
            markers->setHorizontalHeaderLabels(c_names2); 
            if(models_.empty()) 
                markers->setColumnCount(2);
            else {
                QHBoxLayout *combo_layout = setup_models_combo_(markers);
                main_layout->addLayout(combo_layout);
            }

            main_layout->addWidget(markers);
            markers_holder.release();//qt responsible for dealocation
        }
        else {
            QCheckBox* check_box = new QCheckBox("add this information to the next chunk of haplotypes");
            check_box->setChecked(add_as_posfix_);
            connect(
                check_box,
                &QCheckBox::stateChanged,
                this,
                [this] (int state) { this->add_as_posfix_ = (state != Qt::Unchecked);});
            main_layout->addWidget(check_box);
        }

        int info_index = 0, marker_index = 0, model_index = 0;
        for(auto col_num = 0; col_num < static_cast<int>(info_.size()); ++col_num) {
            if(info_[col_num].type != FieldInfo::FT_MARKER) {
                add_info_row_(other_data, col_num, info_index);
                ++info_index;
            }
            else {
                add_marker_row_(markers, col_num, marker_index, model_index); 
                ++marker_index;
            }
        }
        markers->setRowCount(marker_index);
        QCheckBox* apply_box = new QCheckBox(tr("Apply to similar data"));
        apply_box->setChecked(false);
        main_layout->addWidget(apply_box);
        QHBoxLayout *but_layout = new QHBoxLayout;
        QPushButton *c_button = new QPushButton(tr("Cancel import"));
        QPushButton *s_button = new QPushButton(tr("Skip line"));
        QPushButton *d_button = new QPushButton(tr("Accept line"));
        connect(d_button, &QAbstractButton::clicked, this, [this, apply_box] () {
            close_dialog(apply_box->isChecked() ? DR_ACCEPT_ALL : DR_ACCEPT);});
        connect(s_button, &QAbstractButton::clicked, this, [this, apply_box] () {
            close_dialog(apply_box->isChecked() ? DR_SKIP_ALL : DR_SKIP);});
        connect(c_button, &QAbstractButton::clicked, this, [this] () {
            close_dialog(DR_CANCEL);});
        but_layout->addWidget(c_button);
        but_layout->addWidget(s_button);
        but_layout->addWidget(d_button);
        main_layout->addLayout(but_layout);
        //std::cout << col_num << std::endl;
        setLayout(main_layout);
        //setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    }

    void ModelDialog::showEvent(QShowEvent * event)
    {
        QDialog::showEvent(event);
        auto sz = size();
        sz.setHeight(sz.height() * 2);
        sz.setWidth(sz.width() * 2);
        resize(sz);
        //adjustSize();
    }

    QTableWidget* ModelDialog::setup_info_table_() noexcept
    {
        static const QStringList c_names = {
            tr("Column"), tr("Name"), tr("Id"), tr("Origin"), tr("Category"),
            tr("haplogroup"), tr("snp"), tr("info")
        };
        QTableWidget* info_table = new QTableWidget(info_.count_text, c_names.size());

        info_table->setHorizontalHeaderLabels(c_names); 
        //info_table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        info_table->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        return info_table;
    }

    QHBoxLayout* ModelDialog::setup_models_combo_(
        QTableWidget* markers_table) noexcept
    {
        QString label_name = tr("Model:");
        if(models_.size() == 1) {
            label_name.append(' ');
            label_name +=  QString::fromStdString((*models_.begin())->name);
        }
        QLabel* combo_label = new QLabel(label_name);
        QHBoxLayout *combo_layout = new QHBoxLayout;
        combo_layout->addWidget(combo_label);
        if(models_.size() > 1) {
            QComboBox* model_combo =  new QComboBox;
            for(auto it = models_.begin(); it != models_.end(); ++it)
            {
                QString name = QString::fromStdString((*it)->name);
                model_combo->addItem(name);
            }
            connect(
                model_combo,
                static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
                this,
                [this, markers_table] (int index){ model_changed(markers_table, index);});

            combo_layout->addWidget(model_combo);
        }
        return combo_layout;
    }

    void ModelDialog::change_info_state(
        QTableWidget* info_table,
        int state,
        int model_index,
        int row_index) noexcept
    {
        FieldInfo::FieldType type = FieldInfo::FT_IGNORY;
        if(state != Qt::Unchecked) {
            type = FieldInfo::FT_INFO;
            for(int i = 2; i < info_table->columnCount(); ++i) { 
                QCheckBox* widget = static_cast<QCheckBox*>(
                    info_table->cellWidget(row_index, i));
                if(widget->isChecked()) {
                    if(i - 2 < FieldInfo::FT_INFO)
                        type = static_cast<FieldInfo::FieldType>(i - 2);
                    break;
                }
            }
        }
        info_[model_index].type = type;
    }

    void ModelDialog::change_info_type(
        QTableWidget* info_table,
        int model_index,
        int row_index,
        int col_index) noexcept
    {
        const int snp_col_index = 2 + FieldInfo::FT_SNP;
        if(col_index < snp_col_index) {
            const int info_col_index = 2 + FieldInfo::FT_INFO;
            for(int i = 0; i < info_table->rowCount(); ++i)
                if(i != row_index) { 
                    QRadioButton* widget = static_cast<QRadioButton*>(
                        info_table->cellWidget(i, col_index));
                    if(widget->isChecked()) {
                        widget = static_cast<QRadioButton*>(
                            info_table->cellWidget(i, info_col_index));
                        widget->setChecked(true);
                        break;
                    }
                }
        }
        if(info_[model_index].type != FieldInfo::FT_IGNORY)
            info_[model_index].type =
                static_cast<FieldInfo::FieldType>(col_index - 2);
    }

    void ModelDialog::add_info_row_(
        QTableWidget* info_table,
        int model_index,
        int row_index) noexcept
    {
        QCheckBox* enable = new QCheckBox(QString("%1").arg(model_index + 1));
        enable->setChecked(true);
        info_table->setCellWidget(row_index, 0, enable);
        connect(
            enable,
            &QCheckBox::stateChanged,
            this,
            [this, model_index, row_index, info_table] (int state)
            { change_info_state(info_table, state, model_index, row_index);});

        QString qname(info_[model_index].name.c_str());
        QTableWidgetItem* item = new QTableWidgetItem(qname);
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        info_table->setItem(row_index, 1, item);

        int tg = info_[model_index].type + 2;
        QButtonGroup* group = new QButtonGroup(info_table);
        for(int i = 2; i < info_table->columnCount(); ++i) { 
            QRadioButton* radio = new QRadioButton;
            group->addButton(radio);
            if(i == tg)
                radio->setChecked(true);

            connect(
                radio,
                &QRadioButton::toggled,
                this,
                [this, model_index, row_index, info_table, i] (bool checked)
                {if(checked) change_info_type(info_table, model_index, row_index, i);});
            info_table->setCellWidget(row_index, i, radio);
        }
    }

    void ModelDialog::add_marker_row_(
        QTableWidget* markers_table,
        int marker_index,
        int row_index,
        int& changed_index) noexcept
    {
        const auto& marker_names = dna_manipulation::HaploSet::get_supported_marker_names();
        QTableWidgetItem* item =
            new QTableWidgetItem(QString("%1").arg(marker_index + 1));
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        markers_table->setItem(row_index, 0, item);

        QString qname;
        if(info_.data)
            qname = QString::fromStdString(info_[marker_index].name);
        else
            qname =  QString::fromStdString(marker_names[info_[marker_index].name[0]]);
        item = new QTableWidgetItem(qname);
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        markers_table->setItem(row_index, 1, item);
        if(!models_.empty()) {
            const auto& model = *models_.front();
            for(;model[changed_index].type != FieldInfo::FT_MARKER &&
                 changed_index < static_cast<int>(model.size());
                 ++changed_index);
            if(changed_index < static_cast<int>(model.size())) {
                qname = QString::fromStdString(
                    marker_names[model[changed_index].name[0]]);
                QPushButton* widget = new QPushButton(qname);
                //widget->setForegroundRole(QPalette::BrightText);
                connect(widget, &QAbstractButton::clicked, this,
                    [this, markers_table, row_index, marker_index] () {
                    marker_button_clicked(markers_table, marker_index, row_index);});
                markers_table->setCellWidget(row_index, 2, widget);
                ++changed_index;
            }
        }
    }

    void ModelDialog::marker_changed(QTableWidget* table, QComboBox* combo, int index)
    {
        try
        {
            const auto& indexes = dna_manipulation::HaploSet::get_marker_indexes();
            const auto& names = dna_manipulation::HaploSet::get_supported_marker_names();
            dna_manipulation::SupportedMarkers first_marker = 
                static_cast<dna_manipulation::SupportedMarkers>(
                    indexes.at(combo->currentText().toStdString()));
            std::vector<std::string> cont;
            auto row = table->currentRow() + model_->count_text;
            (*model_)[row++].name[0] = static_cast<unsigned char>(first_marker);
            first_marker = static_cast<dna_manipulation::SupportedMarkers>(
                static_cast<unsigned char>(first_marker) + 1);
            for(auto ind = row; ind < info_.size(); ++ind)
                cont.push_back(info_[ind].name);

            dna_manipulation::HaploSet::MarkersLine markers = dna_manipulation::HaploSet::choose_markers(
                cont,
                dna_manipulation::HaploSet::get_markers(),
                first_marker); 
            
            auto ind = table->currentRow() + 1;
            for(auto it = markers.begin(); it != markers.end(); ++it) {
                QComboBox* widget = static_cast<QComboBox*>(table->cellWidget(ind++, 2));
                bool old_state = widget->blockSignals(true);
                widget->setCurrentText(QString::fromStdString(names[*it]));
                widget->blockSignals(old_state);
                (*model_)[row++].name[0] = *it;
            }
        }
        catch(const dna_manipulation::LibException& e)
        {
            std::cerr << combo->currentText().toStdString() << std::endl;
            std::cerr << __func__ << ": " << e.what() << std::endl;
        }
        catch(const std::exception& e)
        {
            std::cerr << __func__ << ": " << e.what() << std::endl;
        }

    }

    void ModelDialog::model_changed(QTableWidget* widget, int index)
    {
        current_model_ = index;
        update_widget_(widget, *models_[current_model_]);
    }

    void ModelDialog::update_widget_(QTableWidget* widget, const Model& model)
    {
        const auto& marker_names = dna_manipulation::HaploSet::get_supported_marker_names();
        QString qname;
        size_t j = 0;
        for(auto i = 0; i < widget->rowCount(); ++i, ++j) {
            while(model[j].type != FieldInfo::FT_MARKER && j < model.size())
                ++j; 
            if(j == model.size())
                return;
            QPushButton* button =
                static_cast<QPushButton*>(widget->cellWidget(i, 2));
            qname = QString::fromStdString(marker_names[model[j].name[0]]);
            button->setText(qname);
        }
    }

    void ModelDialog::setup_current_model_()
    {
        if(current_model_ >= 0){
            model_.reset(new Model(*models_[current_model_]));
            current_model_ = -1;
        }
    }

    /*
       widget - table
       index - indxex in model
       row_index - index in table
    * */
    void ModelDialog::marker_button_clicked(QTableWidget* widget, int index, int row_index)
    {
        QPushButton* button = static_cast<QPushButton*>(widget->cellWidget(row_index, 2));
        const auto& name = button->text();
        std::unique_ptr<EditModelDialog> model_dlg(
            new EditModelDialog(info_, name, index));
        if(model_dlg->exec() == QDialog::Accepted) {
            setup_current_model_();
            const auto& names = dna_manipulation::HaploSet::get_supported_marker_names();
            dna_manipulation::SupportedMarkers first_marker = 
                static_cast<dna_manipulation::SupportedMarkers>(
                    model_dlg->selected_marker());
            (*model_)[index].name[0] = static_cast<unsigned char>(first_marker);
            button->setText(QString::fromStdString(names[first_marker]));
            first_marker = static_cast<dna_manipulation::SupportedMarkers>(
                static_cast<unsigned char>(first_marker) + 1);
            std::vector<std::string> cont;
            for(size_t ind = index + 1; ind < info_.size(); ++ind)
                if(info_[ind].type == FieldInfo::FT_MARKER)
                    cont.push_back(info_[ind].name);
            dna_manipulation::HaploSet::MarkersLine markers;
            try
            {
                markers = dna_manipulation::HaploSet::choose_markers(
                    cont, dna_manipulation::HaploSet::get_markers(), first_marker); 
                for(auto it = markers.begin(); it != markers.end(); ++it) {
                    button = static_cast<QPushButton*>(widget->cellWidget(++row_index, 2));
                    button->setText(QString::fromStdString(names[*it]));
                    (*model_)[++index].name[0] = *it;
                }
            }
            catch(const dna_manipulation::LibException& e)
            {//update just selected marker
                std::cerr << __func__ << ": " << e.what() << std::endl;
            }
        }
    }

    EditModelDialog::EditModelDialog(
        Model& info, const QString& name, int index)
    {
        static std::map<unsigned char, dna_manipulation::HaploSet::MarkersLine>
            allowed_markers;
        const auto& indexes = dna_manipulation::HaploSet::get_marker_indexes();
        const auto& names = dna_manipulation::HaploSet::get_supported_marker_names();
        unsigned char value = static_cast<unsigned char>(std::stoi(info[index].name));
        auto allowed_it = allowed_markers.find(value);
        if(allowed_it == allowed_markers.end()) {
            allowed_it = allowed_markers.insert(
                    allowed_markers.end(),
                    {value, dna_manipulation::HaploSet::get_possible_markers(value)});
            std::sort(allowed_it->second.begin(), allowed_it->second.end());
        }
        unsigned char first_marker = indexes.at(name.toStdString());
        QString text;
        text = name + ": " + QString::fromStdString(info[index].name);
        marker_ = first_marker;

        QGridLayout* markers_layout = new QGridLayout;

        auto value_it = std::lower_bound(
            allowed_it->second.begin(), allowed_it->second.end(), first_marker);
        size_t count_columns = sqrt(std::max(
            std::distance(allowed_it->second.begin(), value_it),
            std::distance(value_it, allowed_it->second.end()))) + 1;
        auto marker_it = allowed_it->second.begin();
        size_t row = 0;
        do
        {
            for(auto col = 0U; marker_it != allowed_it->second.end() && col < count_columns;
                    ++marker_it, ++col)
            {
                auto marker = *marker_it;
                std::unique_ptr<QRadioButton> radio(
                    new QRadioButton(QString::fromStdString(names[marker])));
                if(marker == first_marker)
                    radio->setChecked(true);
                connect(radio.get(), &QRadioButton::toggled, this, [this, marker] ()
                    {marker_ = marker;});
                markers_layout->addWidget(radio.release(), row, col);
            }
            ++row;
        } while(marker_it != allowed_it->second.end());

        QScrollArea* scrollArea = new QScrollArea;
        QFrame* frame = new QFrame;
        frame->setLayout(markers_layout);
        scrollArea->setWidget(frame);

        QLabel* label = new QLabel(text);

        QVBoxLayout* main_layout = new QVBoxLayout;
        main_layout->addWidget(label);
        main_layout->addWidget(scrollArea);
        //main_layout->addLayout(markers_layout);
        QDialogButtonBox* bbox = new QDialogButtonBox(
            QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
        connect(bbox, &QDialogButtonBox::accepted, this, &EditModelDialog::accept);
        connect(bbox, &QDialogButtonBox::rejected, this, &EditModelDialog::reject);
        main_layout->addWidget(bbox);
        setLayout(main_layout);
    }

    void ModelDialog::close_dialog(DialogResult status) noexcept
    {
        result_ = status;
        done(static_cast<int>(status));
    }
}
