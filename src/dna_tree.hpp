#ifndef DNA_TREE_HPP
#define DNA_TREE_HPP

#include<list>
#include<map>
#include<memory>

#include "haplotype.hpp"
#include "markers_db.hpp"

namespace dna_manipulation
{
    class DNATree
    {
    public:
        struct MarkerValue
        {
            bool operator==(const MarkerValue& cmp) const noexcept
            { return marker == cmp.marker && value == cmp.value; }
            unsigned char marker;
            unsigned char value;
        };
        typedef std::vector<MarkerValue> Mutations;
        typedef std::list<MarkerValue> MarkerNode;
        typedef std::map<std::string, MarkerNode> Nodes;

        DNATree() noexcept : base_(), freq_() {}; 

        DNATree(const dna_manipulation::MarkersRecord::MarkersArray& base);

        void add(const dna_manipulation::MarkersRecord& rec);

        void build(size_t count_markers = 0);

        void print_debug();

        const MarkersRecord::MarkersArray& base() const noexcept
            {return base_; }

        friend class DNATreeWalker;
    private:

        struct TreeNode;
        typedef std::unique_ptr<TreeNode> TreeNodePtr;

        struct TreeNode
        {
            void print_debug(size_t level = 0) const;
            size_t size(bool subtree);

            Mutations mutations;
            std::list<std::string> keys;
            std::vector<TreeNodePtr> links;
        };

        void add_(
            const std::string& id,
            MarkerNode& node,
            size_t weights[SupportedMarkers::MAX]);

        size_t check_node_(
            const TreeNode* node,
            MarkerNode& mnode);

        void add_node_(
            TreeNodePtr& node,
            const std::string& id,
            MarkerNode& mnode);

    private:
        dna_manipulation::MarkersRecord::MarkersArray base_;
        size_t freq_[SupportedMarkers::MAX];
        size_t mutation_rate_[SupportedMarkers::MAX];
        std::map<size_t, Nodes> nodes_;
        std::vector<TreeNodePtr> root_paths_;
    };

    class DNATreeWalker
    {
    public:
        DNATreeWalker(const DNATree& tree)
            : dna_tree_(tree) {};

        DNATreeWalker(const DNATreeWalker& walker)
            : dna_tree_(walker.dna_tree_),
              node_(walker.node_),
              save_node_(walker.save_node_)
        {};

        size_t size();

        bool go(size_t index, bool restore_state = false);

        void restore() { node_ = save_node_; }

        void save() { save_node_ = node_; }

        bool root_node() const noexcept { return node_ == nullptr; }

        DNATreeWalker& operator=(const DNATreeWalker&) = delete;

        const DNATree::TreeNode* operator->() const noexcept { return node_;}

    private:
        const DNATree& dna_tree_;
        DNATree::TreeNode* node_ = nullptr;
        DNATree::TreeNode* save_node_ = nullptr;
    };

}
#endif
