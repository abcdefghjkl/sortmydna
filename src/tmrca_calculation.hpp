#ifndef TMRCA_CALCULATION_HPP
#define TMRCA_CALCULATION_HPP

#include<map>
#include<array>
#include"haplotype.hpp"

namespace dna_manipulation
{
    class BaseHaploCalculator
    {
    public:
        typedef std::array<unsigned char, SupportedMarkers::MAX> MarkersArray;

        BaseHaploCalculator(int threshold = 10) noexcept;

        void add(const MarkersArray& markers);

        virtual void calc() noexcept;

        const MarkersArray& base() const noexcept {return base_; }

    protected:
        typedef std::map<unsigned char, size_t> CounterType;

        CounterType freq_[SupportedMarkers::MAX];
        int threshold_;

        bool all_valuable_[SupportedMarkers::MAX];
        size_t count_haplotypes_ = 0;
        MarkersArray base_;
    };

    class TMRCAAgeCalculator: public BaseHaploCalculator
    {
    public:
        TMRCAAgeCalculator() {};

        virtual void calc() noexcept;

        size_t count_mutations() const { return count_muatations_;}
        size_t age_distance() const { return age_distance_;}
        size_t age_error() const { return age_error_ ;}
        size_t count_markers() const { return count_markers_;}
        double mutation_speed() const { return mut_speed_;}

    private:
        size_t count_muatations_ = 0;
        size_t age_distance_ = 0;
        size_t age_error_ = 0;
        size_t count_markers_ = 0;
        double mut_speed_ = 0;
    };
}


#endif
