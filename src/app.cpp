/**
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include<sortmydnaconfig.h>
#include"app.hpp"
#include"exception.hpp"
#include"appconfig.hpp"
#include"haplotype.hpp"
#include"sqlite3_backend.hpp"
#include<QString>
#include<QSettings>
#include<QStandardPaths>
#include<QDir>
#include<sstream>
#include<iostream>
#include<stdlib.h>

namespace sortmydna
{
    App::App(int &argc, char* argv[])
        : QApplication(argc, argv)
    {
        if(!config_.read_config())
        {
            config_.def_config();
        }
        db_connection_.reset(
            new SQLiteBackend(config_.db_path.toStdString()));
        main_win_.reset(new MainWindow(config_, db_connection_.get()));
    }
    
    void App::init()
    {
        main_win_->showMaximized();
    }

    bool App::notify(QObject *receiver, QEvent *e)
    {
        try
        {
            return QApplication::notify(receiver, e);
        }
        catch(const std::exception& e)
        {
            std::cerr << __func__ << ":"  << e.what() << std::endl;
        }
        catch(...)
        {
            std::cerr << "caught unknown exception"  << std::endl;
        }
        return true;
    }
    
    QString AppConfig::def_db_path()
    {
        QStringList paths =
            QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
        QString db_path;
        auto i = 0;
        while(i < paths.size()) {
            db_path = paths.at(i);
            db_path += "/.config/";
            db_path += PROJECT_NAME;
            QDir directory;
            if(directory.mkpath(db_path)) {
                db_path += "/db";
                break;
            }
            ++i;
        }
        if(i == paths.size())
            AppException("Can't create configuration directory");
        return db_path;
    }

    void AppConfig::def_config()
    {
        auto panels = dna_manipulation::HaploSet::get_panel_names();
        if(panels.empty())
            model_name = "Custom";
        else
            model_name = QString::fromStdString(*panels.begin());
        db_path = def_db_path();
        enable_filter = false;
        view_config.show_option = ViewConfig::SW_ALL;
        view_config.show_info_column = false;
        view_config.show_distance_column = false;
        sort_order = 1;
    }
    
    bool AppConfig::read_config()
    {
        QSettings settings("Smart", PROJECT_NAME);
        if(!settings.contains("global/model_name"))
            return false;
        model_name = settings.value("global/model_name").toString();
        if(settings.contains("custom/model"))
            custom_model  = settings.value("custom/model").toString();
        if(settings.contains("global/haplotype"))
            haplotype  = settings.value("global/haplotype").toString();
        if(settings.contains("global/haplogroup"))
            haplo_group  = settings.value("global/haplogroup").toString();
        if(settings.contains("global/haplosubgroup"))
            haplo_subgroup  = settings.value("global/haplosubgroup").toString();
        if(settings.contains("view/show_option"))
            view_config.show_option = static_cast<ViewConfig::ShowValues>(settings.value("view/show_option").toInt());
        else
            view_config.show_option = ViewConfig::SW_ALL;
        
        if(settings.contains("view/custom_markers"))
            view_config.custom_markers  = settings.value("view/custom_markers").toString();

        if(settings.contains("view/show_info"))
            view_config.show_info_column = settings.value("view/show_info").toBool();
        else
            view_config.show_info_column = false;
        if(settings.contains("view/show_distance"))
            view_config.show_distance_column = settings.value("view/show_distance").toBool();
        else
            view_config.show_distance_column = false;
        if(settings.contains("view/filter"))
            enable_filter = settings.value("view/filter").toBool();
        else
            enable_filter = false;
        if(settings.contains("view/filter_value"))
            filter_string = settings.value("view/filter_value").toString();
        if(settings.contains("view/filter_regexp"))
            filter_regexp = settings.value("view/filter_regexp").toString();

        if(settings.contains("view/sort_order"))
            sort_order = settings.value("view/sort_order").toUInt();
        else
            sort_order = 1;
        if(settings.contains("import/group"))
            import_group  = settings.value("import/group").toString();
        if(settings.contains("import/subgroup"))
            import_subgroup  = settings.value("import/subgroup").toString();
        if(settings.contains("global/db"))
            db_path  = settings.value("global/db").toString();
        else 
            db_path = def_db_path();

        return true;
    }

    void AppConfig::write_config()
    {   
        QSettings settings("Smart", PROJECT_NAME);
        settings.setValue("global/model_name", model_name);
        settings.setValue("global/haplotype", haplotype);
        settings.setValue("global/haplogroup", haplo_group);
        settings.setValue("global/haplosubgroup", haplo_subgroup);
        settings.setValue("global/db", db_path);
        settings.setValue("custom/model", custom_model);
        settings.setValue("import/group", import_group);
        settings.setValue("import/subgroup", import_subgroup);
        settings.setValue("view/show_option", view_config.show_option);
        settings.setValue("view/custom_markers", view_config.custom_markers);
        settings.setValue("view/show_info", view_config.show_info_column);
        settings.setValue("view/show_distance", view_config.show_distance_column);
        settings.setValue("view/filter", enable_filter);
        settings.setValue("view/filter_value", filter_string);
        settings.setValue("view/filter_regexp", filter_regexp);
        settings.setValue("view/sort_order", sort_order);
    }
}
