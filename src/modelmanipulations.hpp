#ifndef SORT_MY_DNA_MODEL_MANIPULATION_HPP
#define SORT_MY_DNA_MODEL_MANIPULATION_HPP

#include<memory>
#include<vector>
#include<list>
#include"haploutils.hpp"

namespace sortmydna
{
    struct FieldInfo
    {
        enum FieldType
        {
            FT_ID = 0,
            FT_ORIGIN,
            FT_CATEGORY,
            FT_HAPLOGROUP,
            FT_SNP,
            FT_INFO,
            FT_MARKER,
            FT_IGNORY
        };
        bool operator==(const FieldInfo& info) const
        { return (this == &info ||
                (type == info.type && name == info.name));
        }

        FieldType type;
        std::string name;
    };

    typedef std::vector<FieldInfo> FieldDescription;

    class Model;

    typedef std::unique_ptr<Model> ModelPtr;


    class Model: public FieldDescription
    {
    public:
        enum ParseResult
        {
            PR_SUCCESS = 0,
            PR_FAIL,
            PR_CANCEL
        };

        Model() : count_markers(), count_fields(), count_text(), accept_data(PR_FAIL), data(true) {};

        bool operator==(const Model& info) const;
        bool operator!=(const Model& info) const { return !(*this == info);}

        ParseResult check_model(const Model& base_model) noexcept;

        ModelPtr create_base_model() const;
        
        void adapt_model(const Model& base_model);

        static 
        ModelPtr create_model(const std::list<dna_manipulation::SubString>& columns); 

    public:
        std::string name;
        size_t count_markers;
        size_t count_fields;
        size_t count_text;
        mutable ParseResult accept_data;
        bool data;
    };
}

#endif

