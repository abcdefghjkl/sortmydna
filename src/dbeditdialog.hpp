#pragma once

#include<QDialog>
#include"appconfig.hpp"

class QTableView;

namespace sortmydna
{
    class EditDbDialog: public QDialog
    {
    Q_OBJECT
    public:
        EditDbDialog(QWidget* parent, const AppConfig& config);

    private:
        const AppConfig& config_;
    };
}
