/**
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include<cmath>
#include<sstream>
#include<iostream>
#include"markers_db.hpp"
#include"haplotype.hpp"
#include"haploutils.hpp"

namespace dna_manipulation
{
    size_t MarkersRecord::valuable_indexes(
        unsigned char indexes[SupportedMarkers::MAX]) const noexcept
    {
        size_t count_markers = 0;
        for(size_t i = 0; i < SupportedMarkers::MAX; ++i)
            if(markers[i]) {
                if(indexes)
                    indexes[count_markers] = i;
                ++count_markers;
            }
        return count_markers;
    }

    size_t MarkersDBUtils::calc_common_ancestor_age(
        const MarkersRecord& rec1,
        const MarkersRecord& rec2,
        double& mutation_speed,
        unsigned long& mutation_distance,
        unsigned long& age_distance,
        unsigned long& age_error)
    {
        mutation_speed = 0.0f;
        mutation_distance = 0;
        size_t count_markers = 0;
        for(size_t i = 0; i < SupportedMarkers::MAX; ++i) {
            if(rec1[i] && rec2[i]) {
                ++count_markers;
                mutation_speed += dna_manipulation::HaploSet::get_mutation_speed(static_cast<dna_manipulation::SupportedMarkers>(i));
                if(rec1[i] > rec2[i])
                    mutation_distance += rec1[i] - rec2[i];
                else if(rec2[i] > rec1[i])
                    mutation_distance += rec2[i] - rec1[i];
            }
        }
        age_distance =  (mutation_distance / 2.0f / mutation_speed);
        if(age_distance >= count_markers) {
            float correction =  mutation_distance / 2.0f / count_markers;
            age_distance +=  correction / 2.0f * (1.0f + exp(correction));
        }
        float corr = (1.0f / sqrt(mutation_distance)) * 100.0f;
        corr = sqrt(corr * corr + 100.0f);
        age_distance *= 25;
        age_error = age_distance * corr / 100;
        return count_markers;
    }
    /*
       @return count checked markers
       */
    size_t MarkersDBUtils::calc_distance(
        unsigned char indexes[SupportedMarkers::MAX],
        size_t count_markers,
        const MarkersRecord& rec1,
        const MarkersRecord& rec2,
        unsigned long& mutation_distance,
        unsigned long* generation_distance)
    {
        size_t res = 0;
        mutation_distance = 0;
        if(generation_distance)
            *generation_distance = 0;
        for(size_t i = 0; i < count_markers; ++i) {
            auto index = indexes[i];
            if(rec1.markers[index] != rec2.markers[index]) {
                if(rec1.markers[index] && rec2.markers[index]) {//both setted
                    unsigned long diff_mark;
                    if(rec1.markers[index] < rec2.markers[index])
                        diff_mark =  rec2.markers[index] - rec1.markers[index];
                    else
                        diff_mark =  rec1.markers[index] - rec2.markers[index];
                    mutation_distance += diff_mark;
                    if(generation_distance)
                        *generation_distance += HaploSet::get_generations(
                            static_cast<SupportedMarkers>(index), diff_mark);
                    res++;
                }
            }
            else
                res++;
        }
        return res;
    }

    std::string MarkersDBUtils::to_string(const MarkersLine& line)
    {
        std::string res;
        res.reserve(line.size() * 3);
        auto names = HaploSet::get_supported_marker_names();
        for(auto marker: line)
        {
            res += names[marker];
            res.push_back(' ');
        }
        res.pop_back();
        return res;
    }

    MarkersDBUtils::MarkersLine MarkersDBUtils::from_string(const char* str, size_t len)
    {
        MarkersLine res;
        res.reserve((len + 1)/3);
        dna_manipulation::Splitter<> splitter(str, len, dna_manipulation::Delimiters(" "));
        dna_manipulation::SubString substr;
        const auto& markers = dna_manipulation::HaploSet::get_marker_indexes();
        while(splitter.get_token(substr))
        {
            auto it = markers.find(substr);
            if(it == markers.end())
            {
                std::ostringstream err;
                err << __FILE__ << ":" << __LINE__ << ", " << __func__ << ": in '" << std::string(str, len) << "' unknown marker: " << substr.str();
                throw LibException(err.str());
            }
            res.push_back(static_cast<dna_manipulation::SupportedMarkers>(it->second));
        }
        return res;
    }

    const MarkersRecord& MarkersDBBase::operator[] (size_t index) const 
    {
        if(index >= size())
            throw LibException("Index out of bounds");
        return (*db_records_)[index];
    }

    const MarkersRecord& SortedDB::operator[] (size_t index) const
    {
        if(index >= size())
            throw LibException("Index out of bounds");
        return (*db_records_)[sort_index_[index_][index]];
    }

    size_t SortedDB::get_distance(size_t index) const noexcept
    {
        if(index_  == SI_MUTATIONS)
            return sort_info_[sort_index_[index_][index]].diff_mut;
        else
            return sort_info_[sort_index_[index_][index]].diff_gen;
    }

    SortedDB::SortedDB(
        const MarkersRecord& base,
        const MarkersDBBase& db)
        : MarkersDBBase(db.shared_data()),
          base_(base)
    {
        apply_(base_);
    }

    SortedDB::SortedDB(
        const MarkersRecord& base,
        std::unique_ptr<DBContainer>&& rec_ptr)
        : MarkersDBBase(std::move(rec_ptr)),
          base_(base)
    {
        apply_(base_);
    }

    SortedDB::SortedDB(
        const MarkersRecord& base,
        const SortedDB& rec_ptr)
        : MarkersDBBase(rec_ptr.shared_data()),
          base_(base)
    {
        apply_(base_);
    }

    void SortedDB::switch_index(SortIndex ind) noexcept
    {
        index_ = ind; // mutations
    } 

    SortedDB::SortIndex SortedDB::get_sort_index() const noexcept
    {
        return index_;
    }

    void SortedDB::apply_(const MarkersRecord& base)
    {
        unsigned char indexes[SupportedMarkers::MAX];
        size_t count_markers = base.valuable_indexes(indexes);
        sort_info_.resize(db_records_->size());
        sort_index_[SI_MUTATIONS].resize(db_records_->size());
        sort_index_[SI_GENERATIONS].resize(db_records_->size());
        for(auto i = 0U; i < db_records_->size(); ++i) {
            sort_info_[i].count_markers = MarkersDBUtils::calc_distance(
                indexes, count_markers, base, (*db_records_)[i],
                sort_info_[i].diff_mut, &sort_info_[i].diff_gen);
            sort_index_[SI_MUTATIONS][i] = i;
            sort_index_[SI_GENERATIONS][i] = i;
        }
        std::sort(
            sort_index_[SI_MUTATIONS].begin(),
            sort_index_[SI_MUTATIONS].end(),
            [this] (size_t a, size_t b) {return sort_info_[a].diff_mut < sort_info_[b].diff_mut; });
        std::sort(
            sort_index_[SI_GENERATIONS].begin(),
            sort_index_[SI_GENERATIONS].end(),
            [this] (size_t a, size_t b) {return sort_info_[a].diff_gen < sort_info_[b].diff_gen; });
    }

    void SortedDB::Elem::swap(SortedDB::Elem& elem) && noexcept
    {
        std::swap(*this, elem);
    }

    void MarkersDB::add_record(const MarkersRecord& record)
    {
        db_records_->push_back(record);
    }

    void MarkersDB::add_record(MarkersRecord&& record)
    {
        db_records_->push_back(std::move(record));
    }

    void MarkersDB::set_records(const DBContainer& records)
    {
        *db_records_ = records;
    }

    void MarkersDB::set_records(std::unique_ptr<DBContainer>&& records)
    {
        db_records_ = std::move(records);
    }

    void MarkersDB::debug_print() const noexcept
    {
        for(const auto& rec : *db_records_) {
            std::cout << rec.id << '|' << rec.info << '|';
            for(auto marker : rec.markers)
                std::cout << std::to_string(marker) << '|';
            std::cout << std::endl;
        }
    }
}
