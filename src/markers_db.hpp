/**
  file: markers_db.hpp 2015-2019 Andrey Gusev

  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include"haplotype.hpp"
#include"tmrca_calculation.hpp"
#include<vector>
#include<deque>
#include<memory>

namespace dna_manipulation
{
    struct MarkersRecord
    {
        typedef BaseHaploCalculator::MarkersArray MarkersArray;
        MarkersRecord() : markers() {};

        /* Count not zero markers and return them 
           @return indexes - not zero markers
           @return  count not zero markers */
        size_t valuable_indexes(
            unsigned char indexes[SupportedMarkers::MAX]) const noexcept;

        unsigned char operator[] (size_t index) const { return markers[index];}

        unsigned long db_id = 0;
        std::string id;
        std::string origin;
        std::string category;
        std::string haplogroup;
        std::string subclad;
        std::string snp;
        std::string info;
        MarkersArray markers;
    };

    typedef std::vector<MarkersRecord> MarkersRecords;

    class MarkersDBUtils
    {
    public:
        using CounterType = std::map<unsigned char, size_t>;
        using MarkersLine = HaploSet::MarkersLine;

        /* Calculate distances between 2 marker records
           @parma rec1 - first record, must provide operator[] for marker
           @parma rec2 - second record, must provide operator[] for marker
           @return mutation_speed - speed mutation for this markers
           @return mutation_distance - difference in mutations
           @return age_distance - age until count ancestor
           @return count valuable markers */ 
        static size_t calc_common_ancestor_age(
            const MarkersRecord& rec1,
            const MarkersRecord& rec2,
            double& mutation_speed,
            unsigned long& mutation_distance,
            unsigned long& age_distance,
            unsigned long& age_error);

        /* Calculate distances between 2 marker records
           @param indexes - actual markers of records
           @param count_markers - count actual markers
           @parma rec1 - first record, must provide operator[] for marker
           @parma rec2 - second record, must provide operator[] for marker
           @return mutation_distance - difference in mutation
           @return generation_distance - difference in speed of marker mutation
           @return count not zero markers */ 
        static size_t calc_distance(
            unsigned char indexes[SupportedMarkers::MAX],
            size_t count_markers,
            const MarkersRecord& rec1,
            const MarkersRecord& rec2,
            unsigned long& mutation_distance,
            unsigned long* generation_distance = nullptr);

        /* Find mutated markers between 2 marker records
           @param indexes - actual markers of records
           @param count_markers - count actual markers
           @parma rec1 - first record
           @parma rec2 - second record
           @return mutations - markers with mutations 
           @return count markers with mutations */ 
        template<class REC1, class REC2>
        static size_t find_mutations(
            unsigned char indexes[SupportedMarkers::MAX],
            size_t count_markers,
            const REC1& rec1,
            const REC2& rec2,
            unsigned char mutations[SupportedMarkers::MAX]);


        /*Calculate base haplotype
           @param db - database of records
           @return base - base haplotype
           @param indexes - indexes in db, if empty all records from db
           @param threshold - minimum percent of records
           with marke of base haplotype */
        template<
            class DBType,
            class CalculatorType,
            class IndexContainer = std::vector<size_t>>
        static void calc_for_set(
            const DBType& db,
            CalculatorType& calculator,
            const IndexContainer& indexes = IndexContainer());

        /* convert line of marker to string represantation like
         * DYS393 ... DYS418" */
        static std::string to_string(const MarkersLine&);

        /* convert from string to line of marker */
        static MarkersLine from_string(const char* str, size_t len);

        static MarkersLine from_string(const std::string& str)
        {
            return from_string(str.c_str(), str.size());
        }

    };

    class MarkersDBBase
    {
    public:
        typedef std::vector<MarkersRecord> DBContainer;
        typedef std::shared_ptr<DBContainer> DBSharedPtr;

        MarkersDBBase() : db_records_(std::make_shared<DBContainer>()) {};

        virtual ~MarkersDBBase () {};

        virtual bool empty() const noexcept {return db_records_->empty(); }

        virtual size_t size() const noexcept { return db_records_->size(); };

        virtual const MarkersRecord& operator[](size_t index) const;

        inline
        const MarkersRecord& get_record(size_t index) const noexcept
        {
            return (*db_records_)[index];
        }

        const DBSharedPtr& shared_data() const  { return db_records_;}

    protected:
        MarkersDBBase(const DBSharedPtr& ptr) : db_records_(ptr) {};

        MarkersDBBase(const DBSharedPtr&& ptr) : db_records_(std::move(ptr)) {};

    protected: 
        DBSharedPtr db_records_;
    };

    class MarkersDB: public MarkersDBBase
    {
    public:

        MarkersDB() {};

        virtual ~MarkersDB() {};

        void add_record(const MarkersRecord& record);

        void add_record(MarkersRecord&& record);

        void set_records(const DBContainer& records);

        void set_records(std::unique_ptr<DBContainer>&& records);

        void debug_print() const noexcept;

    };

    struct FilterRule
    {
        enum Signs
        {
            S_LESS,
            S_EQUAL,
            S_NEQUAL,
            S_GREATER
        };

        SupportedMarkers marker;
        Signs sign;
        unsigned char value;
    };

    class SortedDB: public MarkersDBBase
    {
        public:
        enum SortIndex
        {
            SI_MUTATIONS = 0,
            SI_GENERATIONS,
            SI_MAX
        };


        typedef std::vector<FilterRule> FilterRules;

        SortedDB(
            const MarkersRecord& base,
            const MarkersDBBase& db);

        SortedDB(
            const MarkersRecord& base,
            std::unique_ptr<DBContainer>&& rec_ptr);

        SortedDB(
            const MarkersRecord& base,
            const SortedDB& rec_ptr);

        virtual ~SortedDB() {};

        bool empty() const noexcept override {return sort_index_[index_].empty(); }

        size_t size() const noexcept override { return sort_index_[index_].size(); };

        size_t index(size_t index) const noexcept { return sort_index_[index_][index]; }

        const MarkersRecord& operator[](size_t index) const override;

        size_t get_distance(size_t index) const noexcept;

        void switch_index(SortIndex ind) noexcept;

        SortIndex get_sort_index() const noexcept;

        private:
            void apply_(const MarkersRecord& base);

            struct Elem
            {
                void swap(SortedDB::Elem& elem) && noexcept;
                //operator unsigned long() const {return diff;}
                unsigned long count_markers;
                unsigned long diff_mut;
                unsigned long diff_gen;
                bool pass_filter = true;
            };
        private:
        SortIndex index_ = SI_MUTATIONS;
        std::vector<Elem> sort_info_;
        std::vector<size_t> sort_index_[SortIndex::SI_MAX];
        MarkersRecord base_;
    };

    using SharedSortedDBPtr = std::shared_ptr<SortedDB>;

    using MarkersLine = dna_manipulation::HaploSet::MarkersLine;
    struct DBMarkerLine
    {
        int id;
        unsigned int count_markers;
        std::string name;
        MarkersLine line;
    };
    using DBMarkersLines = std::deque<DBMarkerLine>;
    using SharedDBMarkersLinePtr = std::shared_ptr<DBMarkersLines>;

}

namespace dna_manipulation
{
    template<class REC1, class REC2>
    size_t MarkersDBUtils::find_mutations(
        unsigned char indexes[SupportedMarkers::MAX],
        size_t count_markers,
        const REC1& rec1,
        const REC2& rec2,
        unsigned char mutations[SupportedMarkers::MAX])
    {
        size_t count_mutations = 0;
        for(size_t i = 0; i < count_markers; ++i) {
            auto index = indexes[i];
            if(rec1[index] != rec2[index]) {
                mutations[count_mutations++] = index;
            }
        }
        return count_mutations;
    }

    template<class DBType, class CalculatorType, class IndexContainer>
    void MarkersDBUtils::calc_for_set(
        const DBType& db,
        CalculatorType& calculator,
        const IndexContainer& indexes)
    {
        size_t total_records = indexes.empty() ? db.size() : indexes.size();
        auto it = indexes.begin();
        for(size_t i = 0; i < total_records; ++i) {
            const auto& rec = db[indexes.empty() ? i : *it++];
            calculator.add(rec.markers);
        }
        calculator.calc();
    }
}
