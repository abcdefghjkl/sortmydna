#ifndef CUSTOM_VIEW_DIALOG_HPP
#define CUSTOM_VIEW_DIALOG_HPP

#include<QDialog>
#include"haplotype.hpp"

class QTableWidget;

namespace sortmydna
{
    class CustomViewDialog: public QDialog
    {
    Q_OBJECT
    public:
        CustomViewDialog(
            const dna_manipulation::HaploSet::MarkersLine& line);

        static QStringList get_maker_names_(
            const dna_manipulation::HaploSet::MarkersLine& markers) noexcept;

        virtual void accept();

        inline
        dna_manipulation::HaploSet::MarkersLine&
        get_markers_line() { return line_; }

    protected:
        QTableWidget* setup_custom_table_() noexcept;

        enum TableIndexes: int
        {
            TI_NAME = 0,
            TI_CHECK = 1
        };

    private:
        const dna_manipulation::HaploSet::MarkersLine& old_line_;//TODO: for reseting to previous values
        dna_manipulation::HaploSet::MarkersLine line_;
        QTableWidget* table_;
    };
}

#endif
