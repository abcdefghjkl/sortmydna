#ifndef SORT_MY_DNA_EXCEPTION_HPP
#define SORT_MY_DNA_EXCEPTION_HPP

namespace sortmydna
{
    class AppException: public std::runtime_error
    {
    public:
        AppException(const std::string& str) : 
            std::runtime_error(str) {};
    };
}

#endif
