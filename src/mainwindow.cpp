/**
  file: mainwindow.cpp 2015-2019 Andrey Gusev
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include"mainwindow.hpp"

#include<iostream>
#include<sstream>
#include<set>
#include<thread>
#include<memory>
#include<QFrame>
#include<QMenu>
#include<QMenuBar>
#include<QLabel>
#include<QCheckBox>
#include<QHBoxLayout>
#include<QVBoxLayout>
#include<QWidgetAction>
#include<QTableWidget>
#include<QTableWidgetItem>
#include<QHeaderView>
#include<QRadioButton>
#include<QGroupBox>
#include<QPushButton>
#include<QFileDialog>
#include<QCoreApplication>
#include"enterhaplotypedialog.hpp"
#include"importdbdialog.hpp"
#include"exception.hpp"
#include"exception.hpp"
#include"helpdialog.hpp"
#include"filterdialog.hpp"
#include"ancestortimedialog.hpp"
#include"treedialog.hpp"
#include"customviewdialog.hpp"
#include"filters.hpp"
#include"dbviewmodel.hpp"

namespace sortmydna
{
    MainWindow::MainWindow(AppConfig& config, DBBackend* dbb)
        : add_haplotype_(),
          config_(config),
          dbb_(dbb)

    {
        parse_haplotype_(
            config_.model_name,
            config_.haplotype,
            config_.model_name == tr("Custom") ? &config_.custom_model : nullptr,
            my_markers_,
            my_haplotype_);

        parse_markers_string_(config_.view_config.custom_markers, custom_markers_);

        QFrame* frame = new QFrame;
        setup_window_(frame);
        setup_menu_();
        setCentralWidget(frame);
    }

    template<class T>
    void MainWindow::setup_menu_action_(
        QMenu* menu,
        const char* name,
        const char* tip,
        T func)
    {
        auto *act = new QAction(tr(name), this);
        //act->setShortcuts(QKeySequence::New);
        act->setStatusTip(tr(tip));
        connect(act, &QAction::triggered, this, func);
        menu->addAction(act);
    }

    void MainWindow::setup_menu_()
    {
        auto menu = menuBar()->addMenu(tr("&Actions"));
        setup_menu_action_(
            menu,
            "&Set haplotype",
            "Set my haplotype",
            &MainWindow::set_haplotype);
        setup_menu_action_(
            menu,
            "&Calc age",
            "Calc common ancester age",
            &MainWindow::calc_common_ancester);
        setup_menu_action_(
            menu,
            "&Tree",
            "Show haplotype tree",
            &MainWindow::show_tree);
        setup_menu_action_(
            menu,
            "&Export",
            "Export to filip",
            &MainWindow::export_to_filip);
        setup_menu_action_(
            menu,
            "&Exit",
            "Exit",
            &MainWindow::close);

         menu = menuBar()->addMenu(tr("&View"));

         QRadioButton* r_view = new QRadioButton(tr("View all"), menu);
         if(config_.view_config.show_option == ViewConfig::SW_ALL)
             r_view->setChecked(true);
         connect(r_view, &QRadioButton::toggled, this, [this] (bool state){ change_view(ViewConfig::ShowValues::SW_ALL, state);});
         QWidgetAction *wact = new QWidgetAction(menu);
         wact->setDefaultWidget(r_view);
         wact->setStatusTip(tr("View all columns"));
         menu->addAction(wact);
         r_view = new QRadioButton(tr("View my"), menu);
         if(config_.view_config.show_option == ViewConfig::SW_MY)
             r_view->setChecked(true);
         connect(r_view, &QRadioButton::toggled, this, [this] (bool state){ change_view(ViewConfig::ShowValues::SW_MY, state);});
         wact = new QWidgetAction(menu);
         wact->setDefaultWidget(r_view);
         wact->setStatusTip(tr("View my columns"));
         menu->addAction(wact);
         r_view = new QRadioButton(tr("View custom"), menu);
         if(config_.view_config.show_option == ViewConfig::SW_CUSTOM)
             r_view->setChecked(true);
         connect(r_view, &QRadioButton::toggled, this, [this] (bool state){ change_view(ViewConfig::ShowValues::SW_CUSTOM, state);});
         wact = new QWidgetAction(menu);
         wact->setDefaultWidget(r_view);
         wact->setStatusTip(tr("View custom columns"));
         menu->addAction(wact);

         QCheckBox* check = new QCheckBox("View distance", menu);
         check->setChecked(config_.view_config.show_distance_column);
         connect(check, &QCheckBox::stateChanged, this, [this] (int state)
             {config_.view_config.show_distance_column = (state != Qt::Unchecked);
              auto dist_column = db_model_->get_distance_index();
              if(config_.view_config.show_distance_column)
                    db_widget_->showColumn(dist_column);
                else
                    db_widget_->hideColumn(dist_column);});
         wact = new QWidgetAction(menu);
         wact->setDefaultWidget(check);
         wact->setStatusTip(tr("View distance column"));
         menu->addAction(wact);

         check = new QCheckBox("View info", menu);
         check->setChecked(config_.view_config.show_info_column);
         connect(check, &QCheckBox::stateChanged, this, [this] (int state)
             {config_.view_config.show_info_column = (state != Qt::Unchecked);
              auto info_column = db_model_->get_info_index();
              if(config_.view_config.show_info_column)
                    db_widget_->showColumn(info_column);
                else
                    db_widget_->hideColumn(info_column);});
         wact = new QWidgetAction(menu);
         wact->setDefaultWidget(check);
         wact->setStatusTip(tr("View info column"));
         menu->addAction(wact);

         filter_check_ = new QCheckBox("&Filterering", menu);
         filter_check_->setChecked(config_.enable_filter);
         connect(filter_check_, &QCheckBox::toggled, this, &MainWindow::turn_filter);
         wact = new QWidgetAction(menu);
         wact->setDefaultWidget(filter_check_);
         wact->setStatusTip(tr("Filter haplotypes"));
         menu->addAction(wact);

        setup_menu_action_(
            menu,
            "&Filter settings",
            "Set filter",
            &MainWindow::enter_filter);

        setup_menu_action_(
            menu,
            "&Custom view settings",
            "Custom view",
            &MainWindow::custom_view);

        //Menu Data
        menu = menuBar()->addMenu(tr("&Database"));
        setup_menu_action_(
            menu,
            "&Import",
            "Import database",
            &MainWindow::import_database);
        setup_menu_action_(
            menu,
            "&View",
            "View database",
            &MainWindow::view_database);

         //Menu Help
        menu = menuBar()->addMenu(tr("&Help"));
        setup_menu_action_(
            menu,
            "&About",
            "About program",
            &MainWindow::about);
    }

    void MainWindow::change_view(int option, bool state)
    {
       if(state)
       {
           config_.view_config.show_option = option;
           auto markers = choose_marker_line(config_.view_config.show_option);
           db_model_->change_markers(std::move(markers));
       }
    }

    void MainWindow::about()
    {
        std::unique_ptr<AboutDialog> about(new AboutDialog());
        about->exec();
    }

    void MainWindow::update_model_()
    {
        for(size_t i = 0; i < my_markers_.size(); ++i) {
                auto marker = static_cast<dna_manipulation::SupportedMarkers>(
                    my_haplotype_.markers[my_markers_[i]]);
                QTableWidgetItem* item = new QTableWidgetItem(
                    QString("%1").arg(static_cast<int>(marker)));
                item->setFlags(item->flags() ^ Qt::ItemIsEditable);
                if(config_.enable_filter &&  filter_markers_.find(marker) != filter_markers_.end()) {
                    QBrush fg(Qt::darkGreen);// = item->foreground();
                    item->setForeground(fg);
                }
                haplo_widget_->setItem(0, i, item);
        }
    }

    void MainWindow::setup_window_(QFrame* frame)
    {
        using SortedDB = dna_manipulation::SortedDB;
        const QStringList& headers = db_model_->get_marker_names(my_markers_);
        haplo_widget_ = new QTableWidget(1, headers.size());
        haplo_widget_->setHorizontalHeaderLabels(headers);
        haplo_widget_->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        if(!config_.haplotype.isEmpty())
            update_model_();

         haplo_widget_->resizeRowsToContents();
         haplo_widget_->verticalHeader()->setVisible(false);
         haplo_widget_->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
         haplo_widget_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        //adjustSize();
        //for(auto i = 0; i < my_haplo->verticalHeader()->count(); ++i)
        //    my_haplo->verticalHeader()->setSectionHidden(i, true);

        QVBoxLayout *vbox = new QVBoxLayout;
        QRadioButton* sort_radio1 = new QRadioButton(tr("Mutations"));
        sort_radio1->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        if(config_.sort_order == SortedDB::SI_MUTATIONS)
            sort_radio1->setChecked(true);
        QRadioButton* sort_radio2 = new QRadioButton(tr("Markers"));
        if(config_.sort_order == SortedDB::SI_GENERATIONS)
            sort_radio2->setChecked(true);
        sort_radio2->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        vbox->addWidget(sort_radio1);
        vbox->addWidget(sort_radio2);
        connect(sort_radio1, &QRadioButton::toggled, this, [this] ()
            { change_sort_order(SortedDB::SI_MUTATIONS);});
        connect(sort_radio2, &QRadioButton::toggled, this, [this] ()
            { change_sort_order(dna_manipulation::SortedDB::SI_GENERATIONS);});
        QGroupBox* sort_box = new QGroupBox(tr("Sorting"));
        sort_box->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sort_box->setLayout(vbox);

        filter_label_  = new QLabel(config_.filter_string);

        QPushButton* filter_button = new QPushButton(tr("Filter settings"));
        filter_button->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        connect(filter_button, &QAbstractButton::clicked, this, &MainWindow::enter_filter);

        QVBoxLayout* filter_layout = new QVBoxLayout;
        filter_layout->addWidget(filter_label_);
        filter_layout->addWidget(filter_button);

        filter_box_ = new QGroupBox("Filtering");
        filter_box_->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        filter_box_->setCheckable(true);
        filter_box_->setChecked(config_.enable_filter);
        connect(filter_box_, &QGroupBox::toggled, this, &MainWindow::turn_filter);
        filter_box_->setLayout(filter_layout);

        QVBoxLayout* top_vbox = new QVBoxLayout;
        top_vbox->addWidget(sort_box);
        top_vbox->addWidget(filter_box_);

        QHBoxLayout* haplo_layout = new QHBoxLayout;
        haplo_layout->addLayout(top_vbox);
        haplo_layout->addWidget(haplo_widget_);

        db_model_.reset(new DbViewModel(choose_marker_line(config_.view_config.show_option)));
        create_db_table(
            nullptr,
            choose_marker_line(config_.view_config.show_option),
            config_.view_config.show_distance_column,
            config_.view_config.show_info_column);

        db_layout_ = new QVBoxLayout;
        db_layout_->addLayout(haplo_layout);
        db_layout_->addWidget(db_widget_);
        frame->setLayout(db_layout_);

        std::thread t1([this] (
                const std::string& group,
                const std::string& subgroup,
                const dna_manipulation::MarkersRecord& haplotype)
            {
                db_load_task(group, subgroup, haplotype);
            },
            config_.haplo_group.toStdString(),
            config_.haplo_subgroup.toStdString(),
            my_haplotype_);
        t1.detach();
    }

    const dna_manipulation::HaploSet::MarkersLine&
    MainWindow::choose_marker_line(int option) noexcept
    {
        switch(option)
        {
            case ViewConfig::SW_CUSTOM:
                return custom_markers_;
                break;
            case ViewConfig::SW_MY:
                return my_markers_;
                break;
            default:
                return dna_manipulation::HaploSet::get_markers();
                break;
        }
    }

    void MainWindow::create_db_table(
        const dna_manipulation::SortedDB* db,
        const dna_manipulation::HaploSet::MarkersLine& markers,
        bool show_distance,
        bool show_info)
    {
        db_widget_ = new QTableView();
        QHeaderView *header = new QHeaderView(Qt::Horizontal, db_widget_);
        header->setSectionResizeMode(QHeaderView::ResizeToContents);
        db_widget_->setHorizontalHeader(header);
        header = new QHeaderView(Qt::Vertical, db_widget_);
        db_widget_->setVerticalHeader(header);
        db_widget_->setModel(db_model_.get());
        db_widget_->setColumnHidden(static_cast<int>(db_model_->get_distance_index()), !show_distance);
        db_widget_->setColumnHidden(static_cast<int>(db_model_->get_info_index()), !show_info);
    }
    
    void MainWindow::export_to_filip()
    {
        auto db = db_model_->get_db();
        if(!db)//TODO: need to report error to user
            return;
        QString file_name = QFileDialog::getSaveFileName(
            this,
            tr("Select File"));
        std::ofstream ofs(file_name.toStdString().c_str());
        if(ofs.fail())//TODO: need to report error to user
            return;
        size_t size = std::min(db->size(), 16UL);//TODO: next to ask user about records
        ofs << size + 1 << std::endl;
        ofs.fill(' ');
        unsigned long distance;
        unsigned char indexes[dna_manipulation::SupportedMarkers::MAX];
        size_t count_markers = my_haplotype_.valuable_indexes(indexes);
        for(size_t i = 0; i < size + 1; ++i) {
            ofs.width(9);
            dna_manipulation::MarkersRecord const *rec1;
            dna_manipulation::MarkersRecord const *rec2;
            if(i == 0) {
                ofs  << std::left << "my";
                rec1 = &my_haplotype_;
            }
            else {
                rec1 = &(*db)[i - 1];
                ofs << rec1->id;
            }
            ofs.width(1);
            for(size_t j = 0; j < size + 1; ++j) {
                if(j == 0)
                    rec2 = &my_haplotype_;
                else
                    rec2 = &(*db)[j - 1];
                if(i == j)
                    ofs << " 0";
                else {// TODO: neeed to optimaze to calculate distance only once if (i < j)
                    dna_manipulation::MarkersDBUtils::calc_distance(
                        indexes,
                        count_markers,
                        *rec1,
                        *rec2,
                        distance);
                    ofs << ' ' << distance;
                }
            }
            ofs << std::endl;
        }
        //
        ofs.close();
    }

    void MainWindow::change_sort_order(dna_manipulation::SortedDB::SortIndex index) noexcept
    {
       config_.sort_order = index;
       db_model_->change_sort_order(index);
       apply_filter_();
    }

    void MainWindow::update_data(MarkersRecordsPtr& records)
    {
        filter_label_->setText(config_.filter_string);
        records->switch_index(static_cast<dna_manipulation::SortedDB::SortIndex>(
            config_.sort_order));
        filter_cache_.clear();// need rebuild filters after changing db, so clear cache
        db_model_->set_db(records);
        apply_filter_();
        build_tree_();
    }

    void MainWindow::build_tree_()
    {
        std::thread build_tree( [] (
            MainWindow* dlg,
            MarkersRecordsPtr db) {
                dna_manipulation::BaseHaploCalculator calculator;
                dna_manipulation::MarkersDBUtils::calc_for_set(*db, calculator);
                TreeDialog::TreePtr tree(
                    new dna_manipulation::DNATree(calculator.base()));
                for(size_t rec_index = 0; rec_index < db->size(); ++rec_index) {
                    const auto& rec = (*db)[rec_index];
                    tree->add(rec);
                }
                tree->build();
                dlg->tree_ = tree;
                {
                    std::lock_guard<std::mutex> lock(dlg->tree_dlg_lock_);
                    if(dlg->tree_dlg_)
                        dlg->tree_dlg_->set_tree(tree);
                }
            },
            this, db_model_->get_db());
        build_tree.detach();
    }

    bool MainWindow::event(QEvent * e)
    {
        if(e->type() == FinishEvent::event_id)
        {
            FinishEvent* ev = static_cast<FinishEvent*>(e);
            MarkersRecordsPtr records(std::move(ev->records));
            update_data(records);
            return true;
        }
        else
            return QMainWindow::event(e);
    }

    void MainWindow::closeEvent(QCloseEvent * event)
    {
        config_.write_config();
        QMainWindow::closeEvent(event);
    }

    void MainWindow::db_load_task(
        const std::string& group,
        const std::string& subgroup,
        const dna_manipulation::MarkersRecord& haplotype)
    {
        try
        {
            std::unique_ptr<DBBackend::MarkersRecords> loaded_db;
            loaded_db.reset(new DBBackend::MarkersRecords);
            dbb_->read_records(group, subgroup, *loaded_db);
            MarkersRecordsPtr sorted_db(
                new dna_manipulation::SortedDB(haplotype, std::move(loaded_db)));
            QEvent* event = new FinishEvent(std::move(sorted_db));
            QCoreApplication::instance()->postEvent(this, event); 
        }
        catch(const AppException& e)
        {
            std::cerr << __func__ << ": " << e.what() << std::endl;
        }
    }

    void MainWindow::set_haplotype()
    {
        if(!add_haplotype_) {
            add_haplotype_ = new EnterHaplotypeDialog(this, config());
            add_haplotype_->setAttribute(Qt::WA_DeleteOnClose, true);
            connect(add_haplotype_, &QDialog::finished, this, &MainWindow::close_haplotype);
        }
        add_haplotype_->show();
        add_haplotype_->raise();
        add_haplotype_->activateWindow();
    }

    void MainWindow::show_tree()
    {
        if(tree_) {
            {
                std::lock_guard<std::mutex> lock(tree_dlg_lock_);
                tree_dlg_.reset(new TreeDialog());
                tree_dlg_->set_tree(tree_);
            }
            tree_dlg_->exec();
        }
    }

    void MainWindow::calc_common_ancester()
    {
        std::set<int> selected_rows;
        auto model = db_widget_->selectionModel();
        auto indexes = model->selectedRows();
        for(const auto& index : indexes)
            selected_rows.insert(index.row());
        if(ancestor_dlg_)
            ancestor_dlg_->close();
        ancestor_dlg_.reset(new AncestorTimeDialog(
            selected_rows, my_haplotype_, *db_model_->get_db()));
        ancestor_dlg_->show();
        ancestor_dlg_->raise();
        ancestor_dlg_->activateWindow();
    }

    void MainWindow::import_database()
    {
        if(!import_dlg_) {
            import_dlg_ = new ImportDbDialog(this, config());
            import_dlg_->setAttribute(Qt::WA_DeleteOnClose, true);
            connect(import_dlg_, &QDialog::finished, this, &MainWindow::close_import_dialog);
        }
        import_dlg_->show();
        import_dlg_->raise();
        import_dlg_->activateWindow();
    }

    void MainWindow::parse_markers_string_(
        const QString& markers_str,
        dna_manipulation::HaploSet::MarkersLine& markers_line) noexcept
    {
        QStringList values = markers_str.split(
            " ", QString::SplitBehavior::SkipEmptyParts);
        markers_line.resize(values.size());
        size_t j = 0;
        const auto& names =
            dna_manipulation::HaploSet::get_marker_indexes();
        //assume all marker names are valid
        for(int i = 0; i < values.size(); ++i) {
            auto it = names.find(values.at(i).toStdString());
            if(it != names.end())
                markers_line[j++] =
                    static_cast<dna_manipulation::SupportedMarkers>(it->second);
        }
        markers_line.resize(j);
    }

    void MainWindow::parse_haplotype_(
        const QString& model_name,
        const QString& haplo_type,
        const QString* custom_model,
        dna_manipulation::HaploSet::MarkersLine& markers_line,
        dna_manipulation::MarkersRecord& markers) const
    {
        try
        {
            if(model_name.isEmpty())
                return;
            if(custom_model)
                parse_markers_string_(*custom_model, markers_line);
            else 
                markers_line = dna_manipulation::HaploSet::get_markers(
                    model_name.toStdString());
            for(size_t i = 0; i < dna_manipulation::SupportedMarkers::MAX; ++i)
                markers.markers[i] = 0;
            for(size_t i = 0; i < markers_line.size(); ++i) {
                QString value = haplo_type.section(
                    ' ', i, i, QString::SectionSkipEmpty);
                markers.markers[markers_line[i]] =
                    static_cast<unsigned char>(value.toInt());
            }
        }
        catch(const dna_manipulation::LibException& e)
        {
            std::ostringstream err;
            err << __func__ << ": " << e.what();
            throw AppException(err.str());
        }
    }

    void MainWindow::close_haplotype(int code)
    {
        if(code == QDialog::Rejected) {
            add_haplotype_ = nullptr;//should be destroyed by Qt
            return;
        }
        bool need_reload = false;
        try
        {
            QString custom_model;
            bool custom = add_haplotype_->get_custom_model(custom_model);
            dna_manipulation::HaploSet::MarkersLine markers_line;
            dna_manipulation::MarkersRecord markers;
            const auto& model_name = add_haplotype_->get_model_name();
            const auto& haplo_type = add_haplotype_->get_haplo_type();
            parse_haplotype_(
                model_name,
                haplo_type,
                custom ? &custom_model : nullptr,
                markers_line,
                markers);
            if(custom)
                config_.custom_model = custom_model;
            config_.model_name = model_name;
            config_.haplotype = haplo_type;
            const auto& haplo_group = add_haplotype_->get_haplo_group();
            const auto& haplo_subgroup = add_haplotype_->get_haplo_subgroup();
            if(config_.haplo_group != haplo_group ||
               config_.haplo_subgroup != haplo_subgroup)
                need_reload = true;
            config_.haplo_group = haplo_group;
            config_.haplo_subgroup = haplo_subgroup;
            my_markers_ = markers_line;
            my_haplotype_ = markers;

        }
        catch(const AppException& e)
        {//TODO: need better handler of exception
            std::cerr << __func__ << ": " << e.what() << std::endl;
            return;
        }

        update_model_();
        if(need_reload) {
            std::thread t1([this] (
                const std::string& group,
                const std::string& subgroup,
                const dna_manipulation::MarkersRecord& haplotype) {
                    db_load_task(group, subgroup, haplotype);
                },
                config_.haplo_group.toStdString(),
                config_.haplo_subgroup.toStdString(),
                my_haplotype_);
            t1.detach();
        }
        else {
            MarkersRecordsPtr sorted_db(
                new dna_manipulation::SortedDB(my_haplotype_, *db_model_->get_db()));
            QEvent* event = new FinishEvent(std::move(sorted_db));
            QCoreApplication::instance()->postEvent(this, event); 
        }
        config_.write_config();
        add_haplotype_ = nullptr;//should be destroyed by Qt
    }

    void MainWindow::db_save_task(
        DBPtr db_rec,
        const std::string& group,
        const std::string& subgroup,
        const std::string& load_subgroup,
        const dna_manipulation::MarkersRecord& haplotype,
        bool reload_data)
    {
        for(size_t i = 0; i < db_rec->size(); ++i)
            try
            {
                dbb_->write_record(group, subgroup, (*db_rec)[i]);
            }
            catch(const AppException& e)
            {
                std::cerr << "Can't write to database: " << e.what() << std::endl;
            }
        db_rec.reset();
        if(reload_data)
            db_load_task(group, load_subgroup, haplotype);
    }

    void MainWindow::close_import_dialog(int code)
    {
        DBPtr db_rec(std::move(import_dlg_->get_exported_data()));
        if(code == QDialog::Rejected) {
            import_dlg_ = nullptr;
            return;
        }
        config_.write_config();
        if(db_rec && !db_rec->empty()) {
            bool reload_data = false;
            if(config_.import_group == config_.haplo_group && 
               QString::compare(config_.haplo_subgroup, config_.import_subgroup) <= 0 )
            {
                reload_data = true;
            }
            std::thread t1([this, db_rec, reload_data] (
                const std::string& group,
                const std::string& subgroup,
                const std::string& load_subgroup)
                {
                    db_save_task(
                        db_rec,
                        group,
                        subgroup,
                        load_subgroup,
                        my_haplotype_,
                        reload_data);
                },
                config_.import_group.toStdString(),
                config_.import_subgroup.toStdString(),
                config_.haplo_subgroup.toStdString());
            t1.detach();
        }
        import_dlg_ = nullptr;
    }
    
    void MainWindow::parse_filter_markers_() noexcept
    {
        QStringList restrictions = config_.filter_string.split(
            ";", QString::SplitBehavior::SkipEmptyParts);
        constexpr const auto& names =
            dna_manipulation::HaploSet::get_marker_indexes();
        filter_markers_.clear();
        for(auto i = 0; i < restrictions.size(); ++i) {
            QStringList parts = restrictions[i].split(
                " ", QString::SplitBehavior::SkipEmptyParts);
            if(parts.size() != 3)
                continue;
            auto it = names.find(parts[0].toStdString());
            if(it == names.end())
                continue;
            auto marker = static_cast<dna_manipulation::SupportedMarkers>(it->second);
            filter_markers_.insert(marker);
        }
    }

    void MainWindow::enter_filter()
    {
        std::unique_ptr<FilterDialog> dlg(new FilterDialog(
            config_.filter_string,
            config_.filter_regexp,
            my_haplotype_, my_markers_));
        if(dlg->exec() == QDialog::Accepted) {
            auto str = dlg->get_filter();
            auto regexp = dlg->regexp();
            if(str != config_.filter_string || regexp != config_.filter_regexp) {
                if(str != config_.filter_string) {
                    config_.filter_string = str;
                    parse_filter_markers_();
                }
                else 
                    config_.filter_regexp = regexp;
                filter_label_->setText(config_.filter_string);
                apply_filter_();
            }
        }
    }

    void MainWindow::custom_view()
    {
        std::unique_ptr<CustomViewDialog> dlg(new CustomViewDialog(
            choose_marker_line(config_.view_config.show_option)));
        if(dlg->exec() == QDialog::Accepted) {
            custom_markers_ = dlg->get_markers_line();
            QString markers_str;
            const auto& names = dna_manipulation::HaploSet::get_supported_marker_names();
            for(size_t i = 0; i < custom_markers_.size(); ++i) {
                if(i)
                    markers_str += ' ';
                markers_str += QString::fromStdString(names[custom_markers_[i]]);
            }
            config_.view_config.custom_markers = markers_str;
            if(config_.view_config.show_option == ViewConfig::SW_CUSTOM)
                change_view(ViewConfig::SW_CUSTOM, true);
        }
    }

    void MainWindow::apply_filter_() noexcept
    {
        auto db = db_model_->get_db();
        if(db)
        {
            const auto& filter = filter_cache_.get_filter(config_.filter_string, config_.filter_regexp, *db);
            for(size_t i = 0; i < db->size(); ++i)
            {
                auto hide = config_.enable_filter ? (filter.count(db->index(i)) == 0) : false;
                if(db_widget_->isRowHidden(i) != hide)
                    db_widget_->setRowHidden(static_cast<int>(i), hide);
            }
        }
    }

    void MainWindow::turn_filter(bool on)
    {
        if(filter_box_->isChecked() != on) {
            filter_box_->blockSignals(true);
            filter_box_->setChecked(on);
            filter_box_->blockSignals(false);
        }
        if(filter_check_->isChecked() != on) {
            filter_check_->blockSignals(true);
            filter_check_->setChecked(on);
            filter_check_->blockSignals(false);
        }
        config_.enable_filter = on;
        apply_filter_();
    }

    void MainWindow::view_database()
    {
        std::unique_ptr<EditDbDialog> dlg(new EditDbDialog(this, config()));
        if(dlg->exec() == QDialog::Accepted) {
        }
    }

    const int FinishEvent::event_id = QEvent::registerEventType(QEvent::User);

    FinishEvent::FinishEvent(MarkersRecordsPtr&& new_records)
        : QEvent(static_cast<QEvent::Type>(event_id)),
          records(std::move(new_records))
    {
    }

}
