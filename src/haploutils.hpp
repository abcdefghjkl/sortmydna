/**
  file: haploutils.hpp 2015-2019 Andrey Gusev

  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/


#pragma once

#include"haplotype.hpp"
#include<algorithm>
#include<string>
#include<fstream>
#include<vector>

namespace dna_manipulation
{
    class FileReader
    {
    public:
        FileReader(const char* name);
        ~FileReader() noexcept;

        size_t read_lines(size_t count, std::vector<std::string>& lines);
    private:
        std::ifstream ifs_;
    };

    class SubString
    {
    public:
        SubString() = default;

        SubString(const std::string& str) noexcept
            : start_(str.data()), end_(start_ + str.size()) {};

        SubString(const char* start, const char* end) noexcept
            : start_(start), end_(end) {};

        std::string str() const { return std::string(start_, end_); }

        size_t size() const noexcept { return end_ - start_; }

        bool empty() const noexcept { return end_ == start_; }

        char operator[] (size_t index) const noexcept {return start_[index]; }

        bool operator== (const std::string& cmp) {
            return (size() == cmp.size() && std::equal(start_, end_, cmp.c_str()));
        }

        operator std::string () const { return str(); }

        const char* begin() const noexcept { return start_; }

        const char* end() const noexcept { return end_; }

    private:
        const char* start_;
        const char* end_;
    };

    //set of symbols as delimiters
    struct Delimiters
    {
        Delimiters(const char* delim)
            : delimiters_(delim) {};
        //check that symb is delimeter
        bool operator() (char symb) const noexcept
            { return delimiters_.find(symb) != std::string::npos; }

    private:
        const std::string delimiters_;
    };

    extern const char* space_delimiters;
    extern const Delimiters s_delim;

    template<class DELIMETERS = Delimiters>
    class Splitter
    {
    public:
        Splitter(
            const char* str,
            size_t len,
            const DELIMETERS& delim = s_delim) noexcept
            : data_(str, str + len), delimiter_(delim) {};

        explicit Splitter(
            const std::string& str,
            const DELIMETERS& delim = s_delim) noexcept
            : data_(str), delimiter_(delim) {};

        bool get_token(SubString& substr) noexcept;

    private:
        size_t last_position_ = 0;
        const SubString data_;
        const DELIMETERS delimiter_;
    };

    template<class DELIMETERS>
    bool Splitter<DELIMETERS>::get_token(SubString& substr) noexcept
    {
        while(last_position_ < data_.size() && delimiter_(data_[last_position_]))
            ++last_position_;
        if(last_position_ == data_.size())
            return false;
        size_t start = last_position_++;
        while(last_position_ < data_.size() && !delimiter_(data_[last_position_]))
            ++last_position_;
        substr = SubString(data_.begin() + start, data_.begin() + last_position_);
        return true;
    }

}
