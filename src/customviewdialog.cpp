#include"customviewdialog.hpp"

#include<QVBoxLayout>
#include<QCheckBox>
#include<QTableWidget>
#include<QDialogButtonBox>
#include"sortmydnaconfig.h"

namespace sortmydna
{
    CustomViewDialog::CustomViewDialog(
        const dna_manipulation::HaploSet::MarkersLine& line)
        : old_line_(line),
          line_(line)
    {
        QString dialogname = PROJECT_NAME;
        dialogname += tr(" - setting custom view");
        setWindowTitle(dialogname);

        table_ = setup_custom_table_();

        QDialogButtonBox* bbox = new QDialogButtonBox(
            QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
        connect(bbox, &QDialogButtonBox::accepted, this, &CustomViewDialog::accept);
        connect(bbox, &QDialogButtonBox::rejected, this, &CustomViewDialog::reject);

        QVBoxLayout *main_layout = new QVBoxLayout;
        main_layout->addWidget(table_);
        main_layout->addWidget(bbox);
        setLayout(main_layout);
    }

    void CustomViewDialog::accept()
    {
        line_.resize(table_->rowCount());
        size_t j = 0;
        for(int i = 0; i < table_->rowCount(); ++i) {
            QCheckBox* widget = static_cast<QCheckBox*>(
                table_->cellWidget(i, TableIndexes::TI_CHECK));
            if(widget->isChecked())
                line_[j++] = static_cast<dna_manipulation::SupportedMarkers>(i);
        }
        line_.resize(j);
        QDialog::accept();
    }

    QTableWidget* CustomViewDialog::setup_custom_table_() noexcept
    {
        static const QStringList c_names = {
            tr("Name"), tr("Checked")};
        //const QStringList& names = get_maker_names_(line_);
        const auto& names = dna_manipulation::HaploSet::get_supported_marker_names();
        QTableWidget* table = new QTableWidget(names.size(), c_names.size());
        table->setHorizontalHeaderLabels(c_names); 
        size_t j = 0;
        for(size_t i = 0; i < names.size(); ++i) {
            QTableWidgetItem* item = new QTableWidgetItem(QString::fromStdString(names[i]));
            item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            table->setItem(i, TableIndexes::TI_NAME, item);
            QCheckBox* check = new QCheckBox;
            if(j <  line_.size() && line_[j] == i) {
                check->setChecked(true);
                ++j;
            }
            else
                check->setChecked(false);
            table->setCellWidget(i, TableIndexes::TI_CHECK, check);
        }
        return table;
    }

    QStringList CustomViewDialog::get_maker_names_(
        const dna_manipulation::HaploSet::MarkersLine& markers) noexcept
    {
        QStringList headers;
        const auto& names = dna_manipulation::HaploSet::get_supported_marker_names();
        for(size_t i = 0; i < markers.size(); ++i) {
            const auto& name = names[markers[i]];
            headers << QString::fromStdString(name);
        }
        return headers;
    }

}

