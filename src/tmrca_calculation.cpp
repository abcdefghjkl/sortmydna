/**
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include<cmath>
#include"tmrca_calculation.hpp"

namespace dna_manipulation
{
    BaseHaploCalculator::BaseHaploCalculator(int threshold) noexcept 
       : threshold_(threshold)
    {
        for(size_t j = 0; j < SupportedMarkers::MAX; ++j) {
            all_valuable_[j] = true;
        }
    }

    void BaseHaploCalculator::add(const MarkersArray& markers)
    {
        for(size_t j = 0; j < markers.size(); ++j) {
            if(markers[j])
                ++freq_[j][markers[j]];
            else
                all_valuable_[j] = false;
        }
        ++count_haplotypes_;
    }

    void BaseHaploCalculator::calc() noexcept
    {
        for(size_t j = 0; j < SupportedMarkers::MAX; ++j) {
            size_t max_freq = 0;
            size_t count_records = 0;
            for(auto it = freq_[j].begin(); it != freq_[j].end(); ++it) { 
                count_records += it->second;
                if(max_freq < it->second) {
                    max_freq = it->second;
                    base_[j] = it->first;
                }
            }
            size_t min_count = count_records * threshold_ / 100;
            if(max_freq < min_count) {
                base_[j] = 0;
            }
        }
    }

    void TMRCAAgeCalculator::calc() noexcept
    {
        BaseHaploCalculator::calc();
        count_markers_ = 0;
        mut_speed_ = 0;
        for(size_t j = 0; j < SupportedMarkers::MAX; ++j)
            if(all_valuable_[j]) {
                for(auto it = freq_[j].begin(); it != freq_[j].end(); ++it) { 
                    size_t diff;
                    if(it->first < base_[j])
                        diff = base_[j] - it->first;
                    else if(it->first > base_[j])
                        diff = it->first - base_[j];
                    else
                        continue;
                    count_muatations_ += diff * it->second; 
                }
                ++count_markers_;
                mut_speed_ += HaploSet::get_mutation_speed(
                    static_cast<SupportedMarkers>(j));
            }
        if(count_markers_ && count_haplotypes_ && count_muatations_) {
            float mut_rate = static_cast<float>(count_muatations_) / count_haplotypes_;
            age_distance_ = mut_rate / mut_speed_;
            if(age_distance_ >= count_markers_) {
                float correction =  mut_rate / count_markers_;
                age_distance_ +=  correction / count_haplotypes_ * (1.0f + exp(correction));
            }
            float corr = (1.0f / sqrt(count_muatations_)) * 100.0f;
            corr = sqrt(corr * corr + 100.0f);
            age_distance_ *= 25;
            age_error_ = age_distance_ * corr / 100;
        }
    }

}
