#ifndef SORT_MY_DNA_HELP_DIALOG_HPP
#define SORT_MY_DNA_HELP_DIALOG_HPP

#include<QDialog>

namespace sortmydna
{
    class AboutDialog : public QDialog
    {
    Q_OBJECT
    public:
        AboutDialog();
        virtual ~AboutDialog() {};
    };
}

#endif
