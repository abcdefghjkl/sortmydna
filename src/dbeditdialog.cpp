#include"dbeditdialog.hpp"
#include<QVBoxLayout>
#include<QTableView>

namespace sortmydna
{
    EditDbDialog::EditDbDialog(QWidget* parent, const AppConfig& config)
    : QDialog(parent),
      config_(config)
    {
        QVBoxLayout *vbox = new QVBoxLayout;
        QTableView* view = new QTableView;
        vbox->addWidget(view);
        setLayout(vbox);
    }
}
