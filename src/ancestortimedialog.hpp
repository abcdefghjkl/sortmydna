#ifndef ANCESTOR_TIME_DIALOG_HPP
#define ANCESTOR_TIME_DIALOG_HPP

#include<set>
#include<QDialog>

namespace dna_manipulation
{
    class SortedDB;
    struct MarkersRecord;
}

class QTableWidget;
namespace sortmydna
{
    class AncestorTimeDialog: public QDialog
    {
    Q_OBJECT
    public:
        AncestorTimeDialog(
            const std::set<int>& indexes,
            const dna_manipulation::MarkersRecord& my_rec,
            const dna_manipulation::SortedDB& db);

    protected:
        virtual void showEvent(QShowEvent * event);

        QTableWidget* setup_base_table_(
            const dna_manipulation::MarkersRecord& base);

        QTableWidget* setup_common_age_table_(
            size_t count_muatations,
            size_t age_distance,
            size_t age_error,
            size_t count_markers,
            double mut_speed);

        QTableWidget* setup_age_table_(
            const std::set<int>& indexes,
            const dna_manipulation::MarkersRecord& my_rec,
            const dna_manipulation::SortedDB& db);
    };
}

#endif
