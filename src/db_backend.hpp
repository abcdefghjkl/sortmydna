/**
  file: db_backend.hpp 2015-2019 Andrey Gusev
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include"markers_db.hpp"
#include"haplotype.hpp"

namespace sortmydna
{
    class DBBackend
    {
    public:
        typedef dna_manipulation::MarkersDBBase::DBContainer
            MarkersRecords;

        inline
        DBBackend() = default;

        inline
        virtual ~DBBackend() {};

        //iniatialization database
        inline
        virtual void init(const std::string& connection) = 0;

        //fill database by default data
        virtual void fill_default() = 0;

        //read additional marker records
        virtual void read_records(
            const std::string& group,
            const std::string& subgroup,
            MarkersRecords& rec) = 0;

        //load haplo models
        virtual dna_manipulation::SharedDBMarkersLinePtr
        read_model(const std::string* const name) = 0;

        //save haplo model
        virtual void write_model(
            const std::string& name,
            const dna_manipulation::HaploSet::MarkersLine& line) = 0;

        //save marker records
        virtual void write_record(
            const std::string& group,
            const std::string& subgroup,
            const dna_manipulation::MarkersRecord& rec) = 0;
    };
}
