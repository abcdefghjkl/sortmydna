/**
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include"markers_db.hpp"
#include"tmrca_calculation.hpp"
#include"ancestortimedialog.hpp"
#include<QTableWidget>
#include<QHeaderView>
#include<QVBoxLayout>
#include<QDialogButtonBox>

namespace sortmydna
{
    AncestorTimeDialog::AncestorTimeDialog(
        const std::set<int>& indexes,
        const dna_manipulation::MarkersRecord& my_rec,
        const dna_manipulation::SortedDB& db)
    {
       QTableWidget* table = setup_age_table_(indexes, my_rec, db);

       dna_manipulation::MarkersRecord base;
       dna_manipulation::TMRCAAgeCalculator calculator;
       dna_manipulation::MarkersDBUtils::calc_for_set(db, calculator, indexes);

       base.markers = calculator.base();

       QTableWidget* base_table = setup_base_table_(base);
       QTableWidget* common_age_table = setup_common_age_table_(
           calculator.count_mutations(),
           calculator.age_distance(),
           calculator.age_error(),
           calculator.count_markers(),
           calculator.mutation_speed());
           
       QDialogButtonBox* bbox = new QDialogButtonBox(QDialogButtonBox::Ok);
       connect(bbox, &QDialogButtonBox::accepted, this, &AncestorTimeDialog::accept);
       QVBoxLayout *main_layout = new QVBoxLayout;
       //main_layout->setSizeConstraint(QLayout::SetMinimumSize);
       main_layout->addWidget(base_table);
       main_layout->addWidget(common_age_table);
       main_layout->addWidget(table);
       main_layout->addWidget(bbox);
       setLayout(main_layout);
       setWindowTitle(tr("Common ancestor age calculation"));
    }

    QTableWidget* AncestorTimeDialog::setup_base_table_(
        const dna_manipulation::MarkersRecord& base)
    {
        const auto& names = dna_manipulation::HaploSet::get_supported_marker_names();
       unsigned char indexes[dna_manipulation::SupportedMarkers::MAX];
       size_t count = base.valuable_indexes(indexes);
       QTableWidget* table = new QTableWidget(1, count + 1);
       table->verticalHeader()->setVisible(false);
       table->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
       table->setHorizontalHeaderItem(0, new QTableWidgetItem("Count markers"));
        QTableWidgetItem* item =
            new QTableWidgetItem(QString("%1").arg(count));
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        table->setItem(0, 0, item);

       for(size_t i = 0; i < count; i++) {
           table->setHorizontalHeaderItem(
               i + 1,
               new QTableWidgetItem(
                   QString::fromStdString(names[indexes[i]])));
            QTableWidgetItem* item =
                new QTableWidgetItem(QString("%1").arg(base[indexes[i]]));
            item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            table->setItem(0, i + 1, item);
       }

       table->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
       table->resizeRowsToContents();
       table->verticalHeader()->setVisible(false);
       table->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
       table->setMaximumHeight(table->horizontalHeader()->size().height() * 3);

       return table;
    }

    QTableWidget* AncestorTimeDialog::setup_common_age_table_(
        size_t count_mutations,
        size_t age_distance,
        size_t age_error,
        size_t count_markers,
        double mut_speed)
    {
        const QStringList headers = {
            tr("count mutations"), tr("count markers"), tr("mutation speed"),
            tr("age distance"), tr("age error")};
        QTableWidget* table = new QTableWidget(1, 5);
        table->setHorizontalHeaderLabels(headers);
        table->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
        table->horizontalHeader()->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

       QTableWidgetItem* item =
           new QTableWidgetItem(QString("%1").arg(count_mutations));
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        table->setItem(0, 0, item);
        item = new QTableWidgetItem(QString("%1").arg(count_markers));
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        table->setItem(0, 1, item);
        item = new QTableWidgetItem(QString("%1").arg(mut_speed));
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        table->setItem(0, 2, item);
        item = new QTableWidgetItem(QString("%1").arg(age_distance));
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        table->setItem(0, 3, item);
        item = new QTableWidgetItem(QString("%1").arg(age_error));
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        table->setItem(0, 4, item);
        table->verticalHeader()->setVisible(false);
        table->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
        table->setMaximumHeight(table->horizontalHeader()->size().height() * 3);
        return table;
    }

    QTableWidget* AncestorTimeDialog::setup_age_table_(
        const std::set<int>& indexes,
        const dna_manipulation::MarkersRecord& my_rec,
        const dna_manipulation::SortedDB& db)
    {
        const QStringList headers = {
            tr("info"), tr("count markers"), tr("count mutations"),
            tr("mutation speed"), tr("age distance"), tr("age error")};
        QTableWidget* table = new QTableWidget(indexes.size(), headers.size());
        table->setHorizontalHeaderLabels(headers);
        //table->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        table->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
        table->horizontalHeader()->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        int row = 0;
        for(const int index : indexes) {
            const auto& rec = db[index];
            double mutation_speed;
            unsigned long mutation_distance;
            unsigned long age_distance;
            unsigned long age_error;
            size_t count_markers =
                dna_manipulation::MarkersDBUtils::calc_common_ancestor_age(
                    my_rec, rec, mutation_speed, mutation_distance, age_distance, age_error);
            table->setVerticalHeaderItem(
                row, new QTableWidgetItem(QString::fromStdString(rec.id)));
            QTableWidgetItem* item =
                new QTableWidgetItem(QString::fromStdString(rec.info + rec.snp));
            item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            table->setItem(row, 0, item);
            item = new QTableWidgetItem(QString("%1").arg(count_markers));
            item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            table->setItem(row, 1, item);
            item = new QTableWidgetItem(QString("%1").arg(mutation_distance));
            item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            table->setItem(row, 2, item);
            item = new QTableWidgetItem(QString("%1").arg(mutation_speed));
            item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            table->setItem(row, 3, item);
            item = new QTableWidgetItem(QString("%1").arg(age_distance));
            item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            table->setItem(row, 4, item);
            item = new QTableWidgetItem(QString("%1").arg(age_error));
            item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            table->setItem(row, 5, item);
            ++row;
        }
        return table;
    }

    void AncestorTimeDialog::showEvent(QShowEvent * event)
    {
        QDialog::showEvent(event);
    /*    auto sz = size();
        sz.setHeight(sz.height() * 2);
        sz.setWidth(sz.width() * 2);*/
        resize(maximumSize());
    }

}

