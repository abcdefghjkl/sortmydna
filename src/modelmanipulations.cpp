/**
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include"modelmanipulations.hpp"
#include"haploutils.hpp"

namespace sortmydna
{
    ModelPtr Model::create_model(
        const std::list<dna_manipulation::SubString>& columns) 
    {
        auto item_valuable = [](const dna_manipulation::SubString& str) -> bool
        {
            for(auto i = 0U; i < str.size(); ++i) 
                if(std::isprint(str[i]) && !std::isspace(str[i]))
                        return true;
            return false;

        };
        ModelPtr model(new Model);
        const std::unordered_map<std::string, int>& multi_markers =
            dna_manipulation::HaploSet::get_multi_markers();
        const auto& marker_indexes =
            dna_manipulation::HaploSet::get_marker_indexes();
        model->resize(columns.size() + dna_manipulation::HaploSet::count_multi_markers);
        size_t field_num = 0;
        
        model->data = true;//assume this data
        unsigned char last_multi_index = 0;
        std::string text_field;
        size_t count_empty_fields = 0;
        for(const auto& item :columns) {
            if(!item_valuable(item)) {//count empty fields in the end, assume they are 0
                if(model->data) {
                    (*model)[field_num].type = FieldInfo::FT_MARKER;
                    (*model)[field_num].name = "0";
                    ++model->count_markers;
                    ++count_empty_fields;
                    ++field_num;
                }
                continue;
            }
            else
                count_empty_fields = 0;
            std::string name;//check markers in upper case
            name.resize(item.size());
            for(size_t i = 0; i < item.size(); ++i)
                name[i] = std::toupper(item[i]);
            if(name[0] == '"') {//this begining of text field
                text_field.assign(name, 1, name.size() - 1);
                continue;
            }
            else if(!text_field.empty()) {
                if(*name.rbegin() == '"') { 
                    text_field.append(name, 0, name.size() - 1);
                    name.swap(text_field);
                    text_field.clear();
                }
                else {
                    text_field += name;
                    continue;
                }
            }
            bool as_text = false;
            auto it_m = marker_indexes.find(name);
            if(it_m != marker_indexes.end()) {
                ++model->count_markers;
                model->data = false;
                (*model)[field_num].type = FieldInfo::FT_MARKER;
                (*model)[field_num].name = it_m->second;
                ++field_num;
            }
            else {
                auto it = multi_markers.find(name);
                if(it != multi_markers.end()) {
                    model->count_markers += it->second;
                    model->data = false;
                    auto it_m = marker_indexes.find(name + 'A');
                    unsigned char index = it_m->second;
                    if(last_multi_index != index) {
                        //filter repeating multi index column name
                        last_multi_index = index;
                        for(unsigned char i = 0; i < it->second; ++i) {
                            (*model)[field_num].type = FieldInfo::FT_MARKER;
                            (*model)[field_num].name = index++;
                            ++field_num;
                        }
                    }
                }
                else if(model->data) {
                    //analize to detect data markers or text
                    //if threre are only numbers it is marker
                    //it there ary the '-' and markers it is multimarker
                    //else it is a text
                    size_t count_markers = 1;
                    size_t count_digits = 0;
                    bool this_marker = true;
                    for(size_t i = 0; i < item.size(); ++i) {
                        if(std::isdigit(item[i])) {
                            if(++count_digits > 2) {
                                this_marker = false;
                                break;
                            }
                        }
                        else {//i != 0 && i != item.size() - 1 &&
                            if(item[i] == '-' && (count_digits > 0) && item.size() != 1){//multimarker
                                ++count_markers;
                            }
                            else {
                                this_marker = false;
                                break;
                            }
                            count_digits = 0;
                        }
                    }
                    if(this_marker && model->count_markers <
                            dna_manipulation::SupportedMarkers::MAX) {
                        size_t start_offset = 0;
                        for(size_t i = 0; i <= item.size(); ++i)
                           if(i == item.size() || item[i] == '-') {
                                (*model)[field_num].type = FieldInfo::FT_MARKER;
                                (*model)[field_num].name.assign(
                                        item.begin () + start_offset, i - start_offset);
                                start_offset = i + 1;
                                ++field_num;
                           }
                        model->count_markers += count_markers;
                    }
                    else
                        as_text = true;
                }
                else //is not data model all other fields are text
                    as_text = true;
            }
            if(as_text) {
                (*model)[field_num].type =
                    (model->count_text < FieldInfo::FT_INFO ?
                    static_cast<FieldInfo::FieldType>(model->count_text) :
                    FieldInfo::FT_INFO);
                (*model)[field_num].name.swap(name);
                ++model->count_text;
                ++field_num;
            }
            ++model->count_fields;
        }
        if(count_empty_fields) {
            //strip emprty fields in data model
            model->count_markers -= count_empty_fields;
            field_num -= count_empty_fields;
        }
        model->resize(field_num);
        //Can't be less than 6 markers, it is text
        if(model->count_markers < 6) {
            model->data = false;
            for(size_t i = 0; i < model->size(); ++i)
                if((*model)[i].type == FieldInfo::FT_MARKER) {
                    (*model)[i].type = FieldInfo::FT_INFO;
                    (*model)[i].name = std::to_string((*model)[i].name[0]); 
                }
            model->count_text += model->count_markers;
            model->count_markers = 0;
        }
        return model;
    }

    Model::ParseResult Model::check_model(const Model& base_model) noexcept
    {
        if(base_model.data)
            return PR_CANCEL;
        if(base_model.count_markers != count_markers) 
            return ParseResult::PR_CANCEL;
        size_t count_text_fields = 0;
        if(!data) {
            size_t j = 0;
            for(size_t i = 0; i < size(); ++i) {
                if(j == base_model.size())
                    return PR_CANCEL;
                if((*this)[i].type == FieldInfo::FT_MARKER) {
                    for(; j < base_model.size(); ++j)
                        if(base_model[j].type == FieldInfo::FT_MARKER) {
                            if(base_model[j].name != (*this)[i].name)
                                return PR_CANCEL;
                            else {
                                ++j;
                                break;
                            }
                        }
                }
                else {
                    if(base_model[j].type == FieldInfo::FT_MARKER)
                        return PR_CANCEL;
                    ++count_text_fields;
                    ++j;
                }
            }
            if(count_text_fields == base_model.count_text)
                return ParseResult::PR_SUCCESS;
            else
                return PR_CANCEL;
        }
        //data model
        size_t j = 0;
        for(size_t i = 0; i < size(); ++i) {
            if(j == base_model.size())
                return PR_CANCEL;
            if((*this)[i].type == FieldInfo::FT_MARKER) {
                    if(base_model[j].type == FieldInfo::FT_MARKER &&
                       ((*this)[i].name == "0" || 
                       dna_manipulation::HaploSet::check_marker(
                           static_cast<dna_manipulation::SupportedMarkers>(base_model[j].name[0]),
                           std::stoi((*this)[i].name)))) {
                        ++j;
                    }
                    else 
                        return PR_CANCEL;
            }
            else if(base_model[j].type != FieldInfo::FT_MARKER) {
                ++count_text_fields;
                ++j;
            }
            else {
                return PR_CANCEL;
            }
        }
        if(count_text_fields == base_model.count_text)
            return ParseResult::PR_SUCCESS;
        else
            if(count_text_fields < base_model.count_text)
                return PR_FAIL;
            else
                return PR_CANCEL;
    }

    ModelPtr Model::create_base_model() const
    {
        ModelPtr new_model(new Model);
        std::vector<dna_manipulation::SubString> data_markers(count_markers);
        size_t j = 0;
        for(size_t i = 0; i <  size(); ++i)
            if((*this)[i].type == FieldInfo::FT_MARKER)
                data_markers[j++] = dna_manipulation::SubString((*this)[i].name);
        data_markers.resize(j);
        dna_manipulation::HaploSet::MarkersLineInfo info_line =
            dna_manipulation::HaploSet::choose_markers_line(data_markers);
        new_model->name = info_line.name;
        new_model->count_markers = info_line.line.size();
        new_model->count_fields = count_fields;
        new_model->accept_data = PR_FAIL;
        new_model->data = false;
        j = 0;
        new_model->resize(size());
        for(size_t i = 0; i <  size(); ++i) {
            (*new_model)[i].type = (*this)[i].type;
            if((*this)[i].type == FieldInfo::FT_MARKER)
                (*new_model)[i].name = static_cast<unsigned char>(info_line.line[j++]);
            else
                ++new_model->count_text;
        }
        return new_model;
    }

    void Model::adapt_model(const Model& base_model)
    {
        size_t j = 0;
        for(size_t i = 0; i <  size(); ++i)
            if((*this)[i].type != FieldInfo::FT_MARKER) {
                for(; j < base_model.size(); ++j)
                    if(base_model[j].type != FieldInfo::FT_MARKER) {
                        (*this)[i].type = base_model[j++].type;
                        break;
                    }
            }
    }

    bool Model::operator==(const Model& info) const
    {
        //don't compary accept_data
        return (this == &info ||
                (count_markers == info.count_markers  &&
                count_fields == info.count_fields  &&
                count_text == info.count_text &&
                data == info.data &&
                *this == static_cast<const FieldDescription&>(info)));
    }

}

