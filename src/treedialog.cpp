/**
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include<cmath>
#include"dna_tree.hpp"
#include"treedialog.hpp"
#include<QApplication>
#include<QHBoxLayout>
#include<QVBoxLayout>
#include<QPushButton>
#include<QDialogButtonBox>
#include<QScrollArea>
#include<QPixmap>
#include<QBitmap>
#include<QPainter>
#include<QPicture>
#include<QLabel>
#include<iostream>
#include<thread>

namespace sortmydna
{
    TreeDialog::TreeDialog()
    {
        QScrollArea* scrollArea = new QScrollArea;
        view_label_ = new QLabel;
        view_label_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        scrollArea->setWidget(view_label_);

        QPushButton *v_button = new QPushButton(tr("View tree"));
        v_button->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        connect(v_button, &QAbstractButton::clicked, this, &TreeDialog::draw_tree);

        QDialogButtonBox* bbox = new QDialogButtonBox(QDialogButtonBox::Ok);
        bbox->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        connect(bbox, &QDialogButtonBox::accepted, this, &TreeDialog::accept);

        QVBoxLayout* control_layout = new QVBoxLayout;
        control_layout->addWidget(v_button);
        control_layout->addWidget(bbox);

        QHBoxLayout *main_layout = new QHBoxLayout;
        main_layout->addWidget(scrollArea);
        main_layout->addLayout(control_layout);
        setLayout(main_layout);
    }

    void TreeDialog::set_tree(TreePtr& tree)
    {
        //std::lock_guard<std::mutex> guard(tree_lock_);
        tree_ = tree;
    }

    /*
    void TreeDialog::setup_tree(
        const dna_manipulation::SortedDB& db,
        TreeDialog* ptr) noexcept
    {
        ptr->set_tree(tree);
    }*/

    void TreeDialog::draw_block_(
        QPixmap* pixmap,
        const StrVector& strs,
        int x_pos,
        int y_pos)
    {
        QPainter p(pixmap);
        int text_pos = y_pos;
        int width = 0;
        for(const auto& value : strs) {
            //std::cout << "draw " << value.key.toStdString() << std::endl;
            p.drawText(x_pos + 1, text_pos + value.size.height() - 1, value.key);
            text_pos += value.size.height();
            width = std::max(value.size.width(), width);
        }
        width += 3;
        if(text_pos != y_pos) {
            p.drawRect(x_pos, y_pos, width, text_pos);
            //std::cout << "draw rect " << x_pos << 'x' << y_pos << "x" << width << 'x' <<  text_pos << std::endl;
        }
        p.end();
    }

    void TreeDialog::fill_size_(StringDescriptor& descr)
    {
        if(!metrics_)
            metrics_.reset(new QFontMetrics(QApplication::font()));
        descr.size = metrics_->size(0, descr.key);
    }

    QString TreeDialog::get_marker_str_(
        unsigned char marker, unsigned char value)
    {
        const auto& names =
            dna_manipulation::HaploSet::get_supported_marker_names();
        QString str = QString::fromStdString(names[marker]);
        str.append('=');
        str += QString("%1").arg(static_cast<int>(value));
        return str;
    }

    TreeDialog::StringDescriptor TreeDialog::calc_mutations_block_size_(
        const dna_manipulation::DNATree::Mutations& mutations) noexcept
    {
        size_t count = sqrt(mutations.size()); 
        size_t mod = mutations.size() % count;
        if(mod != 0 && count > 1 && mod < mutations.size() % (count - 1))
            --count;
        StringDescriptor descr;
        StringDescriptor ret;
        ret.size.rwidth() = 0;
        ret.size.rheight() = 0;
        for(size_t i = 0; i < mutations.size(); ++i) {
            const auto& marker = mutations[i];
            descr.key += get_marker_str_(marker.marker, marker.value);
            if((i + 1) % count == 0 || i == mutations.size() - 1) {
                fill_size_(descr);
                if(ret.size.rwidth() < descr.size.width())
                    ret.size.rwidth() = descr.size.width();
                ret.size.rheight() += descr.size.height() + 1;
                if(!ret.key.isEmpty())
                    ret.key.append('\n');
                ret.key += descr.key;
                descr.key.clear();
            }
            else
                descr.key.append(' ');
        }
        ret.size.rwidth() = ret.size.width() + 5;
        ret.size.rheight() = ret.size.height() + 2;
        return ret;
    }

    void TreeDialog::draw_mutations_block_(
        int x,
        int y,
        QPainter& pnt,
        const StringDescriptor& descr) noexcept
    {
        QRectF rect(x, y, descr.size.width(), descr.size.height());
        std::cout << "Rect at " << rect.x() << 'x' << rect.y() << 'x' << rect.width() << 'x' << rect.height() << ' ' << std::endl;
        pnt.drawRoundedRect(rect, 20, 20);
        rect.setX(rect.x() + 1);
        rect.setY(rect.y() + 1);
        rect.setWidth(rect.width() - 2);
        rect.setHeight(rect.height() - 1);
        std::cout << "Text at " << rect.x() << 'x' << rect.y() << 'x' << rect.width() << 'x' << rect.height() << ' ' << descr.key.toStdString() << std::endl;
        pnt.drawText(rect, Qt::AlignLeft, descr.key);
    }

    template<class ITER>
    TreeDialog::BlockDescriptor TreeDialog::calc_block_params_(ITER begin, size_t count) noexcept
    {
        BlockDescriptor res;
        if(!count) {
            res.size.setWidth(0);
            res.size.setHeight(0);
            return res;
        }
        res.positions.resize(count);
        if(count == 1) {
            res.positions[0].setX(0);
            res.positions[0].setY(0);
            res.size.setHeight(begin->height());
            res.size.setWidth(begin->width());
        }
        else {
            //int min_width = begin->width();
            //++begin;
            struct FloorData
            {
                FloorData(int w = 0, int h = 0, int p = 0)
                 : width(w), height(h), position(p) {}; 
               int width;
               int height;
               int position;
            };
            std::vector<FloorData> floors;
            std::vector<size_t> indexes;
            floors.reserve(count);
            indexes.resize(count);
            size_t ind = 0;
            std::generate(
                indexes.begin(),
                indexes.end(),
                [&ind] {return ind++;});
            std::sort(
                indexes.begin(),
                indexes.end(),
                [begin, count](size_t left, size_t right) {
                    auto left_it = begin;
                    auto right_it = begin;
                    std::advance(left_it, left);
                    std::advance(right_it, right);
                    return left_it->height() > right_it->height(); });

            res.positions[indexes[0]].setX(0);
            res.positions[indexes[0]].setY(0);
            auto cur_it = begin;
            std::advance(cur_it, indexes[0]);
            floors.emplace_back(cur_it->width(), cur_it->height(), 0);
            int cur_height = cur_it->height();
            int cur_width = cur_it->width();
            std::cout << "R: " << res.positions[indexes[0]].x() << 'x' << res.positions[indexes[0]].y() << ' ' << cur_it->width() << 'x' << cur_it->height() << "; ";
            for(size_t i = 1; i < count; ++i) {
                cur_it = begin;
                std::advance(cur_it, indexes[i]);
                int size_limit =  cur_it->height() + cur_height;
                bool next_floor = true;
                for(auto& fl_item : floors) {
                    if(fl_item.height >= cur_it->height() &&
                        fl_item.width  + cur_it->width() < size_limit) {
                        res.positions[indexes[i]].setX(fl_item.width + 6);
                        res.positions[indexes[i]].setY(fl_item.position);
                        fl_item.width += 6 + cur_it->width();
                        if(cur_width < fl_item.width)
                            cur_width = fl_item.width;
                        next_floor = false;
                        break;
                    }
                }
                if(next_floor) {
                    floors.emplace_back(
                        cur_it->width(), cur_it->height(), cur_height + 6);
                    res.positions[indexes[i]].setX(0);
                    res.positions[indexes[i]].setY(cur_height + 6);
                    cur_height = cur_height + 6 + cur_it->height();
                    if(cur_width < cur_it->width())
                        cur_width = cur_it->width();
                }
                std::cout << "R: " << res.positions[indexes[i]].x() << 'x' << res.positions[indexes[i]].y() << ' ' << cur_it->width() << 'x' << cur_it->height() << "; ";
            }
                std::cout << "Size: " << cur_width << 'x' << cur_height << std::endl;
            res.size.setHeight(cur_height);
            res.size.setWidth(cur_width);
        }
        return res;
    }

    QSize TreeDialog::calc_sizes_(
            const dna_manipulation::DNATreeWalker& wk,
            StrList& sizes) noexcept
    {
        QSize ret(0, 0);
        StringDescriptor ds;
        if(!wk.root_node()) {
            if(!wk->mutations.empty()) {
                ds = calc_mutations_block_size_(wk->mutations);
                ret = ds.size;
            }
        }
        sizes.push_back(std::move(ds));
        auto it = sizes.end();
        --it;
        dna_manipulation::DNATreeWalker walker(wk);
        walker.save();
        size_t count_root_nodes = walker.size();
        std::vector<QSize>  all_sizes;
        all_sizes.resize(count_root_nodes);
        for(size_t i = 0; i < count_root_nodes; ++i) {
            walker.go(i, true);
            auto& child_size = all_sizes[i];
            child_size = calc_sizes_(walker, sizes);
        }
        it->descr = calc_block_params_(all_sizes.begin(), all_sizes.size());
        if(ret.width() < it->descr.size.width())
            ret.setWidth(it->descr.size.width());
        ret.setHeight(ret.height() + it->descr.size.height() + 6);
        return ret;
    }


    QRectF TreeDialog::draw_node_(
        int x,
        int y,
        StrList& descrs,
        const QRectF& parent_rect,
        QPainter& pnt,
        const dna_manipulation::DNATreeWalker& wk) noexcept
    {
        QRectF pres = parent_rect;
        QRectF res(x, y, 0, 0);
        qreal real_width = 0, real_height = 0;
        auto& descr = descrs.front();
        std::cout << __func__ << ": " << x << ',' << y << ' ' << descr.key.toStdString() << std::endl;
        if(!wk.root_node()) {
            if(!wk->mutations.empty()) {
                draw_mutations_block_(
                    x,
                    y,
                    pnt,
                    descr);
                //pres
                /*
                if(parent_rect.x() > pres.x() + pres.width()) {
                    pnt.drawLine(
                        parent_rect.x(),
                        parent_rect.y() + parent_rect.height() / 2,
                        pres.x() + pres.width() / 2,
                        y);
                }
                else if(parent_rect.x() + parent_rect.width() < pres.x()) {
                    pnt.drawLine(
                        parent_rect.x() + parent_rect.width(),
                        parent_rect.y() + parent_rect.height() / 2,
                        pres.x() + pres.width() / 2,
                        y);
                }
                else {
                    pnt.drawLine(
                        parent_rect.x() + parent_rect.width() / 2,
                        parent_rect.y() + parent_rect.height(),
                        pres.x() + pres.width() / 2,
                        y);
                }*/
                y += descr.size.height() + 6;
                real_width = descr.size.width();
                real_height = descr.size.height();
            }
            /*
            if(!keys.empty()) {
            }
            else {
            }*/
        }
        std::vector<QPoint> positions;
        positions.swap(descr.descr.positions);
        descrs.pop_front();
        dna_manipulation::DNATreeWalker walker(wk);
        walker.save();
        size_t count_root_nodes = walker.size();
        for(size_t i = 0; i < count_root_nodes; ++i) {
            walker.go(i, true);
            std::cout << i  << " iter: " << positions[i].x() << ',' << positions[i].y() << std::endl;
            QRectF rect = draw_node_(
                x + positions[i].x(),
                y + positions[i].y(),
                descrs, pres, pnt, walker);
            //pnt.setBrush(Qt::NoBrush);
            //pnt.setBackgroundMode(Qt::TransparentMode);
            //pnt.drawRect(rect);
            res.setWidth(std::max(res.width(), rect.width()));
            res.setHeight(std::max(rect.height(), res.height()));
            //x += rect.width() + 6;
        }
        res.setWidth(std::max(real_width, res.width()));
        res.setHeight(real_height + res.height());
        return res;
    }

    void TreeDialog::draw_tree() noexcept
    {
        TreePtr tree = tree_;
        if(!tree)
            return;
        QPicture pic;
        QPainter pnt;
        pnt.begin(&pic);
        dna_manipulation::DNATree::Mutations base_mutations(
            dna_manipulation::SupportedMarkers::MAX);
        unsigned char index = 0;
        const dna_manipulation::MarkersRecord::MarkersArray& base = tree->base();
        std::transform(
            base.begin(),
            base.end(),
            base_mutations.begin(),
            [&index](unsigned char value){ dna_manipulation::DNATree::MarkerValue ret; ret.marker = index++; ret.value = value; return ret; });
        StringDescriptor main_descr =
            calc_mutations_block_size_(base_mutations);
        dna_manipulation::DNATreeWalker walker(*tree);
        StrList sizes;
        auto child_size = calc_sizes_(walker, sizes);
        int pos_x = (child_size.width() - main_descr.size.width()) / 2;
        if(pos_x < 0)
            pos_x = 0;
        draw_mutations_block_(pos_x, 0, pnt, main_descr);

        QSize img_size(
            std::max(child_size.width(), main_descr.size.width()),
            child_size.height() + main_descr.size.height() + 20);
        QRectF rect(pos_x, 0, main_descr.size.width(), main_descr.size.height());
        draw_node_(
            0, main_descr.size.height() + 20 , sizes, rect,  pnt, walker);
        pnt.end();
        pic.save("pre.bmp");
        std::unique_ptr<QBitmap> pixmap(new QBitmap(img_size));

        if(pixmap.get()) {
            pixmap->fill();
            std::cout << "fill " << pixmap->width() << 'x' << pixmap->height() << std::endl;
            QPainter p(pixmap.get());
            p.drawPicture(0, 0, pic);
            p.end();
            pixmap->save("1.bmp");
            view_label_->setPixmap(*pixmap);
            view_label_->resize(pixmap->size());
            auto sz = size();
            sz.setHeight(height() * 4);
            sz.setWidth(width() * 6);
            resize(sz);
            showMaximized();
        }
    }
}
