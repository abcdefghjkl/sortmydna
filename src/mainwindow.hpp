#ifndef SORT_MY_DNA_MAIN_WINDOW_HPP
#define SORT_MY_DNA_MAIN_WINDOW_HPP

#include<QMainWindow>
#include<memory>
//#include<mutex>
#include<QEvent>
#include"appconfig.hpp"
#include"db_backend.hpp"
#include"importdbdialog.hpp"
#include"enterhaplotypedialog.hpp"
#include"ancestortimedialog.hpp"
#include"haplotype.hpp"
#include"treedialog.hpp"
#include"filters.hpp"
#include"dbeditdialog.hpp"
#include"dbviewmodel.hpp"

class QLabel;
class QFrame;
class QTableWidget;
class QStringList;
class QGroupBox;
class QCheckBox;
class QTableWidget;
class QTableView;
class QVBoxLayout;

namespace sortmydna
{
    class DBBackend;
    class DbViewModel;

    typedef std::shared_ptr<dna_manipulation::SortedDB> MarkersRecordsPtr;
    typedef std::shared_ptr<dna_manipulation::MarkersDB> DBPtr;

    class MainWindow: public QMainWindow
    {
    Q_OBJECT
    public:
        MainWindow(AppConfig& config, DBBackend* dbb);

        AppConfig& config() { return config_; }

        void close_haplotype(int code);

        void close_import_dialog(int code);

        void db_load_task(
            const std::string& group,
            const std::string& subgroup,
            const dna_manipulation::MarkersRecord& haplotype);

        void db_save_task(
            DBPtr db_rec,
            const std::string& group,
            const std::string& subgroup,
            const std::string& load_subgroup,
            const dna_manipulation::MarkersRecord& haplotype,
            bool reload_data);

        void update_data(MarkersRecordsPtr& records);
        void set_haplotype();
        void calc_common_ancester();
        void show_tree();
        void enter_filter();
        void custom_view();
        void turn_filter(bool on);
        void view_database();
        void import_database();
        void export_to_filip();
        void change_sort_order(dna_manipulation::SortedDB::SortIndex index) noexcept;
        void about();

        virtual bool event(QEvent * e);

    protected:
        virtual void closeEvent(QCloseEvent * event);

    private:

        const dna_manipulation::HaploSet::MarkersLine&
        choose_marker_line(int option) noexcept;

        void create_db_table(
            const dna_manipulation::SortedDB* db,
            const dna_manipulation::HaploSet::MarkersLine& markers,
            bool show_distance,
            bool show_info);

        void parse_haplotype_(
            const QString& model_name,
            const QString& haplo_type,
            const QString* custom_model,
            dna_manipulation::HaploSet::MarkersLine& markers_line,
            dna_manipulation::MarkersRecord& markers) const;

        static 
        void parse_markers_string_(
            const QString& markers_str,
            dna_manipulation::HaploSet::MarkersLine& markers_line) noexcept;

        void setup_window_(QFrame* frame);

        void setup_menu_();

        template<class T>
        void setup_menu_action_(
            QMenu* menu,
            const char* name,
            const char* tip,
            T func);

        void update_model_();

        void build_tree_();

        void apply_filter_() noexcept;

        void change_view(int option, bool state);

        // parse markers from filter string for
        // highlighting in a db widget
        void parse_filter_markers_() noexcept;

    private:
        typedef std::set<dna_manipulation::SupportedMarkers> MarkersSet;

        MarkersSet filter_markers_;//set marker of filter
        EnterHaplotypeDialog* add_haplotype_ =  nullptr;
        ImportDbDialog* import_dlg_ =  nullptr;
        std::unique_ptr<AncestorTimeDialog> ancestor_dlg_;
        std::unique_ptr<TreeDialog> tree_dlg_;
        AppConfig& config_;
        DBBackend* dbb_;
        std::unique_ptr<DbViewModel> db_model_;
        QTableWidget* haplo_widget_;
        QTableView* db_widget_;
        QGroupBox* filter_box_;
        QCheckBox* filter_check_;
        QLabel* filter_label_;
        QVBoxLayout* db_layout_;
        dna_manipulation::MarkersRecord my_haplotype_;
        dna_manipulation::HaploSet::MarkersLine my_markers_;
        dna_manipulation::HaploSet::MarkersLine custom_markers_;
        FilterCache filter_cache_; //cache of filters for current db
        //custom markers for view
        std::shared_ptr<dna_manipulation::DNATree> tree_;
        std::mutex tree_dlg_lock_;
    };

    class FinishEvent : public QEvent
    { 
    public:
        FinishEvent(MarkersRecordsPtr&& new_records);
    public:
        MarkersRecordsPtr records;
        static const int event_id;
    };
}

#endif
