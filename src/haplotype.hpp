/**
  file: haplotype.hpp 2015-2019 Andrey Gusev
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include<string>
#include<map>
#include<vector>
#include<unordered_set>
#include<unordered_map>
#include<exception>
#include<algorithm>

namespace dna_manipulation
{
    class LibException: public std::runtime_error
    {
    public:
        LibException(const std::string& str) : 
            std::runtime_error(str) {};
    };

    struct HaploInfo
    {
        unsigned char min_margin;
        unsigned char max_margin;
        float mutation_speed;
    };

    enum SupportedMarkers : unsigned char {
        DYS393 = 0, DYS390, DYS19, DYS391, DYS385A, DYS385B,
        DYS426, DYS388, DYS439, DYS389I,DYS392, DYS389II,
        DYS458, DYS459A, DYS459B, DYS455, DYS454, DYS447,
        DYS437, DYS448, DYS449, DYS464A, DYS464B, DYS464C,
        DYS464D,DYS460, Y_GATA_H4, YCAIIA, YCAIIB, DYS456,
        DYS607, DYS576, DYS570, CDYA, CDYB, DYS442, DYS438,
        DYS531, DYS578, DYF395S1A, DYF395S1B,
        DYS590, DYS537, DYS641, DYS472, DYF406S1, DYS511,
        DYS425, DYS413A, DYS413B, DYS557, DYS594, DYS436,
        DYS490, DYS534, DYS450, DYS444, DYS481, DYS520,
        DYS446, DYS617, DYS568, DYS487, DYS572, DYS640,
        DYS492, DYS565, DYS710, DYS485, DYS632, DYS495,
        DYS540, DYS714, DYS716, DYS717, DYS505, DYS556,
        DYS549, DYS589, DYS522, DYS494, DYS533, DYS636,
        DYS575, DYS638, DYS462, DYS452, DYS445, Y_GATA_A10,
        DYS463, DYS441, Y_GGAAT_1B07, DYS525, DYS712, DYS593,
        DYS650, DYS532, DYS715, DYS504, DYS513, DYS561,
        DYS552, DYS726, DYS635, DYS587, DYS643, DYS497,
        DYS510, DYS434, DYS461, DYS435, MAX
    };

    struct HashSupportedMarkers: public std::hash<unsigned char>
    {
        result_type operator() (const SupportedMarkers& marker) const noexcept 
        { return (static_cast<argument_type>(marker)); }
    };

    class SubString;
    class HaploSet
    {
    public:
        typedef std::vector<SupportedMarkers> MarkersLine;

        static constexpr const std::vector<std::string>& get_panel_names() { return names_; }
        //get defult market line by name, id doesn't exist throw LibException
        static const MarkersLine& get_markers(const std::string& name = longest_group_name_);
        static constexpr const std::vector<std::string>& get_haplogroups() { return groups_; }
        static constexpr const std::unordered_set<std::string>& get_all_markers()
            { return all_markers_;}
        static constexpr const std::unordered_set<std::string>& get_simple_markers()
            { return simple_markers_;}
        static constexpr const std::unordered_map<std::string, int>& get_multi_markers()
            { return multi_markers_;}
        static constexpr const std::unordered_map<std::string, unsigned char>& get_marker_indexes() { return marker_indexes_;}

        static constexpr const std::vector<std::string>& get_supported_marker_names() noexcept { return supported_marker_names_; }

        template<typename T, template <typename, typename...> class CONTAINER, typename... Rest>
        static MarkersLine choose_markers(
                const CONTAINER<T, Rest...>& cont,
                const MarkersLine& line,
                SupportedMarkers start_marker = SupportedMarkers::DYS393);

        struct MarkersLineInfo
        {
            std::string name;
            MarkersLine line;
        };

        template<
            typename T,
            template <typename, typename...> class CONTAINER, typename... Rest>
        static MarkersLineInfo choose_markers_line(
                const CONTAINER<T, Rest...>& cont,
                const std::string* group_name = nullptr);

        static MarkersLine get_possible_markers(char value);

        static bool check_marker(SupportedMarkers marker, unsigned char value)
        { return check_margin_(marker, value); }

        static unsigned long get_generations(
            SupportedMarkers marker, unsigned long count_mutations);

        static float get_mutation_speed(SupportedMarkers marker)
        { return haplo_info_.at(marker).mutation_speed; }

    public:
        static const std::size_t count_multi_markers;

        static const std::size_t count_supported_markers;
    private:

        static bool check_margin_(SupportedMarkers name, unsigned char value);

    private:
        static const std::vector<std::string> groups_;
        static const std::vector<std::string> supported_marker_names_;
        static const std::vector<std::string> names_;
        static const std::map<std::string, MarkersLine> markers_;
        static const std::unordered_set<std::string> simple_markers_;
        static const std::unordered_set<std::string> all_markers_;
        static const std::unordered_map<std::string, int> multi_markers_;
        static const std::unordered_map<std::string, unsigned char> marker_indexes_;
        static const std::unordered_map<SupportedMarkers, HaploInfo, HashSupportedMarkers> haplo_info_;
        static const std::string longest_group_name_;
    };

    template<typename T, template <typename, typename...> class CONTAINER, typename... Rest>
    HaploSet::MarkersLine HaploSet::choose_markers(
            const CONTAINER<T, Rest...>& cont,
            const MarkersLine& markers,
            SupportedMarkers start_marker)
    {
        const size_t input_size = cont.size();
        size_t output_index = 0;
        MarkersLine res(input_size);
        auto mark_it = std::find(markers.begin(), markers.end(), start_marker);
        if(mark_it == markers.end()) {

            std::string msg;
            if(start_marker >= MAX) 
                msg = "start marker out of bounds";
            throw LibException(msg);
        }
        for(auto it = cont.begin(); it != cont.end(); ++it) {
            int marker_value = std::stoi(*it);
            if(marker_value < 0 || marker_value > 46) {
                std::string msg = "Incorrect value of marker '";
                msg += *it;
                msg.push_back('\'');
                throw LibException(msg);
            }
            unsigned char value = static_cast<unsigned char>(marker_value);
            while(mark_it != markers.end() && (value != 0 && !check_margin_(*mark_it, value))) ++mark_it;
            if(mark_it == markers.end()) {
                std::string msg = "Not enough markers in line, last value '";
                msg += *it;
                msg.push_back('\'');
                for(size_t i = 0; i < output_index; ++i)
                    if(res[i] != i) {
                        msg += " possible mismatch position ";
                        msg += std::to_string(i);
                        break;
                    }
                throw LibException(msg);
            }
            else
                res[output_index++] = *mark_it++;
        }
        return res;
    }

    template<typename T, template <typename, typename...> class CONTAINER, typename... Rest>
    HaploSet::MarkersLineInfo HaploSet::choose_markers_line(
            const CONTAINER<T, Rest...>& cont,
            const std::string* group_name)
    {
        const size_t input_size = cont.size();
        auto try_it = markers_.end();
        //select possible group of markers
        if(group_name)
            try_it = markers_.find(*group_name);
        else {
            for(auto it = markers_.begin(); it != markers_.end(); ++it)
                if(it->second.size() >= input_size)
                    if(try_it == markers_.end() || try_it->second.size() > it->second.size())
                        try_it = it;
        }
        if(try_it  == markers_.end() || try_it->second.size() < input_size)
            throw LibException("Can't find suitable marker line");
        MarkersLineInfo result;
        try
        {
            result.line = choose_markers(cont, try_it->second);
            result.name = try_it->first;
        }
        catch(const LibException&)
        {
            if(group_name)
                throw;
            result = choose_markers_line(cont, &longest_group_name_);
        }
        return result;
    }

}
