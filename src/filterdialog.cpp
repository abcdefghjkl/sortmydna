/**
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include"filterdialog.hpp"
#include"haplotype.hpp"
#include<QHBoxLayout>
#include<QVBoxLayout>
#include<QGridLayout>
#include<QListWidget>
#include<QPushButton>
#include<QGroupBox>
#include<QComboBox>
#include<QSpinBox>
#include<QLineEdit>
#include<QPlainTextEdit>
#include<QDialogButtonBox>
#include<QLabel>

namespace sortmydna
{
    FilterDialog::FilterDialog(
        const QString& filter_string,
        const QString& filter_regexp,
        const dna_manipulation::MarkersRecord& my_haplotype,
        const dna_manipulation::HaploSet::MarkersLine& my_markers)
        : my_haplotype_(my_haplotype),
          expression_(filter_string)
    {
        const auto& names = dna_manipulation::HaploSet::get_supported_marker_names();
        size_t count_columns = calculate_count_columns_(my_markers.size());

        QSpinBox* spin_box = new QSpinBox;
        spin_box->setMaximum(50);
        QLineEdit* marker_edit = new QLineEdit;
        QGridLayout* my_grid = new QGridLayout;
        QGridLayout* other_grid = new QGridLayout;
        size_t j = 0;
        size_t my_row = 0, my_col = 0;
        size_t other_row = 0, other_col = 0;
        for(size_t i = 0; i < dna_manipulation::SupportedMarkers::MAX; ++i) {
            QPushButton* button = new QPushButton(QString::fromStdString(names[i]));
            connect(button, &QAbstractButton::clicked, this,
                [this, marker_edit, button, spin_box, i] () {
                marker_edit->setText(button->text());
                if(my_haplotype_.markers[i] > 0)
                    spin_box->setValue(my_haplotype_.markers[i]);});

            if(j < my_markers.size() && i == my_markers[j]) {
                my_grid->addWidget(button, my_row, my_col++);
                if(my_col == count_columns) {
                    ++my_row;
                    my_col = 0;
                }
                ++j;
            }
            else {
                other_grid->addWidget(button, other_row, other_col++);
                if(other_col == count_columns) {
                    ++other_row;
                    other_col = 0;
                }
            }
        }

        QComboBox* sign_box = new QComboBox;
        static const QStringList c_names = {"<", "=", "!=", ">"};
        sign_box->addItems(c_names);
        QPushButton *a_button = new QPushButton(tr("Add"));
        QHBoxLayout* nexpr_layout = new QHBoxLayout;
        nexpr_layout->addWidget(marker_edit);
        nexpr_layout->addWidget(sign_box);
        nexpr_layout->addWidget(spin_box);
        nexpr_layout->addWidget(a_button);

        QListWidget* expr_list = new QListWidget;
        expr_list->addItems(parse_expression(filter_string));
        expr_list->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred));
        //
        QPushButton *r_button = new QPushButton(tr("Remove"));

        QHBoxLayout *expr_layout = new QHBoxLayout;
        expr_layout->addWidget(expr_list);
        expr_layout->addWidget(r_button);

        QHBoxLayout *info_layout = new QHBoxLayout;
        QLabel* info_label = new QLabel(tr("Info contains:"));
        regexp_edit_ = new QLineEdit;
        regexp_edit_->setText(filter_regexp);

        info_layout->addWidget(info_label);
        info_layout->addWidget(regexp_edit_);

        QGroupBox* expr_box = new QGroupBox();
        expr_box->setTitle("Filter expressions");
        QVBoxLayout* group_layout = new QVBoxLayout;
        group_layout->addLayout(nexpr_layout);
        group_layout->addLayout(expr_layout);
        group_layout->addLayout(info_layout);
        expr_box->setLayout(group_layout);

        QPlainTextEdit* error_view = new QPlainTextEdit;
        error_view->setVisible(false);
        error_view->setReadOnly(true);

        connect(a_button, &QAbstractButton::clicked, this,
            [=] () {add_expresion_(marker_edit, sign_box, spin_box, expr_list, error_view); }); 
        connect(r_button, &QAbstractButton::clicked, this,
            [error_view, this, expr_list] () {
            error_view->setVisible(false);
            remove_expresion_(expr_list); }); 

        connect(marker_edit, &QLineEdit::textChanged, this, [error_view] () {error_view->setVisible(false); });

        QGroupBox* other_box = new QGroupBox();
        other_box->setTitle(tr("Other markers"));
        other_box->setLayout(other_grid);
        QGroupBox* my_box = new QGroupBox();
        my_box->setTitle(tr("My markers"));
        my_box->setLayout(my_grid);

        QDialogButtonBox* buttons = new QDialogButtonBox(
            QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
        connect(buttons, &QDialogButtonBox::accepted, this, [this, expr_list] () { accept_list(expr_list);});
        connect(buttons, &QDialogButtonBox::rejected, this, &FilterDialog::close);

        QVBoxLayout *main_layout = new QVBoxLayout;
        main_layout->addWidget(other_box);
        main_layout->addWidget(my_box);
        main_layout->addWidget(expr_box);
        main_layout->addWidget(error_view);
        main_layout->addWidget(buttons);
        setLayout(main_layout);
    }

    void FilterDialog::add_expresion_(
        QLineEdit* marker,
        QComboBox* sign,
        QSpinBox* spin,
        QListWidget* list,
        QPlainTextEdit* errors) noexcept
    {
        const auto& markers = dna_manipulation::HaploSet::get_marker_indexes();
        QString expr = marker->text();
        auto it = markers.find(expr.toStdString());
        if(it != markers.end()) {
            expr.append(' ');
            expr.append(sign->currentText());
            expr.append(' ');
            expr.append(spin->text());
            list->addItem(expr);
            errors->setVisible(false);
        }
        else {
            QString error_text("Error name '");
            error_text += expr;
            error_text += "' for marker";
            errors->setPlainText(error_text);
            errors->setVisible(true);
        }

    }

    void FilterDialog::remove_expresion_(QListWidget* list) noexcept
    {
        std::unique_ptr<QListWidgetItem> rem(
            list->takeItem(list->currentRow()));
    }

    size_t FilterDialog::calculate_count_columns_(size_t markers_count)
    {
        size_t max_markers =
            std::max(markers_count, dna_manipulation::SupportedMarkers::MAX - markers_count); 
        size_t count_rows = max_markers / 10;
        if(count_rows * 10 == max_markers)
            return std::min(10UL, count_rows * 2);
        else
            return std::min(10UL, max_markers / (count_rows + 1));
    }

    void FilterDialog::accept_list(QListWidget* list)
    {
       expression_.clear();
       for(int i = 0; i < list->count(); ++i) {
           QListWidgetItem *item = list->item(i);
           expression_.append(item->text());
           expression_.append(";");
       }
       accept();
    }

    QStringList FilterDialog::parse_expression(const QString& str) noexcept
    {
        QStringList res = str.split(";", QString::SplitBehavior::SkipEmptyParts);
        return res;
    }

}

