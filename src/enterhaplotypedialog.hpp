#ifndef ENTER_HAPLOTYPE_DIALOG_HPP
#define ENTER_HAPLOTYPE_DIALOG_HPP

#include<map>
#include<QDialog>
#include<QPlainTextEdit>
#include"appconfig.hpp"

namespace sortmydna
{
    class EnterHaplotypeDialog: public QDialog
    {
    Q_OBJECT
    public:
        EnterHaplotypeDialog(QWidget* parent, const AppConfig& config);
        virtual ~EnterHaplotypeDialog();

        void model_changed(
            const QString& new_model,
            QPlainTextEdit* edit) noexcept;

        void model_text_changed(
            QPlainTextEdit* edit,
            QPlainTextEdit* error_edit,
            bool custom) noexcept;

        void haplo_text_changed(
            QPlainTextEdit* edit,
            QPlainTextEdit* error_edit) noexcept;

        bool get_custom_model(QString& custom_model) const noexcept;

        const QString& get_model_name() const noexcept { return model_name_; }

        const QString& get_haplo_type() const noexcept { return haplo_type_; }

        const QString& get_haplo_group() const noexcept { return haplo_group_; }
        
        const QString& get_haplo_subgroup() const noexcept { return haplo_subgroup_; }

        typedef std::map<int, QString> Errors;

        bool verify_text_(
            const QString& text,
            Errors& errors,
            int pos1,
            int pos2,
            bool haplo_check) noexcept;

    protected:
//        virtual void closeEvent(QCloseEvent *e);

    private:
        QString custom_model_;
        QString model_name_;
        QString haplo_type_;
        QString haplo_group_;
        QString haplo_subgroup_;
        Errors custom_model_errors_;
        Errors haplo_errors_;
        int custom_type_position_ = 0;
        int haplo_type_position_ = 0;
    };
}
#endif

