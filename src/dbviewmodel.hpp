#pragma once

#include<vector>
#include<QString>
#include<QAbstractTableModel>
#include"markers_db.hpp"

namespace sortmydna
{
    using SharedDbPtr = dna_manipulation::SharedSortedDBPtr;

    class DbViewModel: public QAbstractTableModel
    {
    public:
        DbViewModel(const dna_manipulation::HaploSet::MarkersLine& markers);

        void set_db(SharedDbPtr ptr) noexcept;

        SharedDbPtr get_db() const noexcept { return db_ptr_;}

        int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

        int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;

        QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

        QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

        void change_sort_order(dna_manipulation::SortedDB::SortIndex index) noexcept;

        void change_markers(dna_manipulation::HaploSet::MarkersLine&& markers) noexcept;

        size_t get_distance_index() const noexcept { return markers_.size();}

        size_t get_info_index() const noexcept { return markers_.size() + 1;}

        static QStringList get_marker_names(const dna_manipulation::HaploSet::MarkersLine& markers);

        static std::vector<QString> generate_all_names();

        static const QString column_name_(const std::string& name);
    private:
        SharedDbPtr db_ptr_;
        dna_manipulation::HaploSet::MarkersLine markers_;
        static const unsigned int c_additional_headers = 2;
        static const std::vector<QString> marker_names_;
    };
}
