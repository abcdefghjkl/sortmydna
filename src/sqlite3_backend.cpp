/**
  file: sqlite3_backend.cpp 2015-2019 Andrey Gusev

  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include"sqlite3_backend.hpp"
#include<sstream>
#include<algorithm>
#include"haplotype.hpp"
#include"exception.hpp"
#include"markers_db.hpp"
#include"haploutils.hpp"

namespace sortmydna
{
    const std::string SQLiteBackend::init_query_ = [] ()
    {
        const auto& names =
            dna_manipulation::HaploSet::get_supported_marker_names();
        std::string res = "CREATE TABLE IF NOT EXISTS "
            "haploinfo(id INTEGER PRIMARY KEY, "
            "kit_id TEXT NOT NULL UNIQUE, "
            "info TEXT, haplogroup TEXT, subclad TEXT, category TEXT, "
            "ethnicity TEXT, snp TEXT);"
            "CREATE TABLE IF NOT EXISTS "
            "haplomodels(id INTEGER PRIMARY KEY, "
            "name TEXT NOT NULL UNIQUE, "
            "count INTEGER, model TEXT);"
            "CREATE TABLE IF NOT EXISTS "
            "markers(id INTEGER";
        for(auto const& elem :names) {
            std::string name = elem;
            std::replace(name.begin(), name.end(), '-', '_');
            res += ", ";
            res += name;
            res += " INTEGER CHECK (";
            res += name;
            res += " BETWEEN 0 AND 50)";
        }
        res += ", FOREIGN KEY(id) REFERENCES haploinfo(id))";
        return res;
    } ();

    template<class STREAM, class STRING>
    STREAM& add_string(STREAM& stream, const STRING& str, bool& first)
    {
        if(first)
            first = false;
        else
            stream << ',';
        stream << '"' << str << '"';
        return stream;
    }

    void SQLiteBackend::check_error_(
        int code,
        const char* func,
        const char* message,
        const std::string& query,
        char* err)
    {
        if(code != SQLITE_OK)
        {
            std::ostringstream ostr;
            ostr << __FILE__ << ":" << __LINE__ << ", " << func << ": can't " << message << ' ' << err
                 << std::endl << "query: " << query;
            sqlite3_free(err);
            throw AppException(ostr.str());
        }
    }

    void SQLiteBackend::init(const std::string& connection)
    {
        sqlite3* handle;
        int rc = sqlite3_open(connection.c_str(), &handle);
        handle_.reset(handle);
        if(rc != SQLITE_OK)
        {
            std::ostringstream ostr;
            ostr << __func__ << ": can't open database '" <<  connection
                << "': " << sqlite3_errmsg(handle);
            throw AppException(ostr.str());
        }
        char* err = nullptr;
        rc = sqlite3_exec(handle_.get(), init_query_.c_str(), NULL, 0, &err);
        check_error_(rc, __func__, "create tables", init_query_, err);
    }

    void SQLiteBackend::fill_default()
    {
        auto panels = dna_manipulation::HaploSet::get_panel_names();
        std::ostringstream read_query;
        read_query << "SELECT name FROM haplomodels WHERE name IN (";
        bool first = true;
        for(auto const& elem :panels)
            add_string(read_query, elem, first);
        read_query << ");";
        char* err = nullptr;
        int rc = sqlite3_exec(
            handle_.get(),
            read_query.str().c_str(),
            [] (void *obj, int argc, char **argv, char **azColName) {
                if(argc != 1)
                    return 1;
                auto& names = *static_cast<decltype(&panels)>(obj);
                auto it = std::find(names.begin(), names.end(), argv[0]);
                if(it != names.end())
                {
                    it->swap(names.back());
                    names.pop_back();
                }
                return 0;
            },
            &panels,
            &err);
        check_error_(rc, __func__, "read models", read_query.str(), err);
        for(const auto& name : panels)
        {
            write_model(name, dna_manipulation::HaploSet::get_markers(name));
        }
    }

    void SQLiteBackend::read_records(
        const std::string& group,
        const std::string& subclad,
        MarkersRecords& recs)
    {
        const auto& names =
            dna_manipulation::HaploSet::get_supported_marker_names();
        std::ostringstream read_query;
        read_query << "SELECT h.id, h.kit_id, h.info, h.haplogroup, h.subclad,"
            " h.category, h.ethnicity, h.snp";
        for(auto const& elem :names) {
            std::string name = elem;
            std::replace(name.begin(), name.end(), '-', '_');
            read_query << ", m." << name;
        }
        read_query << " FROM haploinfo h, markers m WHERE h.id = m.id";
        if(!group.empty()) {
            read_query << " AND haplogroup = \"" << group << '"';
        }
        if(!subclad.empty())
            read_query << " AND subclad like \"" << subclad << "%\"";
        char* err = nullptr;
        int rc = sqlite3_exec(
            handle_.get(),
            read_query.str().c_str(), 
            [] (void *obj, int argc, char **argv, char **azColName) {
                MarkersRecords& recs = *static_cast<MarkersRecords*>(obj);
                if(argc != sizeof(dna_manipulation::MarkersRecord::markers) + 8)
                    return 1;
                if(recs.capacity() == recs.size())
                    recs.reserve(recs.size() + 1024);
                dna_manipulation::MarkersRecord rec;
                rec.db_id = std::stoi(argv[0]);
                rec.id = argv[1];
                rec.info = argv[2];
                rec.haplogroup = argv[3];
                rec.subclad = argv[4];
                if(argv[5])
                    rec.category = argv[5];
                if(argv[6])
                    rec.origin = argv[6];
                if(argv[7])
                    rec.snp = argv[7];
                for(auto i = 8; i < argc; ++i)
                    rec.markers[i - 8] = static_cast<unsigned char>(std::stoi(argv[i]));
                recs.push_back(std::move(rec));
                return 0;
            },
            &recs,
            &err);
        check_error_(rc, __func__, "read records", read_query.str(), err);
    }

    dna_manipulation::SharedDBMarkersLinePtr SQLiteBackend::read_model(
        const std::string* const name)
    {
        std::ostringstream read_query;
        read_query << "SELECT * FROM haplomodels ";
        if(name){
            bool first = true;
            read_query << "WHERE name = ";
            add_string(read_query, *name, first);
        }
        dna_manipulation::SharedDBMarkersLinePtr res;
        char* err = nullptr;
        int rc = sqlite3_exec(
            handle_.get(),
            read_query.str().c_str(),
            [] (void *obj, int argc, char **argv, char **azColName) {
                // haplomodels(id INTEGER, name TEXT, count INTEGER, model TEXT)
                if(argc != 4)
                    return 1;
                try
                {
                    auto& ptr = *static_cast<decltype(&res)>(obj);
                    if(!ptr)
                        ptr = std::make_shared<dna_manipulation::SharedDBMarkersLinePtr::element_type>();
                    ptr->emplace_back(dna_manipulation::DBMarkerLine
                        { std::stoi(argv[0]),
                          static_cast<unsigned int>(std::stoul(argv[2])),
                          argv[1],
                          dna_manipulation::MarkersDBUtils::from_string(argv[3])
                        });
                }
                catch(dna_manipulation::LibException& e)
                {
                    throw AppException(e.what());
                }
                return 0;
            },
            &res,
            &err);
        check_error_(rc, __func__, "read model", read_query.str(), err);
        return res;
    }

    void SQLiteBackend::write_model(
        const std::string& name, const dna_manipulation::HaploSet::MarkersLine& line)
    {
        bool first = false;
        std::ostringstream write_query;
        write_query << "INSERT INTO haplomodels VALUES(NULL";
        add_string(write_query, name, first) << "," << line.size();
        add_string(write_query, dna_manipulation::MarkersDBUtils::to_string(line), first) << ");";
        char* err = nullptr;
        int rc = sqlite3_exec(handle_.get(), write_query.str().c_str(), NULL, 0, &err);
        check_error_(rc, __func__, "write model", write_query.str(), err);
    }

    std::string SQLiteBackend::normalize_(const std::string& str)
    {
        auto first_pos = str.find('\'');
        if(first_pos == std::string::npos)
            return str;
        std::string ret(str, 0, ++first_pos);
        std::size_t pos;
        do {
            ret.push_back('\'');
            pos = str.find('\'', first_pos);
            if(pos != std::string::npos) {
                ret.append(str, first_pos, pos - first_pos + 1);
                first_pos = pos + 1;
            }
            else
                ret.append(str, first_pos, pos);
        } while(pos != std::string::npos);
        return ret;
    }

    void SQLiteBackend::write_record(
            const std::string& group,
            const std::string& subclad,
            const dna_manipulation::MarkersRecord& rec)
    {
        std::ostringstream write_query;
        write_query << "INSERT INTO haploinfo VALUES(NULL, '"
            << normalize_(rec.id) << "', '" << normalize_(rec.info) << "', '";
        if(rec.haplogroup.empty())
            write_query << group;
        else
            write_query << rec.haplogroup;
        write_query << "', '";
        if(rec.subclad.empty())
            write_query << subclad;
        else
            write_query << rec.subclad;
        write_query << "', '" << normalize_(rec.category) << "', '"
            << normalize_(rec.origin) << "', '"
            << normalize_(rec.snp) << "')";
        char* err = nullptr;
        int rc = sqlite3_exec(handle_.get(), write_query.str().c_str(), NULL, 0, &err);
        check_error_(rc, __func__, "insert haploinfo", write_query.str(), err);

        auto id = sqlite3_last_insert_rowid(handle_.get());
        std::ostringstream write_query2;
        write_query2 << "INSERT INTO markers VALUES(" << id << " ";
        for(size_t i = 0; i < sizeof(rec.markers) / sizeof(rec.markers[0]); ++i)
            write_query2 << ", " << std::to_string(rec.markers[i]);
        write_query2 << ')';
        err = nullptr;
        rc = sqlite3_exec(handle_.get(), write_query2.str().c_str(), NULL, 0, &err);
        check_error_(rc, __func__, "insert markers", write_query2.str(), err);
    }
}

