#include"filters.hpp"
#include"markers_db.hpp"
#include<iostream>

namespace sortmydna
{
bool AdvancedFilter::check(
    const dna_manipulation::MarkersRecord& db_record) const noexcept
{
    bool pass = true;
    auto rit = rules_.begin();
    while(rit != rules_.end() && pass) {
        switch(rit->sign)
        {
            case FilterRule::S_LESS:
                pass = (db_record.markers[rit->marker] < rit->value);
                break;
            case FilterRule::S_EQUAL:
                pass = (db_record.markers[rit->marker] == rit->value);
                break;
            case FilterRule::S_NEQUAL:
                pass = (db_record.markers[rit->marker] != rit->value);
                break;
            case FilterRule::S_GREATER:
                pass = (db_record.markers[rit->marker] > rit->value);
                break;
        }
        ++rit;
    }
    if(pass && !expression_.isEmpty()) {
        auto info_line = db_record.info + db_record.snp;
        pass = (expression_.indexIn(std::move(QString::fromStdString(std::move(info_line)))) != -1);
    }
    return pass;
}

const Index& FilterCache::get_filter(
    const QString& filter_string,
    const QString& regexp,
    const dna_manipulation::MarkersDBBase& db)
{
    auto key = build_key_(filter_string, regexp);
    auto it = storage_.find(key);
    if(it != storage_.end())
        return it->second;
    return add_filter_(std::move(key), filter_string, regexp, db);
}

std::string FilterCache::build_key_(
    const QString& filter_string,
    const QString& regexp)
{
    auto key = filter_string + "/" + regexp;
    return key.toStdString();
}

Index& FilterCache::add_filter_(
    std::string&& key,
    const QString& filter_string,
    const QString& regexp,
    const dna_manipulation::MarkersDBBase& db)
{
    QStringList restrictions = filter_string.split(
        ";", QString::SplitBehavior::SkipEmptyParts);
    FilterRules rules;
    const auto& names =
        dna_manipulation::HaploSet::get_marker_indexes();
    rules.reserve(restrictions.size());
    for(auto i = 0; i < restrictions.size(); ++i) {
        QStringList parts = restrictions[i].split(
            " ", QString::SplitBehavior::SkipEmptyParts);
        if(parts.size() != 3) {
            if(!parts.isEmpty())
                std::cerr << __func__ << ": can't parse expression '"
                    << restrictions[i].toStdString() << "' it contains "
                    << parts.size() << " parts";
            continue;
        }
        auto it = names.find(parts[0].toStdString());
        if(it == names.end()) {
            std::cerr << __func__ << ": marker name '"
                << parts[0].toStdString() << "' isn't supported";
            continue;
        }
        FilterRule::Signs sign;
        if(parts[1] == "<")
            sign = FilterRule::S_LESS;
        else if(parts[1] == "=")
            sign = FilterRule::S_EQUAL;
        else if(parts[1] == "!=")
            sign = FilterRule::S_NEQUAL;
        else if(parts[1] == ">")
            sign = FilterRule::S_GREATER;
        else {
            std::cerr << __func__ << ": wrong sign '" 
                << parts[1] .toStdString() << "'";
            continue;
        }
        auto marker = static_cast<dna_manipulation::SupportedMarkers>(it->second);
        rules.push_back({
            marker,
            sign,
            static_cast<unsigned char>(parts[2].toUInt())});
    }
    AdvancedFilter filter(std::move(rules), regexp); 
    Index new_value;
    for(size_t i = 0; i < db.MarkersDBBase::size(); ++i) {
        const auto& rec = db.get_record(i);
        if(filter.check(rec))
            new_value.insert(i);
    }
    return storage_.emplace(std::move(key), std::move(new_value)).first->second;
}

}
