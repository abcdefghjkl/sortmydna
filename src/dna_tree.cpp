/**
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include<iostream>
#include"dna_tree.hpp"

namespace dna_manipulation
{
    DNATree::DNATree(const dna_manipulation::MarkersRecord::MarkersArray& base)
        : base_(base)
    {
    }

    void DNATree::add(const dna_manipulation::MarkersRecord& rec)
    {
        unsigned char indexes[dna_manipulation::SupportedMarkers::MAX];
        size_t count = rec.valuable_indexes(indexes);
        for(size_t i = 0; i < count; ++i)
            ++freq_[indexes[i]];

        unsigned char mutations[dna_manipulation::SupportedMarkers::MAX];
        size_t count_mutations =
            dna_manipulation::MarkersDBUtils::find_mutations(
            indexes,
            count,
            rec,
            base_,
            mutations);
        MarkerNode& node = nodes_[count][rec.id];
        for(size_t i = 0; i < count_mutations; ++i) {
            node.push_back({mutations[i], rec[mutations[i]]});
            ++mutation_rate_[mutations[i]];
        }
    }

    void DNATree::add_(
        const std::string& id,
        MarkerNode& node,
        size_t weights[SupportedMarkers::MAX])
    {
        size_t count = node.size();
        unsigned char markers[SupportedMarkers::MAX];
        std::transform(
            node.begin(),
            node.end(),
            markers,
            [](const MarkerValue& value) { return value.marker;});
        std::sort(markers, markers + count, [&weights]
            (unsigned char left, unsigned char right) {
                return  weights[left] < weights[right];
            });
        size_t prefer_index = root_paths_.size();
        size_t max_raiting = 0;
        for(size_t i = 0; i < root_paths_.size(); ++i) {
            size_t raiting = check_node_(
                root_paths_[i].get(), node);
            if(raiting == count) { //same mutations
                prefer_index = i;
                break;
            }
            if(raiting > max_raiting) {
                max_raiting = raiting;
                prefer_index =  i;
            }
        }
        if(prefer_index == root_paths_.size()) {
            root_paths_.resize(root_paths_.size() + 1);
        }
        add_node_(root_paths_[prefer_index], id, node);
    }

    void DNATree::add_node_(
        TreeNodePtr& node,
        const std::string& id,
        MarkerNode& mnode)
    {
        if(!node) {
            node.reset(new TreeNode);
            node->keys.push_back(id);
            node->mutations.insert(
                node->mutations.end(), mnode.begin(), mnode.end());
        }
        else {
            size_t count = mnode.size();
            std::vector<MarkerValue> same_mutations;
            same_mutations.reserve(count);
            auto it = mnode.begin();
            for(const auto& mut : node->mutations) {
                while(mut.marker > it->marker)
                    if(++it  == mnode.end())
                        goto end_markers;
                if(mut == *it) {
                    same_mutations.push_back(*it);
                    mnode.erase(it++);
                    if(++it  == mnode.end())
                        break;
                }
            }
end_markers:
            if(same_mutations.size() == node->mutations.size()) {
                if(mnode.empty()) // same mutations
                    node->keys.push_back(id);
                else {
                    size_t links_raiting = 0;
                    size_t index;
                    for(size_t i = 0; i < node->links.size(); ++i) {
                        auto& link = node->links[i];
                        size_t raiting = check_node_(link.get(), mnode);
                        if(raiting > links_raiting) {
                            links_raiting = raiting;
                            index = i;
                        }
                    }
                    if(links_raiting != 0)
                        add_node_(node->links[index], id, mnode); 
                    else {
                        TreeNodePtr new_node(new TreeNode);
                        new_node->keys.push_back(id);
                        new_node->mutations.insert(
                            new_node->mutations.end(),
                            mnode.begin(),
                            mnode.end());
                        node->links.push_back(std::move(new_node));
                    }
                }
            }
            else { //if(same_mutations.size() <  node->mutations.size())
                TreeNodePtr new_node(new TreeNode);
                if(mnode.empty())
                    new_node->keys.push_back(id);
                new_node->mutations.swap(same_mutations);
                node.swap(new_node);
                same_mutations.reserve(
                    new_node->mutations.size() - node->mutations.size());
                auto it_old = new_node->mutations.begin();
                for(auto it = node->mutations.begin();
                    it != node->mutations.end(); ++it) {
                    while(!(*it_old == *it)) {
                        same_mutations.push_back(*it_old);
                        ++it_old;
                    }
                    ++it_old;
                }
                while(it_old != new_node->mutations.end()) {
                    same_mutations.push_back(*it_old);
                    ++it_old;
                }
                new_node->mutations.swap(same_mutations);
                node->links.push_back(std::move(new_node));
                if(!mnode.empty()) {
                    new_node.reset(new TreeNode);
                    new_node->keys.push_back(id);
                    new_node->mutations.insert(
                        new_node->mutations.end(),
                        mnode.begin(),
                        mnode.end());
                    node->links.push_back(std::move(new_node));
                }
            }
        }
    }

    size_t DNATree::check_node_(
        const TreeNode* node,
        MarkerNode& mnode)
    {
        size_t res = 0;
        if(!node)
            return res;
        auto it = mnode.begin();
        for(const auto& mut : node->mutations) {
            while(mut.marker > it->marker)
                if(++it  == mnode.end())
                    goto end_markers;
            if(mut == *it) ++res;
        }
end_markers:
        size_t links_raiting = 0;
        if(res) 
            for(auto& link : node->links)
                links_raiting = std::max(
                    links_raiting,
                    check_node_(link.get(), mnode));
        return res + links_raiting;
    }

    void DNATree::TreeNode::print_debug(size_t level) const
    {
        std::string tabs;
        for(size_t i = 0; i < level; ++i)
            tabs += '\t';
        const auto& names = dna_manipulation::HaploSet::get_supported_marker_names();
        std::cout << tabs << "Mutations:";
        for(size_t i = 0; i < mutations.size(); ++i)
            std::cout << ' ' << names[mutations[i].marker] << " = "
                << static_cast<int>(mutations[i].value);
        std::cout << std::endl;
        if(!keys.empty())
        {
            std::cout << tabs << "Keys:";
            for(const auto& key : keys)
                std::cout << ' '<< key;
            std::cout << std::endl;
        }
        for(size_t i = 0; i < links.size(); ++i)
            links[i]->print_debug(level + 1);
    }

    size_t DNATree::TreeNode::size(bool subtree)
    {
        size_t count = keys.size();
        if(subtree) {
            for(size_t i = 0; i < links.size(); ++i) {
                count += links[i]->size(subtree);
            }
        }
        return count;
    }

    void DNATree::print_debug()
    {
        std::cout << "Base:";
        for(size_t i = 0; i < SupportedMarkers::MAX; ++i)
            std::cout << ' ' << static_cast<int>(base_[i]);
        std::cout << std::endl;
        for(size_t i = 0; i < root_paths_.size(); ++i)
            root_paths_[i]->print_debug();
    }

    void DNATree::build(size_t count_markers)
    {
        size_t weights[SupportedMarkers::MAX] = {};
        for(size_t i = 0; i < SupportedMarkers::MAX; ++i)
            if(freq_[i])
                weights[i] = mutation_rate_[i] * 100 / freq_[i];
        for(auto it = nodes_.rbegin(); it != nodes_.rend(); ++it) {
            if(it->first == count_markers || !count_markers) { 
                auto& nodes = it->second;
                for(auto& node : nodes) {
                    auto& nodelist = node.second;
                    add_(node.first, nodelist, weights);
                }
            }
        }
        nodes_.clear();
    }

    bool DNATreeWalker::go(size_t index, bool restore_state)
    {
        if(restore_state)
            restore();
        if(node_) {
            if(index < node_->links.size()) {
                node_ = node_->links[index].get();
                return true;
            }
            return false;
        }
        else {
            if(index < dna_tree_.root_paths_.size()) {
                node_ = dna_tree_.root_paths_[index].get();
                return true;
            }
        }
        return false;
    }

    size_t DNATreeWalker::size()
    {
        size_t count = 0;
        if(node_) 
            count = node_->links.size();
        else {
            count = dna_tree_.root_paths_.size();
        }
        return count;
    }


}
