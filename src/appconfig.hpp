#ifndef SORT_MY_DNA_APP_CONFIG_HPP
#define SORT_MY_DNA_APP_CONFIG_HPP

#include<QString>

namespace sortmydna
{
    struct ViewConfig
    {
        enum ShowValues{
            SW_ALL = 0,
            SW_MY,
            SW_CUSTOM
        };
        int show_option;
        QString custom_markers;//show markers
        bool show_info_column;
        bool show_distance_column;
    };

    struct AppConfig
    {
        QString db_path;
        QString model_name;//model name
        QString custom_model;//marker of custom model
        QString haplo_group;
        QString haplo_subgroup;
        QString haplotype;
        QString import_group;
        QString import_subgroup;
        QString import_text_postfix;
        QString filter_string;//markers filter string
        QString filter_regexp;//info filter regexp
        bool enable_filter;
        ViewConfig view_config;
        unsigned int sort_order;
        void def_config();
        bool read_config();
        void write_config();
        QString def_db_path();
    };
}

#endif //SORT_MY_DNA_APP_CONFIG_HPP
