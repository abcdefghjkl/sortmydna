#ifndef SORT_MY_DNA_APP_HPP
#define SORT_MY_DNA_APP_HPP

#include<memory>
#include<exception>
#include<QApplication>
#include"mainwindow.hpp"

namespace sortmydna
{
    class App: public QApplication
    {
    Q_OBJECT
    public:
        App(int &argc, char* argv[]);
        virtual ~App() {};

        void init();
    protected:
        virtual bool notify(QObject *receiver, QEvent *e);
        
    private:
        std::unique_ptr<MainWindow> main_win_;
        std::unique_ptr<DBBackend> db_connection_;
        AppConfig config_;
    };
}
#endif

