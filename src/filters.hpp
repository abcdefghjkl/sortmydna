#pragma once
#include"haplotype.hpp"
#include<unordered_set>
#include<unordered_map>
#include<QString>
#include<QHash>
#include<QRegExp>

namespace dna_manipulation
{
    struct MarkersRecord;
    class MarkersDBBase;
}

namespace sortmydna
{
    struct FilterRule
    {
        enum Signs
        {
            S_LESS,
            S_EQUAL,
            S_NEQUAL,
            S_GREATER
        };

        dna_manipulation::SupportedMarkers marker;
        Signs sign;
        unsigned char value;
    };
    using FilterRules = std::vector<FilterRule>;

    struct AdvancedFilter
    {
        AdvancedFilter(const FilterRules& rules, const QString& expr)
            : rules_(rules)
            , expression_(expr) {};

        AdvancedFilter(FilterRules&& rules, QString&& expr)
            : rules_(std::move(rules))
            , expression_(std::move(expr)) {};

        ~AdvancedFilter() noexcept = default;
        AdvancedFilter& operator=(const AdvancedFilter&) = default;
        AdvancedFilter& operator=(AdvancedFilter&&) = default;

        bool check(const dna_manipulation::MarkersRecord& db_record) const noexcept;

        static
        inline
        size_t hash_func(const AdvancedFilter& key) noexcept
        {
            size_t val = 0;
            for(const auto& rule : key.rules_) {
                val += static_cast<size_t>(
                    (rule.sign << 16) + (rule.marker << 8) + rule.value) << 32;
            }
            return qHash(key.expression_) ^ val;
        }
    private:
        FilterRules rules_;
        QRegExp expression_;
    };

    using Index = std::unordered_set<size_t>;
    using AdvancedFilterHashFuncType = decltype(&AdvancedFilter::hash_func);

    class FilterCache
    {
    public:
        /* add a new filter to cache if it doesn't
         * exist add return it
         */
        const Index&
        get_filter(
            const QString& filter_string,
            const QString& regexp,
            const dna_manipulation::MarkersDBBase& db);

        //clear all items from cache
        void clear() noexcept { storage_.clear(); } 

    private:
        //create key from string parameters
        static
        std::string build_key_(const QString& filter_string, const QString& regexp);

        //add filter to cache
        Index& add_filter_(
            std::string&& key,
            const QString& filter_string,
            const QString& regexp,
            const dna_manipulation::MarkersDBBase& db);
    private:
        std::unordered_map<std::string, Index> storage_;

    };
}
