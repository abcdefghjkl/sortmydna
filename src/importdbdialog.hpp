#ifndef SORT_MY_DNA_IMPORT_DB_DIALOG_HPP
#define SORT_MY_DNA_IMPORT_DB_DIALOG_HPP

#include<QDialog>
#include<list>
#include<vector>
#include<map>
#include<memory>
#include<atomic>
#include"haploutils.hpp"
#include"markers_db.hpp"
#include"appconfig.hpp"
#include"modelmanipulations.hpp"

class QLabel;
class QComboBox;
class QHBoxLayout;
class QTableWidget;
class QPlainTextEdit;
class QHBoxLayout;
class QStatusBar;

namespace sortmydna
{
    class ImportDbDialog: public QDialog
    {
    Q_OBJECT
    public:
        enum ParseResult
        {
            PR_SUCCESS = 0,
            PR_FAIL,
            PR_CANCEL
        };

        typedef std::unique_ptr<dna_manipulation::MarkersDB> MarkersDBPtr;

        ImportDbDialog(QWidget* parent, AppConfig& config);
        virtual ~ImportDbDialog() {};

        void select_file_name(QLabel* label) noexcept;
        void import_file() noexcept;

        ParseResult parse_line(const std::string& line);

        MarkersDBPtr& get_exported_data() noexcept { return db_;}

        struct DBRecord
        {
            std::string id;
            std::string info;
            std::vector<unsigned char> markers;
        };

    protected:

        QHBoxLayout* setup_delim_layout_();

        struct Statistic
        {
            std::atomic_ulong count_errors = {0};
            std::atomic_ulong count_imported = {0};
        };

        void add_data_(const Model& base_model, Model& model);

        void create_text_postfix_(const Model& model);

        void update_status_bar_() noexcept;

    private:
        QStatusBar* status_bar_;
        QPlainTextEdit* error_edit_;
        QString file_name_;
        std::vector<ModelPtr> models_;
        AppConfig& config_;
        MarkersDBPtr db_;
        QString delim_ = " \t";
        bool get_group_from_file_ = false;
        Statistic stat_;
    };

    class ModelDialog: public QDialog
    {
    Q_OBJECT
    public:

        enum DialogResult
        {
            DR_CANCEL = 0,
            DR_SKIP,
            DR_SKIP_ALL,
            DR_ACCEPT,
            DR_ACCEPT_ALL
        };

        typedef std::vector<const Model*> BaseModelContainer;

        ModelDialog(
            Model& info,
            const BaseModelContainer& models,
            bool& postix) noexcept;

        virtual ~ModelDialog() {};

        void marker_changed(QTableWidget* markers, QComboBox* combo, int index);

        void model_changed(QTableWidget* widget, int index);

        void marker_button_clicked(QTableWidget* widget, int index, int row_index);  

        void change_info_state(
            QTableWidget* info_table,
            int state,
            int model_index,
            int row_index) noexcept;

        void change_info_type(
            QTableWidget* info_table,
            int model_index,
            int row_index,
            int col_index) noexcept;

        void close_dialog(DialogResult status) noexcept;

        DialogResult result() const noexcept { return result_;}

        int get_model_index() const noexcept { return current_model_; }

        ModelPtr& get_current_model() noexcept { return model_; }

    protected:
        virtual void showEvent(QShowEvent * event);

    private:

        QTableWidget* setup_info_table_() noexcept;

        QHBoxLayout* setup_models_combo_(QTableWidget* markers_table) noexcept;

        void add_info_row_(
            QTableWidget* info_table,
            int model_index,
            int row_index) noexcept;

        void add_marker_row_(
            QTableWidget* markers_table,
            int marker_index,
            int row_index,
            int& changed_index) noexcept;

        void setup_current_model_();

        void update_widget_(QTableWidget* widget, const Model& model);

    private:
        ModelPtr model_;
        Model& info_;
        bool &add_as_posfix_;
        DialogResult result_ = DR_SKIP;
        const BaseModelContainer& models_;
        int current_model_ = -1;
    };

    class EditModelDialog: public QDialog
    {
    Q_OBJECT
    public:
        EditModelDialog(Model& info, const QString& name, int index);
        unsigned char selected_marker() const noexcept { return marker_;}

    private:
        unsigned char marker_ = 0;
    };
}
#endif
