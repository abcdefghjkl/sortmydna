#include<string>
#include<iostream>
#include<vector>
#include<map>
#include<list>
#include<set>
#include"sqlite3_backend.hpp"
#include"exception.hpp"
#include"haplotype.hpp"
#include"dna_tree.hpp"

int test_branches(
    const std::string& db_path,
    const std::string& group,
    const std::string& subgroup)
{
    try
    {
        const auto& names = dna_manipulation::HaploSet::get_supported_marker_names();
        dna_manipulation::MarkersRecords recs;
        sortmydna::SQLiteBackend backend(db_path);
        backend.read_records(group, subgroup, recs);
        /*
        std::map<size_t, size_t> counts;
        for(const auto& rec : recs) {
            ++counts[rec.valuable_indexes(nullptr)];
            std::cout << rec.id << ' ' << rec.info << ' ' << std::endl;
        }
        for(const auto& val : counts) {
            std::cout << val.first << " ->  " << val.second << std::endl;
        }*/
        dna_manipulation::BaseHaploCalculator calc;
        dna_manipulation::MarkersDBUtils::calc_for_set(recs, calc);
        for(size_t j = 0; j < dna_manipulation::SupportedMarkers::MAX; ++j) {
            std::cout << names[j] << " = " << static_cast<unsigned>(calc.base()[j]) << ' ';
        }
        std::cout << std::endl;
        dna_manipulation::DNATree tree(calc.base());
        for(size_t rec_index = 0; rec_index < recs.size(); ++rec_index) {
            const auto& rec = recs[rec_index];
            tree.add(rec);
        }
        tree.build();
        tree.print_debug();
        return 0;
    }
    catch(const sortmydna::AppException& e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
}

int main(int argc, char* argv[])
{
    std::string db_path;
    std::string group("I");
    std::string subgroup("1");
    if(argc > 1)
        group = argv[1];
    if(argc > 2)
        group = argv[2];
    if(argc > 3) {
        db_path = argv[3];
    }
    else {
        char* home = getenv("HOME");
        if(home) {
            db_path = home;
            db_path += "/.config/sortmydna/db";
        }
        else {
            std::cerr << "can't get environment variable HOME" << std::endl;
            return 1;
        }
    }
    return test_branches(db_path, group, subgroup);
}
