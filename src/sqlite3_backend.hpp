/**
  file: sqlite3_backend.hpp 2015-2019 Andrey Gusev
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#pragma once

#include<sqlite3.h>
#include<string>
#include<memory>
#include"db_backend.hpp"

namespace sortmydna
{

    class SQLiteBackend : public DBBackend
    {
    public:
        inline
        SQLiteBackend() {} ;

        SQLiteBackend(const std::string& connection){ init(connection);}

        inline
        virtual ~SQLiteBackend() {};

        void init(const std::string& connection) override;

        void fill_default() override;

        void read_records(
            const std::string& group,
            const std::string& subgroup,
            MarkersRecords& rec) override;

        //load haplo models
        virtual dna_manipulation::SharedDBMarkersLinePtr
        read_model(const std::string* const name = nullptr) override;

        //save haplo model
        void write_model(
            const std::string& name,
            const dna_manipulation::HaploSet::MarkersLine& line) override;

        void write_record(
            const std::string& group,
            const std::string& subgroup,
            const dna_manipulation::MarkersRecord& rec) override;

    private:

        std::string normalize_(const std::string& str);

        void check_error_(int code, const char* func, const char* message, const std::string& query, char* err);

    private:
        typedef decltype(&sqlite3_close) DestroyerType;

        static const std::string init_query_;//query for creating unexisting tables in db

        std::unique_ptr<sqlite3, DestroyerType> handle_ {nullptr, sqlite3_close};
    };
}
