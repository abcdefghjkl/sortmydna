#ifndef SORT_MY_DNA_TREE_DIALOG_HPP
#define SORT_MY_DNA_TREE_DIALOG_HPP

#include<vector>
#include<memory>
#include<QDialog>
#include<QPixmap>
#include"dna_tree.hpp"
#include<mutex>

class QLabel;
class QPixmap;
class QPoint;
class QPainter;
class QSize;

namespace dna_manipulation
{
    class SortedDB;
    class MarkersRecord;
}

namespace sortmydna
{
    class TreeDialog: public QDialog
    {
    Q_OBJECT
    public:
        typedef std::shared_ptr<dna_manipulation::DNATree> TreePtr;
        typedef std::unique_ptr<QPixmap> PixmapPtr;
        typedef std::vector<PixmapPtr> PixmapCollection;

        TreeDialog();

        void draw_tree() noexcept;

        void set_tree(TreePtr& tree);

        QPixmap* draw_pixmap(
            const dna_manipulation::DNATreeWalker& wk) noexcept;

        struct BlockDescriptor
        {
            QSize size;
            std::vector<QPoint> positions;
        };
        
        struct StringDescriptor
        {
            QString key;
            QSize size;
            BlockDescriptor descr;
        };
        typedef std::vector<StringDescriptor> StrVector;
        typedef std::list<StringDescriptor> StrList;
    private:
        void draw_block_(
            QPixmap* pixmap,
            const StrVector& strs,
            int x_pos,
            int y_pox);

        void draw_mutations_block_(
            int x,
            int y,
            QPainter& pnt,
            const StringDescriptor& descr) noexcept;

        QRectF draw_node_(
            int x,
            int y,
            StrList& descrs,
            const QRectF& parent_rect,
            QPainter& pnt,
            const dna_manipulation::DNATreeWalker& wk) noexcept;

        QSize calc_sizes_(
            const dna_manipulation::DNATreeWalker& wk,
            StrList& sizes) noexcept;

        StringDescriptor calc_mutations_block_size_(
            const dna_manipulation::DNATree::Mutations& mutations) noexcept;

        static
        QString get_marker_str_(unsigned char marker, unsigned char value);

        void fill_size_(StringDescriptor& descr);

        template<class ITER>
        BlockDescriptor calc_block_params_(
            ITER begin, size_t count) noexcept;
    private:
        //std::mutex tree_lock_;
        QLabel* view_label_;
        TreePtr tree_;
        std::unique_ptr<QFontMetrics> metrics_;
    };
}

#endif

