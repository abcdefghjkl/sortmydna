#ifndef SORT_MY_DNA_FILTER_DIALOG_HPP
#define SORT_MY_DNA_FILTER_DIALOG_HPP

#include<QDialog>
#include<QStringList>
#include"haplotype.hpp"
#include"markers_db.hpp"

class QPlainTextEdit;
class QComboBox;
class QSpinBox;
class QListWidget;

#include<QLineEdit>

namespace sortmydna
{
    class FilterDialog: public QDialog
    {
    Q_OBJECT
    public:

        FilterDialog(
            const QString& filter_string,
            const QString& filter_regexp,
            const dna_manipulation::MarkersRecord& my_haplotype,
            const dna_manipulation::HaploSet::MarkersLine& my_markers);

        virtual ~FilterDialog() noexcept {};

        void accept_list(QListWidget* list);

        const QString& get_filter() const noexcept { return expression_;}

        QString regexp() const noexcept { return regexp_edit_->text();}
 
        static QStringList parse_expression(const QString& str) noexcept;
    protected:
        static size_t calculate_count_columns_(size_t markers_count);

    private:

        void add_expresion_(
            QLineEdit* marker,
            QComboBox* sign,
            QSpinBox* spin,
            QListWidget* list,
            QPlainTextEdit* errors) noexcept;

        void remove_expresion_(QListWidget* list) noexcept;

    private:
        const dna_manipulation::MarkersRecord& my_haplotype_;
        QString expression_;//expression of filter for markers
        QLineEdit* regexp_edit_;//edit for regexp epression
    };

}

#endif
