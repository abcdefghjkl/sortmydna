/**
  file: haplotype.cpp 2015-2019 Andrey Gusev
  This file is part of sortmydna.

  sortmydna is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  sortmydna is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with sortmydna.  If not, see <http://www.gnu.org/licenses/>.
**/

#include"haplotype.hpp"
#include<sstream>

namespace dna_manipulation
{
    const std::string HaploSet::longest_group_name_ = "Y-DNA111";

    const std::vector<std::string> HaploSet::groups_ = {"A", "A0", "A00", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S"};

    const std::vector<std::string> HaploSet::supported_marker_names_ = {
        "DYS393", "DYS390", "DYS19", "DYS391", "DYS385A", "DYS385B", "DYS426",
        "DYS388", "DYS439", "DYS389I","DYS392", "DYS389II", "DYS458", "DYS459A",
        "DYS459B", "DYS455", "DYS454", "DYS447", "DYS437", "DYS448", "DYS449",
        "DYS464A", "DYS464B", "DYS464C", "DYS464D","DYS460", "Y-GATA-H4", "YCAIIA",
        "YCAIIB", "DYS456", "DYS607", "DYS576", "DYS570", "CDYA", "CDYB", "DYS442",
        "DYS438", "DYS531", "DYS578", "DYF395S1A", "DYF395S1B", "DYS590", "DYS537",
        "DYS641", "DYS472", "DYF406S1", "DYS511", "DYS425", "DYS413A", "DYS413B",
        "DYS557", "DYS594", "DYS436", "DYS490", "DYS534", "DYS450", "DYS444", "DYS481",
        "DYS520", "DYS446", "DYS617", "DYS568", "DYS487", "DYS572", "DYS640", "DYS492",
        "DYS565", "DYS710", "DYS485", "DYS632", "DYS495", "DYS540", "DYS714", "DYS716",
        "DYS717", "DYS505", "DYS556", "DYS549", "DYS589", "DYS522", "DYS494", "DYS533",
        "DYS636", "DYS575", "DYS638", "DYS462", "DYS452", "DYS445", "Y-GATA-A10",
        "DYS463", "DYS441", "Y-GGAAT-1B07", "DYS525", "DYS712", "DYS593", "DYS650",
        "DYS532", "DYS715", "DYS504", "DYS513", "DYS561", "DYS552", "DYS726", "DYS635",
        "DYS587", "DYS643", "DYS497", "DYS510", "DYS434", "DYS461", "DYS435"
    };

    const std::map<std::string, HaploSet::MarkersLine> HaploSet::markers_ = {
        {"Y-DNA12", {SupportedMarkers::DYS393, SupportedMarkers::DYS390,
            SupportedMarkers::DYS19, SupportedMarkers::DYS391,
            SupportedMarkers::DYS385A, SupportedMarkers::DYS385B,
            SupportedMarkers::DYS426, SupportedMarkers::DYS388,
            SupportedMarkers::DYS439, SupportedMarkers::DYS389I,
            SupportedMarkers::DYS392, SupportedMarkers::DYS389II}},
        {"Y-MOSCOW18", {SupportedMarkers::DYS393, SupportedMarkers::DYS390,
           SupportedMarkers::DYS19, SupportedMarkers::DYS391,
           SupportedMarkers::DYS385A, SupportedMarkers::DYS385B,
           SupportedMarkers::DYS439, SupportedMarkers::DYS389I,
           SupportedMarkers::DYS392, SupportedMarkers::DYS389II,
           SupportedMarkers::DYS447, SupportedMarkers::DYS437,
           SupportedMarkers::DYS448, SupportedMarkers::DYS449,
           SupportedMarkers::DYS456, SupportedMarkers::DYS576,
           SupportedMarkers::DYS438, SupportedMarkers::DYS635 }},
        {"Y-DNA37", { SupportedMarkers::DYS393, SupportedMarkers::DYS390,
           SupportedMarkers::DYS19, SupportedMarkers::DYS391,
           SupportedMarkers::DYS385A, SupportedMarkers::DYS385B,
           SupportedMarkers::DYS426, SupportedMarkers::DYS388,
           SupportedMarkers::DYS439, SupportedMarkers::DYS389I,
           SupportedMarkers::DYS392, SupportedMarkers::DYS389II,
           SupportedMarkers::DYS458, SupportedMarkers::DYS459A,
           SupportedMarkers::DYS459B, SupportedMarkers::DYS455,
           SupportedMarkers::DYS454, SupportedMarkers::DYS447,
           SupportedMarkers::DYS437, SupportedMarkers::DYS448,
           SupportedMarkers::DYS449, SupportedMarkers::DYS464A,
           SupportedMarkers::DYS464B, SupportedMarkers::DYS464C,
           SupportedMarkers::DYS464D,SupportedMarkers::DYS460,
           SupportedMarkers::Y_GATA_H4, SupportedMarkers::YCAIIA,
           SupportedMarkers::YCAIIB, SupportedMarkers::DYS456,
           SupportedMarkers::DYS607, SupportedMarkers::DYS576,
           SupportedMarkers::DYS570, SupportedMarkers::CDYA,
           SupportedMarkers::CDYB, SupportedMarkers::DYS442,
           SupportedMarkers::DYS438} },
        {"Y-DNA67", { SupportedMarkers::DYS393, SupportedMarkers::DYS390,
           SupportedMarkers::DYS19, SupportedMarkers::DYS391,
           SupportedMarkers::DYS385A, SupportedMarkers::DYS385B,
           SupportedMarkers::DYS426, SupportedMarkers::DYS388,
           SupportedMarkers::DYS439, SupportedMarkers::DYS389I,
           SupportedMarkers::DYS392, SupportedMarkers::DYS389II,
           SupportedMarkers::DYS458, SupportedMarkers::DYS459A,
           SupportedMarkers::DYS459B, SupportedMarkers::DYS455,
           SupportedMarkers::DYS454, SupportedMarkers::DYS447,
           SupportedMarkers::DYS437, SupportedMarkers::DYS448,
           SupportedMarkers::DYS449, SupportedMarkers::DYS464A,
           SupportedMarkers::DYS464B, SupportedMarkers::DYS464C,
           SupportedMarkers::DYS464D,SupportedMarkers::DYS460,
           SupportedMarkers::Y_GATA_H4, SupportedMarkers::YCAIIA,
           SupportedMarkers::YCAIIB, SupportedMarkers::DYS456,
           SupportedMarkers::DYS607, SupportedMarkers::DYS576,
           SupportedMarkers::DYS570, SupportedMarkers::CDYA,
           SupportedMarkers::CDYB, SupportedMarkers::DYS442,
           SupportedMarkers::DYS438, SupportedMarkers::DYS531,
           SupportedMarkers::DYS578, SupportedMarkers::DYF395S1A,
           SupportedMarkers::DYF395S1B, SupportedMarkers::DYS590,
           SupportedMarkers::DYS537, SupportedMarkers::DYS641,
           SupportedMarkers::DYS472, SupportedMarkers::DYF406S1,
           SupportedMarkers::DYS511, SupportedMarkers::DYS425,
           SupportedMarkers::DYS413A, SupportedMarkers::DYS413B,
           SupportedMarkers::DYS557, SupportedMarkers::DYS594,
           SupportedMarkers::DYS436, SupportedMarkers::DYS490,
           SupportedMarkers::DYS534, SupportedMarkers::DYS450,
           SupportedMarkers::DYS444, SupportedMarkers::DYS481,
           SupportedMarkers::DYS520, SupportedMarkers::DYS446,
           SupportedMarkers::DYS617, SupportedMarkers::DYS568,
           SupportedMarkers::DYS487, SupportedMarkers::DYS572,
           SupportedMarkers::DYS640, SupportedMarkers::DYS492,
           SupportedMarkers::DYS565} },
        {longest_group_name_, { SupportedMarkers::DYS393,
           SupportedMarkers::DYS390, SupportedMarkers::DYS19,
           SupportedMarkers::DYS391, SupportedMarkers::DYS385A,
           SupportedMarkers::DYS385B, SupportedMarkers::DYS426,
           SupportedMarkers::DYS388, SupportedMarkers::DYS439,
           SupportedMarkers::DYS389I, SupportedMarkers::DYS392,
           SupportedMarkers::DYS389II, SupportedMarkers::DYS458,
           SupportedMarkers::DYS459A, SupportedMarkers::DYS459B,
           SupportedMarkers::DYS455, SupportedMarkers::DYS454,
           SupportedMarkers::DYS447, SupportedMarkers::DYS437,
           SupportedMarkers::DYS448, SupportedMarkers::DYS449,
           SupportedMarkers::DYS464A, SupportedMarkers::DYS464B,
           SupportedMarkers::DYS464C, SupportedMarkers::DYS464D,
           SupportedMarkers::DYS460, SupportedMarkers::Y_GATA_H4,
           SupportedMarkers::YCAIIA, SupportedMarkers::YCAIIB,
           SupportedMarkers::DYS456, SupportedMarkers::DYS607,
           SupportedMarkers::DYS576, SupportedMarkers::DYS570,
           SupportedMarkers::CDYA, SupportedMarkers::CDYB,
           SupportedMarkers::DYS442, SupportedMarkers::DYS438,
           SupportedMarkers::DYS531, SupportedMarkers::DYS578,
           SupportedMarkers::DYF395S1A, SupportedMarkers::DYF395S1B,
           SupportedMarkers::DYS590, SupportedMarkers::DYS537,
           SupportedMarkers::DYS641, SupportedMarkers::DYS472,
           SupportedMarkers::DYF406S1, SupportedMarkers::DYS511,
           SupportedMarkers::DYS425, SupportedMarkers::DYS413A,
           SupportedMarkers::DYS413B, SupportedMarkers::DYS557,
           SupportedMarkers::DYS594, SupportedMarkers::DYS436,
           SupportedMarkers::DYS490, SupportedMarkers::DYS534,
           SupportedMarkers::DYS450, SupportedMarkers::DYS444,
           SupportedMarkers::DYS481, SupportedMarkers::DYS520,
           SupportedMarkers::DYS446, SupportedMarkers::DYS617,
           SupportedMarkers::DYS568, SupportedMarkers::DYS487,
           SupportedMarkers::DYS572, SupportedMarkers::DYS640,
           SupportedMarkers::DYS492, SupportedMarkers::DYS565,
           SupportedMarkers::DYS710, SupportedMarkers::DYS485,
           SupportedMarkers::DYS632, SupportedMarkers::DYS495,
           SupportedMarkers::DYS540, SupportedMarkers::DYS714,
           SupportedMarkers::DYS716, SupportedMarkers::DYS717,
           SupportedMarkers::DYS505, SupportedMarkers::DYS556,
           SupportedMarkers::DYS549, SupportedMarkers::DYS589,
           SupportedMarkers::DYS522, SupportedMarkers::DYS494,
           SupportedMarkers::DYS533, SupportedMarkers::DYS636,
           SupportedMarkers::DYS575, SupportedMarkers::DYS638,
           SupportedMarkers::DYS462, SupportedMarkers::DYS452,
           SupportedMarkers::DYS445, SupportedMarkers::Y_GATA_A10,
           SupportedMarkers::DYS463, SupportedMarkers::DYS441,
           SupportedMarkers::Y_GGAAT_1B07, SupportedMarkers::DYS525,
           SupportedMarkers::DYS712, SupportedMarkers::DYS593,
           SupportedMarkers::DYS650, SupportedMarkers::DYS532,
           SupportedMarkers::DYS715, SupportedMarkers::DYS504,
           SupportedMarkers::DYS513, SupportedMarkers::DYS561,
           SupportedMarkers::DYS552, SupportedMarkers::DYS726,
           SupportedMarkers::DYS635, SupportedMarkers::DYS587,
           SupportedMarkers::DYS643, SupportedMarkers::DYS497,
           SupportedMarkers::DYS510, SupportedMarkers::DYS434,
           SupportedMarkers::DYS461, SupportedMarkers::DYS435 } }
        };

    const std::vector<std::string> HaploSet::names_ = [] ()
    {
        std::vector<std::string> res;
        res.reserve(markers_.size());
        for(const auto& elem : markers_)
            res.push_back(elem.first);
        return res;
    } ();

    const std::unordered_map<std::string, int> HaploSet::multi_markers_ = {
        { "DYS413", 2},
        { "DYF395S1", 2},
        { "CDY", 2},
        { "DYS464", 4},
        { "DYS385", 2},
        { "DYS459", 2},
        { "YCAII", 2}
    };

    const std::size_t HaploSet::count_multi_markers = [] ()
    {
        std::size_t count = 0;
            for(const auto& elem : multi_markers_)
                count += elem.second;
        return count;
    } ();

    const size_t HaploSet::count_supported_markers = supported_marker_names_.size();

    const std::unordered_map<SupportedMarkers, HaploInfo, HashSupportedMarkers> HaploSet::haplo_info_ = {
        {SupportedMarkers::DYS393, {9, 17, 0.00059f}},
        {SupportedMarkers::DYS390, {17, 28, 0.0022f}},
        {SupportedMarkers::DYS19, {10, 19, 0.00179f}},
        {SupportedMarkers::DYS391, {6, 14, 0.0022f}},
        {SupportedMarkers::DYS385A, {7, 28, 0.0028f}},
        {SupportedMarkers::DYS385B, {7, 28, 0.0036f}},
        {SupportedMarkers::DYS426, {10, 14, 0.00009f}},
        {SupportedMarkers::DYS388, {8, 18, 0.00022f}},
        {SupportedMarkers::DYS439, {8, 15, 0.003f}},
        {SupportedMarkers::DYS389I, {9, 17, 0.0008f}},
        {SupportedMarkers::DYS392, {6, 17, 0.0004f}},
        {SupportedMarkers::DYS389II, {24, 35, 0.00231f}},
        {SupportedMarkers::DYS458, {12, 22, 0.0062f}},
        {SupportedMarkers::DYS459A, {6, 11, 0.0004f}},
        {SupportedMarkers::DYS459B, {7, 11, 0.00096f}},
        {SupportedMarkers::DYS455, {8, 13, 0.0001f}},
        {SupportedMarkers::DYS454, {9, 14, 0.0003f}},
        {SupportedMarkers::DYS447, {18, 30, 0.0032f}},
        {SupportedMarkers::DYS437, {12, 17, 0.00064}},
        {SupportedMarkers::DYS448, {17, 24, 0.0014}},
        {SupportedMarkers::DYS449, {22, 36, 0.0068}},
        {SupportedMarkers::DYS464A, {9, 21, 0.001}},
        {SupportedMarkers::DYS464B, {9, 21, 0.0011}},
        {SupportedMarkers::DYS464C, {9, 21, 0.0025}},
        {SupportedMarkers::DYS464D, {9, 21, 0.0014}},
        {SupportedMarkers::DYS460, {7, 13, 0.002}},
        {SupportedMarkers::Y_GATA_H4, {8, 14, 0.0017}},
        {SupportedMarkers::YCAIIA, {11, 24, 0.0021}},
        {SupportedMarkers::YCAIIB, {17, 26, 0.00233}},
        {SupportedMarkers::DYS456, {12, 19, 0.00432}},
        {SupportedMarkers::DYS607, {8, 20, 0.0026}},
        {SupportedMarkers::DYS576, {12, 23, 0.006}},
        {SupportedMarkers::DYS570, {12, 25, 0.0047}},
        {SupportedMarkers::CDYA, {25, 41, 0.0066}},
        {SupportedMarkers::CDYB, {25, 45, 0.007}},
        {SupportedMarkers::DYS442, {7, 16, 0.0043}},
        {SupportedMarkers::DYS438, {6, 14, 0.00035}},
        {SupportedMarkers::DYS531, {8, 13, 0.0009}},
        {SupportedMarkers::DYS578, {6, 11, 0.00043}},
        {SupportedMarkers::DYF395S1A, {13, 17, 0.0004}},
        {SupportedMarkers::DYF395S1B, {14, 19, 0.0004}},
        {SupportedMarkers::DYS590, {5, 10, 0.00034}},
        {SupportedMarkers::DYS537, {8, 14, 0.0009}},
        {SupportedMarkers::DYS641, {7, 12, 0.00017}},
        {SupportedMarkers::DYS472, {7, 11, 0.000008}},
        {SupportedMarkers::DYF406S1, {7, 14, 0.0009}},
        {SupportedMarkers::DYS511, {7, 12, 0.0009}},
        {SupportedMarkers::DYS425, {8, 15, 0.000042}},
        {SupportedMarkers::DYS413A, {14, 26, 0.002}},
        {SupportedMarkers::DYS413B, {16, 28, 0.002}},
        {SupportedMarkers::DYS557, {12, 21, 0.0026}},
        {SupportedMarkers::DYS594, {8, 13, 0.00017}},
        {SupportedMarkers::DYS436, {9, 15, 0.00004}},
        {SupportedMarkers::DYS490, {10, 16, 0.00007}},
        {SupportedMarkers::DYS534, {9, 22, 0.00315}},
        {SupportedMarkers::DYS450, {5, 11, 0.00011}},
        {SupportedMarkers::DYS444, {9, 16, 0.0018}},
        {SupportedMarkers::DYS481, {16, 33, 0.004}},
        {SupportedMarkers::DYS520, {15, 26, 0.0015}},
        {SupportedMarkers::DYS446, {9, 20, 0.0028}},
        {SupportedMarkers::DYS617, {8, 17, 0.0005}},
        {SupportedMarkers::DYS568, {7, 13, 0.00005}},
        {SupportedMarkers::DYS487, {10, 16, 0.0007}},
        {SupportedMarkers::DYS572, {8, 13, 0.00092}},
        {SupportedMarkers::DYS640, {8, 13, 0.00015}},
        {SupportedMarkers::DYS492, {9, 15, 0.00015}},
        {SupportedMarkers::DYS565, {7, 14, 0.0019}},
        {SupportedMarkers::DYS710, {28, 42, 0.0073}},
        {SupportedMarkers::DYS485, {10, 18, 0.0016}},
        {SupportedMarkers::DYS632, {7, 10, 0.0001}},
        {SupportedMarkers::DYS495, {12, 18, 0.0014}},
        {SupportedMarkers::DYS540, {10, 14, 0.0013}},
        {SupportedMarkers::DYS714, {21, 30, 0.0045}},
        {SupportedMarkers::DYS716, {25, 29, 0.0005}},
        {SupportedMarkers::DYS717, {16, 23, 0.0006}},
        {SupportedMarkers::DYS505, {9, 16, 0.003}},
        {SupportedMarkers::DYS556, {8, 15, 0.0008}},
        {SupportedMarkers::DYS549, {9, 15, 0.002}},
        {SupportedMarkers::DYS589, {9, 14, 0.00068}},
        {SupportedMarkers::DYS522, {8, 17, 0.0016}},
        {SupportedMarkers::DYS494, {8, 12, 0.0001}},
        {SupportedMarkers::DYS533, {9, 14, 0.00125}},
        {SupportedMarkers::DYS636, {10, 13, 0.00023}},
        {SupportedMarkers::DYS575, {7, 12, 0.0003}},
        {SupportedMarkers::DYS638, {8, 13, 0.00027}},
        {SupportedMarkers::DYS462, {8, 14, 0.0003}},
        {SupportedMarkers::DYS452, {24, 33, 0.001}},
        {SupportedMarkers::DYS445, {6, 14, 0.0005}},
        {SupportedMarkers::Y_GATA_A10, {10, 18, 0.0032}},
        {SupportedMarkers::DYS463, {17, 31, 0.0025}},
        {SupportedMarkers::DYS441, {10, 19, 0.0021}},
        {SupportedMarkers::Y_GGAAT_1B07, {8, 15, 0.001}},
        {SupportedMarkers::DYS525, {8, 14, 0.0017}},
        {SupportedMarkers::DYS712, {17, 32, 0.0062}},
        {SupportedMarkers::DYS593, {14, 17, 0.00012}},
        {SupportedMarkers::DYS650, {14, 23, 0.0037}},
        {SupportedMarkers::DYS532, {8, 17, 0.0035}},
        {SupportedMarkers::DYS715, {20, 27, 0.0032}},
        {SupportedMarkers::DYS504, {11, 20, 0.003}},
        {SupportedMarkers::DYS513, {10, 16, 0.0015}},
        {SupportedMarkers::DYS561, {12, 17, 0.0013}},
        {SupportedMarkers::DYS552, {22, 29, 0.0036}},
        {SupportedMarkers::DYS726, {11, 16, 0.00017}},
        {SupportedMarkers::DYS635, {17, 27, 0.0022}},
        {SupportedMarkers::DYS587, {17, 23, 0.0012}},
        {SupportedMarkers::DYS643, {7, 15, 0.00257}},
        {SupportedMarkers::DYS497, {12, 16, 0.0011}},
        {SupportedMarkers::DYS510, {14, 21, 0.003}},
        {SupportedMarkers::DYS434, {8, 12, 0.0003}},
        {SupportedMarkers::DYS461, {8, 14, 0.0014}},
        {SupportedMarkers::DYS435, {8, 13, 0.00011}}
    };

    const std::unordered_set<std::string> HaploSet::simple_markers_ = []()
    {
        std::unordered_set<std::string> res;
        res.insert(supported_marker_names_.begin(), supported_marker_names_.end());
        return res;
    }();

    const std::unordered_set<std::string> HaploSet::all_markers_ = []()
    {
        std::unordered_set<std::string> res = simple_markers_;
        for(const auto& item : multi_markers_)
            res.insert(item.first);
        return res;
    }();

    const std::unordered_map<std::string, unsigned char> HaploSet::marker_indexes_ = []()
    {
        std::unordered_map<std::string, unsigned char> res;
        for(unsigned char i = 0; i < supported_marker_names_.size(); ++i)
            res[supported_marker_names_[i]] = i;
        return res;
    }();

    const HaploSet::MarkersLine& HaploSet::get_markers(const std::string& name)
    {
        auto it = markers_.find(name);
        if(it == markers_.end()) {
            std::ostringstream err;
            err <<  __func__ << ": can't find markers for panel '" << name << "'"; 
            throw LibException(err.str()); 
        }
        return it->second;
    }

    HaploSet::MarkersLine HaploSet::get_possible_markers(char value)
    {
        MarkersLine res(haplo_info_.size());
        size_t j = 0;
        for(const auto& marker : haplo_info_)
            if(marker.second.min_margin <= value &&
               marker.second.max_margin >= value)
                res[j++] = marker.first;
        res.resize(j);
        return res;
    }

    bool HaploSet::check_margin_(SupportedMarkers name, unsigned char value)
    {
        auto& info = haplo_info_.at(name);
        return (info.min_margin  <= value && info.max_margin >= value);
        //
    }

    unsigned long HaploSet::get_generations(
        SupportedMarkers marker, unsigned long count_mutations)
    {
        const auto& info = haplo_info_.at(marker);
        return static_cast<unsigned long>(count_mutations / info.mutation_speed);
    }
}

